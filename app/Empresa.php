<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $table = 'empresas';
    protected $primaryKey = 'emp_id';

	public function users()
	{
    	return $this->hasMany('App\Users','emp_id','emp_id');
	}
}