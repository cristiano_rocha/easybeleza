<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\MovCaixaRepository;
use App\Entities\MovCaixa;
use App\Validators\MovCaixaValidator;

/**
 * Class MovCaixaRepositoryEloquent
 * @package namespace App\Repositories;
 */
class MovCaixaRepositoryEloquent extends BaseRepository implements MovCaixaRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MovCaixa::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return MovCaixaValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
