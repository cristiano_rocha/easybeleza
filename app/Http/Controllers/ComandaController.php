<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\Comanda;
use App\Comissao;
use App\Item;
use App\MovCaixa;
use App\Pagamento;
use App\ProdutoProfissional;
use App\Profissional;
use App\TipoPagamento;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

use Auth;
use DateTime;
use View;
use Session;

class ComandaController extends Controller
{
    private $Comanda;
    private $item;

   public function _construct(Comanda $Comanda)
   {
      $this->Comanda = $Comanda;
      $this->Item = $Item;
   }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $emp_id = Auth::user()->emp_id;
        $Comandas = Comanda::where('emp_id',$emp_id)->with('cliente')->paginate(10);
        return view('comandas.index')->with('Comandas', $Comandas);
    }

   public function cadastro()
   {
      return view('comandas.cadastro');
   }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($cmd_id = 0)
    {
        $emp_id  = Auth::user()->emp_id;
        $jCli    = Cliente::where('emp_id',$emp_id)->orderby('cli_nome')->orderby('cli_nome')->get();
        $jprof   = Profissional::where('emp_id',$emp_id)->orderby('prof_nome')->get();
        $Comanda = Comanda::where('cmd_id',$cmd_id)->with('itens','cliente')->first();   
        return view::make('comandas.create')->with('jCli',$jCli)->with('jprof',$jprof);        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validação
        // leia http://laravel.com/docs/validation
        $rules = array(
            'cli_id'       => 'required|numeric',
            'cmd_subtotal' => 'required',
            'cmd_total'    => 'required',
            'usu_id'       => 'required|numeric'
        );
        $validator = Validator::make(Input::all(), $rules);

        // processa dados
        if ($validator->fails()) {
            return Redirect::to('comandas/create')
                ->withErrors($validator)
                ->withInput(Input::except('usu_id'));
        } else {
            // gravar
            $comanda = new Comanda;
            $MovCaixa = parent::TemCxAberto();

            $comanda->cmd_data = DateTime::createFromFormat('d/m/Y', $request->cmd_data);

            $comanda->emp_id          = Auth::user()->emp_id;
            $comanda->usu_id          = Auth::user()->id;
            $comanda->cli_id          = $request->cli_id;
            $comanda->cmd_subtotal    = $request->cmd_subtotal;
            $comanda->cmd_total       = $request->cmd_total;
            $comanda->cmd_status      = 1;
            $comanda->movcx_id        = $MovCaixa->movcx_id;
            $comanda->save();

            Session::flash('message', 'Comanda criada com successo!');
            return Redirect::to('comandas/'.$comanda->cmd_id);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Comanda  $comanda
     * @return \Illuminate\Http\Response
     */
    public function show($cmd_id)
    {
        $Comandas = Comanda::where('cmd_id',$cmd_id)->with('itens','cliente')->first();        
        return view('comandas.show')->with('Comandas', $Comandas);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Comanda  $comanda
     * @return \Illuminate\Http\Response
     */
    public function edit($cmd_id)
    {
        $emp_id = Auth::user()->emp_id;
        $jCli    = Cliente::where('emp_id',$emp_id)->orderby('cli_nome')->orderby('cli_nome')->get();
        $jProf   = Profissional::where('emp_id',$emp_id)->orderby('prof_nome')->get();
        $Comanda = Comanda::findOrFail($cmd_id);
        return view::make('comandas.edit')->with('Comanda',$Comanda)->with('jCli',$jCli)->with('jProf',$jProf);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Comanda  $comanda
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $cmd_id)
    {
        // validação
        // leia http://laravel.com/docs/validation
        $rules = array(
            'cli_id'       => 'required|numeric',
            'cmd_subtotal' => 'required',
            'cmd_total'    => 'required',
            'usu_id'       => 'required|numeric'
        );
        $validator = Validator::make(Input::all(), $rules);

        // processa dados
        if ($validator->fails()) {
            return Redirect::to('comandas/create')
                ->withErrors($validator)
                ->withInput(Input::except('usu_id'));
        } else {
            // gravar
            $comanda = Comanda::findOrFail($cmd_id);

            $comanda->cmd_data = DateTime::createFromFormat('d/m/Y', $request->cmd_data);

            $comanda->emp_id          = Auth::user()->emp_id;
            $comanda->usu_id          = Auth::user()->usu_id;
            $comanda->cli_id          = $request->cli_id;
            $comanda->cmd_subtotal    = $request->cmd_subtotal;
            $comanda->cmd_total       = $request->cmd_total;
            $comanda->cmd_status      = 1;
            $comanda->save();

            Session::flash('message', 'Comanda alterada com successo!');
            return Redirect::to('comandas/'.$comanda->cmd_id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comanda  $comanda
     * @return \Illuminate\Http\Response
     */
    public function destroy($cmd_id)
    {
        // delete
        $comanda = Comanda::where('cmd_id',$cmd_id)->first();
        $comanda->delete('cmd_id');

        // redirecionar
        Session::flash('message', 'Comanda excluída!');
        return Redirect::to('comandas');
    }

    public function fatura($id)
    {
        $Comanda = Comanda::where('cmd_id',$id)
            ->with('pagamentos')
            ->first();

        $TiposPagamentos = TipoPagamento::where('status',1)->get();
        
        return view('comandas.fatura', compact('TiposPagamentos',$TiposPagamentos))->with('Comanda',$Comanda);
    }

    public function pagar(Request $request)
    {
        $rules = array(
            'cmd_id'    => 'required|numeric',
            'tpg_id'    => 'required|numeric',
            'pag_valor' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        // processa dados
        if ($validator->fails()) {
            return Redirect::to('comandas/'.$request->cmd_id.'/fatura')
                ->withErrors($validator)
                ->withInput(Input::except('usu_id'));
        } else {
            // gravar
            $Pagamento = new Pagamento;

            $Pagamento->cmd_id          = $request->cmd_id;
            $Pagamento->pag_historico   = 'Prestação serviço';
            $Pagamento->tpg_id          = $request->tpg_id;
            $Pagamento->pag_num         = 1;
            $Pagamento->pag_qtdedias    = 0;
            $Pagamento->pag_dtpagamento = DateTime::createFromFormat('d/m/Y', date('d/m/Y'));
            $Pagamento->pag_valor       = str_replace(",",".", str_replace(".","", $request->pag_valor));
            $Pagamento->pag_vlrpago     = str_replace(",",".", str_replace(".","", $request->pag_valor));
            $Pagamento->status          = 1;
            $Pagamento->save();
        }

        $Comanda = new Comanda;
        $Comanda = Comanda::where('cmd_id',$request->cmd_id)
            ->with('pagamentos')
            ->first();

        $TiposPagamentos = TipoPagamento::where('status',1)->get();
        
        return view('comandas.fatura', compact('TiposPagamentos',$TiposPagamentos))->with('Comanda',$Comanda);
    }

    /**
     * Faturamento de comanda
     *
     * @param \App\Comanda $comanda
     * @return \Illuminate\Http\Response
     */
    public function faturar($id)  
    {
        $MovCaixa = new MovCaixa;
        $MovCaixa = parent::TemCxAberto();

        if ($MovCaixa == null)
        {                           
            return Redirect::to('financeiro/movcaixa');
        } else {       
            $Comanda    = Comanda::findOrFail($id);
            $Pagamentos = Pagamento::where('cmd_id',$id)->get();
            $Itens      = Item::where('cmd_id',$id)->get();
            $Comissoes  = new Comissao;
            //$Comissoes  = Comissao::where('prof_id',$Comanda->prof_id)->get();

            $vComissao = 0;

            foreach ($Itens as $rowi) {
                $vComissao = $vComissao = $rowi->itn_vcomissao;

                $Comissoes->prof_id       = $rowi->prof_id;
                $Comissoes->cli_id        = $Comanda->cli_id;
                $Comissoes->prosrv_id     = $rowi->prosrv_id;
                $Comissoes->cms_data      = new Datetime();
                $Comissoes->cms_qtde      = 1;
                $Comissoes->cms_valor     = $rowi->itn_total;
                $Comissoes->cms_pcomissao = parent::pComissao($rowi->prosrv_id,$rowi->prof_id);
                $Comissoes->cms_vcomissao = $vComissao;
                $Comissoes->emp_id        = $Comanda->emp_id;
            
                $Comissoes->save();
            }            

            foreach ($Pagamentos as $rowp) {
                switch ($rowp->tpg_id) {
                    case '2':
                        $MovCaixa->mov_duplicata = $MovCaixa->mov_duplicata+$rowp->pag_valor;
                        break;
                    case '3':
                        $MovCaixa->mov_cheque = $MovCaixa->mov_cheque+$rowp->pag_valor;
                        break;
                    case '4':
                        $MovCaixa->mov_cartao = $MovCaixa->mov_cartao+$rowp->pag_valor;
                        break;                    
                    default:
                        $MovCaixa->mov_dinheiro = $MovCaixa->mov_dinheiro+$rowp->pag_valor;
                        break;
                }
                $MovCaixa->mov_totalvenda   = $MovCaixa->mov_totalvenda+$rowp->pag_valor;
                $MovCaixa->mov_totalliquido = $MovCaixa->mov_totalliquido+$rowp->pag_valor;

            }

            $MovCaixa->save();
            
            $Comanda->movcx_id      = $MovCaixa->movcx_id;
            $Comanda->cmd_status    = 2;
            $Comanda->save();

            Session::flash('message', 'Comanda '.$id.' faturada');
            return Redirect::to('comandas');
        }

    }

}