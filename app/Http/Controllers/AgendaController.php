<?php

namespace App\Http\Controllers;

use App\Agenda;
use App\Cliente;
use App\Profissional;
use App\ProdutoServico;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;

use Auth;
use Session;
use DateTime;
use Response;

class AgendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $agendas = Agenda::all();
       return view('agenda.index', compact('agendas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $agendas = Agenda::all();
        $prosrvs = ProdutoServico::all();
        return view('agenda.create', compact('agendas', 'prosrvs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'cli_nome'  => 'required',
            'usu_id' => 'required|numeric',
            'prof_id'   => 'required|numeric',
            'prosrv_id' => 'required|numeric',
            'agd_data' => 'required',
            'agd_hora'  => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails())
        {
            return redirect('agenda/create')
            ->withErrors($validator)
            ->withInput(Input::except('usu_id'));
        } else {
            $agenda = new Agenda;

            $agenda->usu_id          = $request->usu_id;
            $agenda->emp_id          = $request->emp_id;
            $agenda->prof_id         = $request->prof_id;
            $agenda->cli_id          = $request->cli_id;
            $agenda->agd_data        = $request->agd_data; //DateTime::createFromFormat('Y/m/d', $request->agd_data);
            $agenda->agd_hora        = DateTime::createFromFormat('H:m', $request->agd_hora);
            $agenda->prosrv_id       = $request->prosrv_id;    
            $agenda->save();
            
            Session::flash('message','Agendameto realizado com sucesso');
            return redirect('agenda/index');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($agd_id)
    {
        $Agendas = Agenda::where('agd_id', $agd_id)->first();
        return view('agenda.show')->with('Agendas', $Agendas);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($agd_id)
    {
        $Agendas = Agenda::findOrfail($agd_id);
        $prosrvs = ProdutoServico::all();
        return view('agenda.edit', compact('Agendas','prosrvs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $agd_id)
    {
        $rules = array(
            'cli_nome'  => 'required',
            'usu_id' => 'required|numeric',
            'prof_id'   => 'required|numeric',
            'prosrv_id' => 'required|numeric',
            'agd_data' => 'required',
            'agd_hora'  => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails())
        {
            return redirect('agenda/create')
            ->withErrors($validator)
            ->withInput(Input::except('usu_id'));
        } else {
            $agenda = new Agenda;

            $agenda->usu_id          = $request->usu_id;
            $agenda->emp_id          = $request->emp_id;
            $agenda->prof_id         = $request->prof_id;
            $agenda->cli_id          = $request->cli_id;
            $agenda->agd_data        = $request->agd_data; //DateTime::createFromFormat('Y/m/d', $request->agd_data);
            $agenda->agd_hora        = DateTime::createFromFormat('H:m', $request->agd_hora);
            $agenda->prosrv_id       = $request->prosrv_id;    
            $agenda->save();
            
            Session::flash('message','Agendameto editado com sucesso');
            return redirect('agenda');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Search user on Database
     * 
     * 
     */
    public function search(Request $request) {
        $query = $request->get('term','');
        
        $clientes=Cliente::where('cli_nome','LIKE','%'.$query.'%')->get();
        
        $data=array();
        foreach ($clientes as $cliente) {
                $data[]=array('value'=>$cliente->cli_nome,'id'=>$cliente->cli_id);
        }
        if(count($data))
             return Response::json($data);
        else
            return ['value'=>'Nenhum cliente encontrado','id'=>''];
    }

    public function searchProf(Request $request) {
        $query = $request->get('term','');
        
        $profissionais=Profissional::where('prof_nome','LIKE','%'.$query.'%')->get();
        
        $data=array();
        foreach ($profissionais as $profissional) {
                $data[]=array('value'=>$profissional->prof_nome,'id'=>$profissional->prof_id);
        }
        if(count($data))
             return $data;
        else
            return ['value'=>'Nenhum profissional encontrado','id'=>''];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function busca(Request $request)
    {
        $periodo = $request->periodo[0];
        switch ($periodo) {
            case 'mes':
                $dtIni = mktime(0, 0, 0, date('m') , 1 , date('Y'));
                $dtFim = mktime(23, 59, 59, date('m'), date("t"), date('Y'));
                break;
            case 'semana':              
                $semana = date('w'); /* Semana atual... */  
                $data   = date('Y-m-d');
                $dtIni  = strtotime('monday this week', strtotime($data));//Primeiro dia da semana último sábado
                $dtFim  = strtotime('saturday this week', strtotime($data));//Último dia da semana próximo sábado
                break;            
            default:
                $data = date('Y-m-d');
                $dtIni = strtotime($data);
                $dtFim = strtotime($data);
                break;
        }
        $dateStart  = date('Y-m-d', $dtIni);
        $dateEnd    = date('Y-m-d', $dtFim);

        $emp_id = Auth::user()->emp_id;

        $profissionais = Profissional::where('emp_id',$emp_id)
            ->orderby('prof_nome')->get();
        $lista = Agenda::where('emp_id',$emp_id)
            ->whereBetween('agd_data',[$dateStart,$dateEnd])
            ->with('profissional','cliente')
            ->WhereHas('profissional', function ($query) use ($request) {
                $query->whereIn('prof_id', $request->profissionais);            
            })->orderBy('agd_data', 'ASC')->get();
        if (count($lista) == 0) {
            Session::flash('message', 'Nenhum registro encontrado');
            return view('calendar.lista', compact('lista','profissionais','request','periodo'));
        }else{
            return view('calendar.lista', compact('lista','profissionais','request','periodo'));
        } 
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function lista()
    {        
        $periodo = 'hoje';
        $data = date('Y-m-d');

        $emp_id = Auth::user()->emp_id;

        $profissionais = Profissional::where('emp_id',$emp_id)   
            ->orderby('prof_nome')->get();
        $lista = Agenda::where('agendas.emp_id',$emp_id)     
            ->whereBetween('agd_data',[$data,$data])
            ->with('profissional')
            ->with('cliente')
            ->orderBy('agd_data', 'ASC')->get();
        
        if (count($lista) == 0) {
            Session::flash('message', 'Nenhum registro encontrado');
            return view('calendar.lista', compact('lista','profissionais','periodo'));
        }else{
            return view('calendar.lista', compact('lista','profissionais','periodo'));
        }
    }
}