<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\Caixa;
use App\MovCaixa;
use App\ProdutoProfissional;

use Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function TemCxAberto()
    {
    	$emp_id = Auth::user()->emp_id;
        $usu_id = Auth::user()->id;

        $MovCaixa = new MovCaixa;
        $MovCaixa = MovCaixa::where('usu_id', $usu_id)
            ->where('emp_id', $emp_id)
            ->where('status', 1)
            ->with('caixa')->first();
        
        return $MovCaixa;
    }

    public function pComissao($prosrv_id,$prof_id) 
    {
        $ProdProf = ProdutoProfissional::where('prosrv_id',$prosrv_id)
            ->where('prof_id',$prof_id)->first();
        if ($ProdProf == null) {
            return 0;
        }else{
            return $ProdProf->proprof_pcomissao;
        }
    }
}