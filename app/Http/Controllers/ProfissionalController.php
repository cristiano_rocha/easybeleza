<?php

namespace App\Http\Controllers;

use App\Profissional;
use App\Produto;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use DateTime;

class ProfissionalController extends Controller
{
    private $Profissional;

   public function _construct(Profissional $Profissional)
   {
      $this->Profissional = $Profissional;
   }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $emp_id = $_COOKIE['emp_id'];
        $Profissionais = Profissional::where('emp_id',$emp_id)->orderBy('prof_nome', 'ASC')->paginate(10);
        
        return view('profissionais.index')->with('Profissionais', $Profissionais);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('profissionais.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	// validação
        // leia http://laravel.com/docs/validation
        $rules = array(
            'prof_nome'       => 'required',
            'usu_id'          => 'required|numeric'
        );
	    $validator = Validator::make(Input::all(), $rules);

        // processa dados
        if ($validator->fails()) {
            return Redirect::to('profissionais/create')
                ->withErrors($validator)
                ->withInput(Input::except('usu_id'));
        } else {
        	// gravar
    	    $profissional = new Profissional;

            $profissional->prof_nascimento = DateTime::createFromFormat('d/m/Y', $request->prof_nascimento);

        	$profissional->usu_id          = $request->usu_id;
	        $profissional->prof_nome       = $request->prof_nome;
        	$profissional->prof_celular    = $request->prof_celular;
            $profissional->prof_cpf        = $request->prof_cpf;
            $profissional->prof_rg         = $request->prof_rg;
            $profissional->prof_email      = $request->prof_email;
            $profissional->prof_cep        = $request->prof_cep;
            $profissional->prof_endereco   = $request->prof_endereco;
            $profissional->prof_bairro     = $request->prof_bairro;
            $profissional->prof_cidade     = $request->prof_cidade;
            $profissional->prof_estado     = $request->prof_estado;
            $profissional->emp_id          = $request->emp_id;
        	$profissional->save();

            Session::flash('message', 'Profissional criado com successo!');
            return Redirect::to('profissionais');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Profissional  $profissional
     * @return \Illuminate\Http\Response
     */
    public function show($prof_id)
    {
        $Profissionais = Profissional::where('prof_id',$prof_id)->first();
        return view('profissionais.show')->with('Profissionais', $Profissionais);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Profissional  $profissional
     * @return \Illuminate\Http\Response
     */
    public function edit($prof_id)
    {
        $Profissionais = Profissional::findOrFail($prof_id);
        return view('profissionais.edit',compact('Profissionais'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Profissional  $profissional
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $prof_id)
    {
        $profissional = Profissional::findOrFail($prof_id);

        $profissional->prof_nascimento = DateTime::createFromFormat('d/m/Y', $request->prof_nascimento);

        $profissional->prof_nome       = $request->prof_nome;
        $profissional->prof_cpf        = $request->prof_cpf;
        $profissional->prof_rg         = $request->prof_rg;
        $profissional->prof_celular    = $request->prof_celular;
        $profissional->prof_email      = $request->prof_email;
        $profissional->prof_endereco   = $request->prof_endereco;
        $profissional->prof_bairro     = $request->prof_bairro;
        $profissional->prof_cep        = $request->prof_cep;
        $profissional->prof_cidade     = $request->prof_cidade;
        $profissional->prof_estado     = $request->prof_estado;
        $profissional->save();
        
        Session::flash('message', 'Profissional atualizado com successo!');
        return Redirect::to('profissionais');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Profissional  $profissional
     * @return \Illuminate\Http\Response
     */
    public function destroy($prof_id)
    {        
        // delete
        $Profissionais = Profissional::where('prof_id',$prof_id)->first();
        $Profissionais->delete('prof_id');

        // redirecionar
        Session::flash('message', 'Profissional excluído!');
        return Redirect::to('profissionais');
    }
}
