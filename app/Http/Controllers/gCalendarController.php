<?php
namespace App\Http\Controllers;

use App\Cliente;
use App\Profissional;
use App\ProdutoServico;
use App\Agenda;
use Auth;
use App\Empresa;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Google_Client;
use Google_Service_Calendar;
use Google_Service_Calendar_Event;
use Google_Service_Calendar_EventDateTime;
use DateTime;
use Session;

class gCalendarController extends Controller
{
    protected $client;
    public function __construct()
    {
        $client = new Google_Client();
        $client->setApplicationName('Login para EasyBeleza');
        $client->setAuthConfig('client_secret.json');
        $client->setAccessType('offline');

        $client->addScope(Google_Service_Calendar::CALENDAR);
        $guzzleClient = new \GuzzleHttp\Client(array('curl' => array(CURLOPT_SSL_VERIFYPEER => false)));
        $client->setHttpClient($guzzleClient);
        $this->client = $client;
        $empresa = new Empresa();        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($lista = false)
    {
        return view('.lista');
        /*
        if(!empty($cookie)){
            $this->$client->refreshToken($this->Cookie->read('token'));
        }
        session_start();
        $empresa = Empresa::findOrfail(Auth::user()->emp_id); // ID Empresa gravado no Cookie

        if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
            $this->client->setAccessToken($_SESSION['access_token']);
            $service = new Google_Service_Calendar($this->client);
            $calendarId = $empresa->emp_gtoken; // DI agenda no cadastro da empresa
            $results = $service->events->listEvents($calendarId);
            /*$calendarList = $service->calendarList->listCalendarList();

            while(true) {
              foreach ($calendarList->getItems() as $calendarListEntry) {
                echo $calendarListEntry->getSummary();
              }
              $pageToken = $calendarList->getNextPageToken();
              if ($pageToken) {
                $optParams = array('pageToken' => $pageToken);
                $calendarList = $service->calendarList->listCalendarList($optParams);
              } else {
                break;
              }
            }*/

            //return $calendarList->getItems();
            /*if ($lista) {
              return $results->getItems();
            }else{
              return view('calendar.index');
            }                       
        } else {
            return redirect()->route('oauthCallback');
        }*/
    }
    public function oauth()
    {
        session_start();

        $rurl = action('gCalendarController@oauth');
        $this->client->setRedirectUri($rurl);
        if (!isset($_GET['code'])) {
            $auth_url = $this->client->createAuthUrl();
            $filtered_url = filter_var($auth_url, FILTER_SANITIZE_URL);


            $accessToken = $this->client->getAccessToken();
            /*$refreshToken = json_decode($accessToken)->refresh_token;
            setcookie('refresh_token', $refreshToken, (time()+(3*24*3600)));*/
            setcookie('access_token', $accessToken,   (time()+(3*24*3600)));

            return redirect($filtered_url);
        } else {
            $this->client->authenticate($_GET['code']);
            
            $_SESSION['access_token'] = $this->client->getAccessToken();

            return redirect()->route('gcalendar.index', compact($_SESSION['access_token']));
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {                
        $jCli  = Cliente::where('cli_id','>',0)
            ->orderby('cli_nome')
            ->get();
        $jprof = Profissional::where('prof_id','>',0)->orderby('prof_nome')->get();
        $jprod = Produtoservico::where('prosrv_id','>',0)->orderby('prosrv_nome')->get();
        return view('calendar.createEvent', compact('jCli','jprof','jprod'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $rules = array(
           'cli_id'      => 'required'
       );

       $validator = Validator::make(Input::all(), $rules);

       if($validator-> fails())
       {
           return redirect('gcalendar/create')
                ->withErrors($validator)
                ->withInput(Input::except('usu_id'));
       } else {

       }

        $dtconvert = DateTime::createFromFormat('d/m/Y', $request->start_date);
        $hrconvert = "$request->start_time";

        $edtconvert = DateTime::createFromFormat('d/m/Y', $request->end_date);
        $ehrconvert = "$request->end_time";
        
        $startDateTime = $dtconvert->format('Y-m-d').'T'.$hrconvert.':00-03:00';
        $endDateTime   = $edtconvert->format('Y-m-d').'T'.$ehrconvert.':00-03:00';        

        $iClient    = strlen($request->cli_id);
        $iClientFim = strpos($request->cli_id, '|');
        $scliente   = substr($request->cli_id, 0, $iClientFim);

        $Cliente = Cliente::where('cli_id',$scliente);

        $iProf         = strlen($request->title);
        $iProfFim      = strpos($request->title, '|');
        $sprofissional = substr($request->title, 0, $iProfFim);
        $iTexto        = $iProf-$iProfFim-1;    

        $iProd         = strlen($request->prosrv_id);
        $iProdFim      = strpos($request->prosrv_id, '|');
        $sproduto      = substr($request->prosrv_id, 0, $iProdFim);
        
        $description = 'Serviço: '.$request->prosrv_id.' <BR /> Cliente: '.$request->cli_id;

            $agenda = new Agenda;

            $agenda->cli_id      = $scliente;
            $agenda->usu_id      = $request->usu_id;
            $agenda->emp_id      = Auth::user()->emp_id;
            $agenda->prof_id     = $sprofissional;
            $agenda->prosrv_id   = $sproduto;
            $agenda->agd_data    = $dtconvert;
            $agenda->agd_hora    = $hrconvert;
            $agenda->agd_horafim = $ehrconvert;
            $agenda->agd_eid     = 0;//$results->id;
            $agenda->save();

            Session::flash('message', 'Evento agendado com successo!');
            return Redirect::to('lista');
    }
    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function show($id)
    {
        $agenda = Empresa::findOrfail($id);       
        return view('calendar.show', compact('agenda'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jCli  = Cliente::where('cli_id','>',0)
                ->orderby('cli_nome')
                ->get();
            $jprof = Profissional::where('prof_id','>',0)->orderby('prof_nome')->get();
            $jprod = Produtoservico::where('prosrv_id','>',0)->orderby('prosrv_nome')->get();
        $agenda = Agenda::findOrFail($id);
        return view('calendar.edit', compact('agenda','jCli','jprof','jprod'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $agd_id
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(Request $request, $eventId)
    {
        $rules = array(
           'cli_id'      => 'required'
       );

       $validator = Validator::make(Input::all(), $rules);

       if($validator-> fails())
       {
           return redirect('gcalendar/create')
                ->withErrors($validator)
                ->withInput(Input::except('usu_id'));
       } else {

       }

        $dtconvert = DateTime::createFromFormat('d/m/Y', $request->start_date);
        $hrconvert = "$request->start_time";

        $edtconvert = DateTime::createFromFormat('d/m/Y', $request->end_date);
        $ehrconvert = "$request->end_time";
        
        $startDateTime = $dtconvert->format('Y-m-d').'T'.$hrconvert.':00-03:00';
        $endDateTime   = $edtconvert->format('Y-m-d').'T'.$ehrconvert.':00-03:00';        

        $iClient    = strlen($request->cli_id);
        $iClientFim = strpos($request->cli_id, '|');
        $scliente   = substr($request->cli_id, 0, $iClientFim);

        $Cliente = Cliente::where('cli_id',$scliente);

        $iProf         = strlen($request->title);
        $iProfFim      = strpos($request->title, '|');
        $sprofissional = substr($request->title, 0, $iProfFim);
        $iTexto        = $iProf-$iProfFim-1;    

        $iProd         = strlen($request->prosrv_id);
        $iProdFim      = strpos($request->prosrv_id, '|');
        $sproduto      = substr($request->prosrv_id, 0, $iProdFim);
        
        $description = 'Serviço: '.$request->prosrv_id.' <BR /> Cliente: '.$request->cli_id;

            $agenda = Agenda::findOrfail($eventId);

            $agenda->cli_id      = $scliente;
            $agenda->usu_id      = Auth::user()->id;
            $agenda->emp_id      = Auth::user()->emp_id;
            $agenda->prof_id     = $sprofissional;
            $agenda->prosrv_id   = $sproduto;
            $agenda->agd_data    = $dtconvert;
            $agenda->agd_hora    = $hrconvert;
            $agenda->agd_horafim = $ehrconvert;
            $agenda->agd_eid     = 0;//$results->id;
            $agenda->save();

            Session::flash('message', 'Evento agendado com successo!');
            return Redirect::to('lista');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param $eventId
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy($eventId)
    {
        $Agendas = Agenda::where('agd_id',$eventId)->first();        
        $Agendas->delete('agd_id');

        // redirecionar
        Session::flash('message', 'Evento excluído!');
        return Redirect::to('lista');
    }


    public function jsonprod(Request $request)
    {
        $id = $request->get('prosrv_id');
        $price = Produtoservico::where('prosrv_id', $id)->first();
        $preco = $price->prosrv_preco;
        return response()->json($preco);
    }

    public function lista() 
    {              
        $emp_id = Auth::user()->emp_id;
        $lista = Agenda::where('emp_id',$emp_id)->with('profissional')->with('cliente')->orderBy('agd_data', 'ASC')->paginate(10);
      
        if ($lista) {
            return view('calendar.lista', compact('lista'));
          }else{
            return view('calendar.index');
          }              
    }
}