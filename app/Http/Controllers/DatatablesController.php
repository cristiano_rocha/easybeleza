<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Datatables;

class DatatablesController extends Controller
{
    public function caixaIndex() {
        return view('financeiro.caixa');
    }

    public function getCaixa() {
        return Datatables::of(Caixa::query())->make(true);
    }
}
