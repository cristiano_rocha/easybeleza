<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Caixa;
use Auth;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class CaixaController extends Controller
{

    private $Caixa;

    public function _construct(Caixa $Caixa)
    {
        $this->Caixa = $Caixa;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $Caixa = Caixa::where('emp_id',Auth::user()->emp_id)->with('Users')->paginate(10);
        $status = $request->session()->get('status');
        return view('financeiro.caixa.index')->with('Caixa', $Caixa)->with('status', $status);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('financeiro.caixa.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule = array('cx_descricao' => 'required');

        $validator = Validator::make(Input::all(), $rule);
        
            if($validator->fails())
            {
                return redirect('financeiro/caixa/create')
                        ->withErrors($validator)
                        ->withInput(Input::except('usu_id'));
            } else {
                $Caixa = new Caixa;
                
                // $Caixa->cx_id                   = $request->cx_id;
                $Caixa->cx_descricao = $request->cx_descricao;
                if ($request->status == 'on') {
                    $Caixa->status = 1;
                }else{
                    $Caixa->status=0;
                }
                $Caixa->usu_id       = $request->usu_id;
                $Caixa->emp_id       = $request->emp_id;
                $Caixa->save();
                

                
                Session::flash('message','Caixa criado com sucesso');
                return redirect('financeiro/caixa');
            }
    }

   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($cx_id)
    {
        $Caixa = Caixa::where('cx_id',$cx_id)->first();
        return view('financeiro.caixa.show')->with('Caixa', $Caixa);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($cx_id)
    {
        $Caixa = Caixa::findOrfail($cx_id);
        return view('financeiro.caixa.edit', compact('Caixa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $cx_id)
    {
        $Caixa = Caixa::findOrfail($cx_id);

        $Caixa->cx_descricao       = $request->cx_descricao;
        if ($request->status == 'on') {
            $Caixa->status = 1;
        }else{
            $Caixa->status=0;
        }
        $Caixa->save();

        Session::flash('message','Caixa atualizado com sucesso');
        return redirect('financeiro/caixa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($cx_id)
    {
        $Caixa = Caixa::find($cx_id);
        $Caixa->delete('cx_id');

        Session::flash('message', 'Caixa excluido!');
        return Redirect::to('financeiro/caixa');
    }
}
