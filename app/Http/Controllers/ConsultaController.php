<?php

namespace App\Http\Controllers;

use App\Cliente;

use Illuminate\Http\Request;

class ConsultaController extends Controller
{
    //
    public function index() {
        return view('consulta.pesquisa');
    }

    public function find(Request $request) {
        $query = $request->get('query','');
        $clientes = Cliente::where('cli_nome','LIKE','%'.$query.'%')->get();
        return response()->json($clientes);
    }
}
