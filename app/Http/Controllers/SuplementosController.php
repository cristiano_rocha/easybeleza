<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\Suplementos;
use App\Caixa;
use Illuminate\Support\Facades\Redirect;

class SuplementosController extends Controller
{

    public function __constructor(Suplementos $Suplementos)
    {
        $this->Suplementos = $Suplementos;
        $this->middleware('CheckUserCaixaStatus');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Suplemento = Suplementos::all();
        
        return view('financeiro.caixa.movcaixa.suplementos.index')->with('Suplemento', $Suplemento);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Caixa = Caixa::all();
        return view('financeiro.caixa.movcaixa.suplementos.create')->with('Caixa', $Caixa);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Suplementos = New Suplementos;
        $MovCaixa  = parent::TemCXAberto();

        $Suplementos->supl_valor          = $request->supl_valor;
        $Suplementos->supl_descricao      = $request->supl_descricao;
        $Suplementos->mov_id            = $MovCaixa->mov_id;
        $Suplementos->usu_id            = Auth::user()->id;
        $Suplementos->emp_id            = Auth::user()->emp_id;
        $Suplementos->save();

        Session::flash('message','Suplementos criado com sucesso');
        return redirect('/suplemento');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($supl_id)
    {
        $Suplemento = Suplementos::where('supl_id', $supl_id)->first();
        return view('financeiro.caixa.movcaixa.suplementos.show')->with('Suplemento', $Suplemento);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($supl_id)
    {
        $Suplemento = Suplementos::findOrfail($supl_id);
        $Caixa = Caixa::all();
        return view('financeiro.caixa.movcaixa.suplementos.edit')->with('Caixa', $Caixa)->with('Suplemento',$Suplemento);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $supl_id)
    {
        $Suplementos = Suplementos::findOrfail($supl_id);

        $Suplementos->supl_valor          = $request->supl_valor;
        $Suplementos->supl_descricao      = $request->supl_descricao;
        $Suplementos->usu_id            = Auth::user()->id;
        $Suplementos->emp_id            = Auth::user()->emp_id;
        $Suplementos->save();

        Session::flash('message', 'Suplementos alterada com sucesso');
        return redirect('suplemento');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($supl_id)
    {
        $Suplemento = Suplementos::find($supl_id);
        $Suplemento->delete('supl_id');

        Session::flash('message',' Suplemento excluída!');
        return Redirect::to('suplemento');
    }
}
