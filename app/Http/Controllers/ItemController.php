<?php

namespace App\Http\Controllers;

use App\Comanda;
use App\Item;
use App\MovCaixa;
use App\ProdutoProfissional;
use App\ProdutoServico;
use App\Profissional;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Auth;
use DateTime;
use Response;
use Session;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Profissionais = Profissional::where('emp_id',$emp_id)->orderBy('prof_nome', 'ASC')->paginate(10);
      $Itens = Item::where('itn_id',$itn_id)->with('produtoservico')->first();
      return view('itens.index')->with('Itens', $Itens);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($cmd_id)        
    {        
        $emp_id = Auth::user()->emp_id;
        $jprof = Profissional::where('emp_id',$emp_id)->where('prof_id','>',0)->orderby('prof_nome')->get();
        $jprod = ProdutoServico::where('emp_id',$emp_id)->where('prosrv_id','>',0)->orderby('prosrv_nome')->get();
        return view('itens.create', compact('cmd_id','jprof','jprod'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $cmd_id)
    {
        // validação
        // leia http://laravel.com/docs/validation
        $rules = array(
            'itn_data'         => 'required',
            'prof_id'         => 'required|numeric',
            'usu_id'          => 'required|numeric'
        );
        $validator = Validator::make(Input::all(), $rules);

        // processa dados
        if ($validator->fails()) {
            return Redirect::to('itens/create')
                ->withErrors($validator)
                ->withInput(Input::except('usu_id'));
        } else {
            // gravar
            $item = new Item;
            $ProdProf = new ProdutoProfissional;
            $ProdProf = ProdutoProfissional::where('prosrv_id',$request->prosrv_id)
                ->where('prof_id',$request->prof_id)
                ->first();

            $item->itn_data = date_create()->format('Y-m-d H:i:s');
            $item->itn_dt_alt = date_create()->format('Y-m-d H:i:s');

            if ($request->itn_custo == null){
                $request->itn_custo = 0;
            }

            if ($request->itn_subtotal == null){
                $request->itn_subtotal = 0;
            }

            $request->itn_total = $request->itn_subtotal;

            $item->usu_id          = $request->usu_id;
            $item->prof_id         = $request->prof_id;
            $item->cmd_id          = $request->cmd_id; 
            $item->prosrv_id       = $request->prosrv_id;
            $item->itn_qtde        = str_replace(",",".", str_replace(".","", $request->itn_qtde));
            $item->itn_ucom        = $request->itn_ucom;
            $item->itn_custo       = str_replace(",",".", str_replace(".","", $request->itn_custo));
            $item->itn_preco       = str_replace(",",".", str_replace(".","", $request->itn_preco));
            $item->itn_desconto    = str_replace(",",".", str_replace(".","", $request->itn_desconto));
            $item->itn_subtotal    = str_replace(",",".", str_replace(".","", $request->itn_subtotal));
            $item->itn_total       = str_replace(",",".", str_replace(".","", $request->itn_total));
            $vComissao = 0;
            if ($ProdProf->proprof_pcomissao > 0){
                $vComissao = ($ProdProf->proprof_pcomissao / 100) * $item->itn_total;
            }
            $item->itn_vcomissao   = $vComissao;
            $item->save();

            $Comandas = Comanda::where('cmd_id',$request->cmd_id)->with('itens','cliente')->first(); 
            $Comandas->cmd_subtotal = ($Comandas->cmd_subtotal+$item->itn_subtotal);
            $Comandas->cmd_total    = ($Comandas->cmd_total+$item->itn_total);
            $Comandas->save();

            Session::flash('message', 'Item incluído');
            return Redirect::to('./comandas/'.$item->cmd_id.'/');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show($cmd_id,$itn_id)
    {
        $Itens = Item::where('itn_id',$itn_id)->with('produtoservico')->first();
        return view('itens.show')->with('Itens', $Itens);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy($cmd_id,$itn_id)
    {
        // delete
        $item = Item::find($itn_id);
        $cmd_id = $item->cmd_id;
        $item->delete('itn_id');

        $Comandas = Comanda::where('cmd_id',cmd_id)->with('itens','cliente')->first(); 
        $Comandas->cmd_subtotal = ($Comandas->cmd_subtotal-$item->itn_subtotal);
        $Comandas->cmd_total    = ($Comandas->cmd_total-$item->itn_total);
        $Comandas->save();



        // redirecionar
        Session::flash('message', 'Item excluído!');
        return Redirect::to('./comandas/'.$cmd_id.'/');
    }

    public function jsonprod(Request $request)
    {
        $id = $request->get('prosrv_id') ;
        $price = Produtoservico::where('prosrv_id', $id)->first();
        $preco = $price->prosrv_preco;
        return response()->json($preco);
    }

    public function prduncomplete(Request $request)
    {        
        $id = $request->get('prosrv_id') ;
        $produto = Produtoservico::where('prosrv_id', $id)->first();
        $prdun = $produto->prosrv_ucom;

        return response()->json($prdun);
    }

    public function ajaxprodutosservicos()
    {
        $prof_id = $_GET['prof_id'];

        $produtos = ProdutoServico::where('prof_id',$prof_id)->get();

        return Response::json($produtos);
    }
}
