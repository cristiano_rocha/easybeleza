<?php

namespace App\Http\Controllers;

use App\Cliente;
use Illuminate\Http\Request;
use Session;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use DateTime;

class ClienteController extends Controller
{
    private $Cliente;

    public function _construct(Cliente $Cliente)
    {
       $this->Cliente = $Cliente;
    }

    public function index()
    {
        // Verifica empresa logada
        $emp_id = $_COOKIE['emp_id'];
        // Filtra por Filial
        $Clientes = Cliente::where('emp_id',$emp_id)->orderBy('cli_nome','ASC')->paginate(10);
        return view('clientes.index')->with('Clientes', $Clientes);
    }

    public function create()
    {
        return view('clientes.create');
    }

    public function show($cli_id = null)
    {
        // Filtra pelo ID de cliente
        $Clientes = Cliente::where('cli_id',$cli_id)->first();
        return view('clientes.show')->with('Clientes', $Clientes);
    }


    public function edit($cli_id)
    {
        // Filtra pelo ID de Cliente e carrega o Form Edit
        $Clientes = Cliente::findOrfail($cli_id);
        return view('clientes.edit', compact('Clientes'));
    }

    public function update(Request $request, $cli_id)
    {
        $cliente = Cliente::findOrFail($cli_id);

        // Verifica se tem data de Nascimento
        if ($request->cli_nascimento >0){
            $cliente->cli_nascimento = DateTime::createFromFormat('d/m/Y', $request->cli_nascimento);
        }
        $cliente->cli_nome           = $request->cli_nome;
        $cliente->cli_cpfcnpj        = $request->cli_cpfcnpj;
        $cliente->cli_telefone       = $request->cli_telefone;
        $cliente->cli_rg             = $request->cli_rg;
        $cliente->cli_email          = $request->cli_email;
        $cliente->cli_endereco       = $request->cli_endereco;
        $cliente->cli_cep            = $request->cli_cep;
        $cliente->cli_cidade         = $request->cli_cidade;
        $cliente->cli_bairro         = $request->cli_bairro;
        $cliente->cli_estado         = $request->cli_estado;

        $cliente->save();

        Session::flash('message','Cliente alterado com sucesso!');
        return redirect('clientes');
    }

    public function store(Request $request)
    {
        $rules = array(
            'cli_nome'      => 'required',
            'usu_id'        => 'required|numeric'
        );

        $validator = Validator::make(Input::all(), $rules);

        // Valida e retorna erro se houver
        if($validator-> fails())
        {
            return redirect('clientes/create')
                ->withErrors($validator)
                ->withInput(Input::except('usu_id'));
        } else {
            $cliente = new Cliente;
        }

        // Verifica se tem data de Nascimento
        if ($request->cli_nascimento >0){
            $cliente->cli_nascimento = DateTime::createFromFormat('d/m/Y', $request->cli_nascimento);
        }

        $cliente->usu_id             = $request->usu_id;
        $cliente->emp_id             = $request->emp_id;
        $cliente->cli_nome           = $request->cli_nome;
        $cliente->cli_cpfcnpj        = $request->cli_cpfcnpj;
        $cliente->cli_telefone       = $request->cli_telefone;
        $cliente->cli_rg             = $request->cli_rg;
        $cliente->cli_email          = $request->cli_email;
        $cliente->cli_endereco       = $request->cli_endereco;
        $cliente->cli_cep            = $request->cli_cep;
        $cliente->cli_cidade         = $request->cli_cidade;
        $cliente->cli_bairro         = $request->cli_bairro;
        $cliente->cli_estado         = $request->cli_estado;

        $cliente->save();

        Session::flash('message','Cliente criado com sucesso!');
        return redirect('clientes');           
    }

    public function saveJson()
    {
        $input = file_get_contents('php://input'); // Pega todos os dados do json
        $jsonDecode = json_decode($input);         // Decodifica o json e transforma em objeto
        $clientes = $jsonDecode->clientes[0];      // Joga Array na variável

        $cliente->cli_nome = $clientes->nome;
        $cliente->cli_cpfcnpj = $clientes->cpfcnpj;
        $cliente->cli_email = $clientes->email;
        $cliente->cli_telefone = $clientes->telefone;
        $cliente->cli_dt_add = now;

        $cliente->save(); // Salva o input

        return $cliente; // Retorna o cliente gravado
    }

    public function updateJson()
    {
      $input = file_get_contents('php://input'); // Pega todos os dados do json
      $jsonDecode = json_decode($input);         // Decodifica o json e transforma em objeto
      $clientes = $jsonDecode->clientes[0];      // Joga Array na variável
      $cliente->cli_nome = $clientes->nome;
      $cliente->cli_cpfcnpj = $clientes->cpfcnpj;
      $cliente->cli_email = $clientes->email;
      $cliente->cli_telefone = $clientes->telefone;
 
      $cliente->update(); // Salva o input
 
      return $cliente; // Retorna o cliente gravado
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Profissional  $profissional
     * @return \Illuminate\Http\Response
     */
    public function destroy($cli_id)
    {
        // Seleciona pelo ID Cliente
        $Clientes = Cliente::where('cli_id',$cli_id)->first();
        $Clientes->delete('cli_id');  // Exclui
        
        // redirecionar
        Session::flash('message', 'Cliente excluído!');
        return Redirect::to('clientes');
    }   

    public function getCliente($cli_id)
    {
        $Cliente = Cliente::findOrfail($cli_id);
        return response($Cliente);
    }
}