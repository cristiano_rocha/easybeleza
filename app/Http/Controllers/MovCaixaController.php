<?php

namespace App\Http\Controllers;
use App\MovCaixa;
use App\Caixa;
use App\Empresa;
use Auth;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use DateTime;

class MovCaixaController extends Controller
{

    public function __construct(MovCaixa $MovCaixa)
    {
        $this->MovCaixa = $MovCaixa;
        $this->middleware('CheckUserCaixaStatus');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $MovCaixa = MovCaixa::where('emp_id',Auth::user()->emp_id)
          ->with('Users')
          ->paginate(10);
        return view('financeiro.caixa.movcaixa.index')->with('MovCaixa', $MovCaixa);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('financeiro.caixa.movcaixa.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Redirector $redirect)
    {		             
    	// Sem uso no momento
    }

    // Método para abrir o Caixa
    public function abertura(Request $request)
    {    	

        $MovCaixa = new MovCaixa;
        
        $emp_id = Auth::user()->emp_id;
        $MovCaixa->mov_saldo_ini      = str_replace(",",".", str_replace(".","", $request->cx_saldo_ini));
    	$MovCaixa->cx_id             = $request->cx_id;
    	$MovCaixa->mov_sangria        = 0;
    	$MovCaixa->mov_suplemento     = 0;
    	$MovCaixa->mov_totalvenda     = 0;
    	$MovCaixa->mov_pago           = 0;
    	$MovCaixa->mov_dinheiro       = 0;
    	$MovCaixa->mov_duplicata      = 0;
        $MovCaixa->mov_cheque         = 0;
        
    	$MovCaixa->mov_cartao         = 0;
    	$MovCaixa->mov_desconto       = 0;
    	$MovCaixa->mov_totalliquido   = 0;
    	$MovCaixa->status            = 1;
    	$MovCaixa->dt_aberto         = new DateTime();
    	$MovCaixa->dt_fecha          = null;
    	$MovCaixa->usu_id_fechou     = null;
    	$MovCaixa->usu_id            = $request->usu_id;
    	$MovCaixa->emp_id            = $request->emp_id;
    	$MovCaixa->save();

    	Session::flash('message','Caixa aberto com sucesso');
    	return redirect('financeiro/movcaixa');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($movcx_id)
    {
        $MovCaixa = MovCaixa::where('movcx_id', $movcx_id)->with('caixa')->first();
        return view('financeiro.caixa.movcaixa.show')->with('MovCaixa', $MovCaixa);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($movcx_id)
    {
        $MovCaixa = Caixa::findOrfail($movcx_id);
        return view('financeiro.caixa.movcaixa.edit', compact('MovCaixa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $movcx_id)
    {
        $MovCaixa = MovCaixa::findOrfail($movcx_id);
        
        $MovCaixa->mov_saldo_ini      = $request->mov_saldo_ini;
        $MovCaixa->mov_sangria        = $request->mov_sangria;
        $MovCaixa->mov_suplemento     = $request->mov_suplemento;
        $MovCaixa->mov_totalvenda     = $request->mov_totalvenda;
        $MovCaixa->mov_pago           = $request->cmov_pago;
        $MovCaixa->mov_dinheiro       = $request->mov_dinheiro;
        $MovCaixa->mov_duplicata      = $request->mov_duplicata;
        $MovCaixa->mov_cheque         = $request->mov_cheque;
        $MovCaixa->mov_cartao         = $request->mov_cartao;
        $MovCaixa->mov_desconto       = $request->mov_desconto;
        $MovCaixa->mov_totalliquido   = $request->mov_totalliquido;
        $MovCaixa->status            = $request->status;
        $MovCaixa->usu_id_fechou     = $request->usu_id_fechou;
        $MovCaixa->dt_aberto         = $request->dt_aberto;
        $MovCaixa->dt_fecha          = $request->dt_fecha;
        $MovCaixa->usu_id            = $request->usu_id;
        $MovCaixa->emp_id            = $request->emp_id;
        $MovCaixa->save();
        Session::flash('message','Movimentação criada com sucesso');
        return redirect('financeiro/movcaixa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($movcx_id)
    {
        $MovCaixa = MovCaixa::find($movcx_id);
        $MovCaixa->delete('movcx_id');

        Session::flash('message','Movimentação excluida!');
        return Redirect::to('financeiro/movcaixa');
    }

    public function abrir()
    {
        $MovCaixa = new MovCaixa;
        $MovCaixa = parent::TemCxAberto();

        if ($MovCaixa == null)
        {

            $Caixas = Caixa::where('emp_id',Auth::user()->emp_id)->orderby('cx_descricao')->get();
            return view('financeiro.caixa.movcaixa.abrir',compact('Caixas',$Caixas));

        }else{

            Session::flash('message', 'já existe o caixa '. $MovCaixa->movcx_id .' aberto dia '.date( 'd/m/Y' , strtotime($MovCaixa->dt_aberto)));
            return redirect('financeiro/movcaixa');

        }

        
    }

    public function fechamento(Request $request)
    {               
        $MovCaixa = new MovCaixa;
        $MovCaixa = parent::TemCxAberto();

        $MovCaixa->mov_dinheiroinfo          = $request->mov_dinheiroinfo;
        $MovCaixa->mov_duplicatainfo         = $request->mov_duplicatainfo;
        $MovCaixa->mov_chequeinfo            = $request->mov_chequeinfo;
        $MovCaixa->mov_cartaoinfo            = $request->mov_cartaoinfo;
        $MovCaixa->status                    = 0;
        $MovCaixa->dt_fecha                  = new Datetime();
        $MovCaixa->usu_id_fechou             = Auth::user()->id;
        $MovCaixa->save();

        Session::flash('message', 'Caixa fechado com sucesso');
        return redirect('financeiro/movcaixa');
    }

    public function fechar()
    {
        $MovCaixa = new MovCaixa;
        $MovCaixa = parent::TemCxAberto();

        if (!isset($MovCaixa))
        {
            Session::flash('message', 'Não existe caixa aberto dia');
            return redirect('financeiro/movcaixa');

        }else{
            $Empresa = Empresa::findOrfail(Auth::user()->emp_id);
            return view('financeiro.caixa.movcaixa.fechar')
              ->with('MovCaixa', $MovCaixa)
              ->with('Empresa',$Empresa);

        }
    }
}
