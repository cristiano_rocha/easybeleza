<?php

namespace App\Http\Controllers;

use App\ProdutoServico;
use App\Profissional;
use App\ProdutoProfissional;
use Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use DateTime;
use Session;

class ProdutoServicoController extends Controller
{
    private $ProdutoServico;

   public function _construct(ProdutoServico $ProdutoServico)
   {
      $this->ProdutoServico = $ProdutoServico;
   }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $emp_id = Auth::user()->emp_id;
        $ProdutosServicos = ProdutoServico::where('emp_id',$emp_id)->paginate(10);
        return view('produtosservicos.index')->with('ProdutosServicos', $ProdutosServicos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $emp_id = Auth::user()->emp_id;
        return view('produtosservicos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'prosrv_nome'       => 'required',
            'usu_id'            =>  'required|numeric',
            'prosrv_custo'      =>  'required|numeric',
            'prosrv_preco'      =>  'required|numeric'
        );

        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails())
        {
            return Redirect::to('produtosservicos/create')
                ->withErrors($validator)
                ->withInput(Input::except('prosrv_id'));
        } else {

            $prosrv = new ProdutoServico;

            $prosrv->usu_id             = $request->usu_id;
            $prosrv->prosrv_codbarras   = $request->prosrv_codbarras;
            $prosrv->prosrv_nome        = $request->prosrv_nome;
            $prosrv->prosrv_descricao   = $request->prosrv_descricao;
            $prosrv->prosrv_ucom        = $request->prosrv_ucom;
            $prosrv->prosrv_custo       = $request->prosrv_custo;
            $prosrv->prosrv_preco       = $request->prosrv_preco;
            // $prosrv->prosrv_pcomissao   = $request->prosrv_pcomissao;
            $prosrv->emp_id             = $request->emp_id;
            
            $prosrv->save();

            Session::flash('message','Produto criado com sucesso');
            return redirect('produtosservicos');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProdutoServico  $produtoServico
     * @return \Illuminate\Http\Response
     */
    public function show($prosrv_id)
    {
        $ProdutosServicos = ProdutoServico::where('prosrv_id',$prosrv_id)->first();
        return view('produtosservicos.show')->with('ProdutosServicos', $ProdutosServicos);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProdutoServico  $produtoServico
     * @return \Illuminate\Http\Response
     */
    public function edit($prosrv_id)
    {
        $emp_id            = Auth::user()->emp_id;
        $ProdutosServicos  = ProdutoServico::with('produtoprofissional')->findOrFail($prosrv_id);
        $ProdProfissionais = ProdutoProfissional::where('prosrv_id',$prosrv_id)
            ->with('profissional')
            ->get();
        return view('produtosservicos.edit', compact('ProdutosServicos','ProdProfissionais'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProdutoServico  $produtoServico
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $prosrv_id)
    {
        $prosrv = ProdutoServico::findOrFail($prosrv_id);

        $prosrv->prosrv_nome            = $request->prosrv_nome;
        $prosrv->prosrv_descricao       = $request->prosrv_descricao;
        $prosrv->prosrv_codbarras       = $request->prosrv_codbarras;
        $prosrv->prosrv_ucom            = $request->prosrv_ucom;
        $prosrv->prosrv_custo           = str_replace(",",".", str_replace(".","", $request->prosrv_custo));
        $prosrv->prosrv_preco           = str_replace(",",".", str_replace(".","", $request->prosrv_preco));
        // $prosrv->prosrv_pcomissao       = str_replace(",",".", str_replace(".","", $request->prosrv_pcomissao));
        $prosrv->save();

        Session::flash('message','Produto atualizado com sucesso!');
        return Redirect::to('produtosservicos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProdutoServico  $produtoServico
     * @return \Illuminate\Http\Response
     */
    public function destroy($prosrv_id)
    {
        // delete
        $ProdutosServicos = ProdutoServico::where('prosrv_id',$prosrv_id)->first();
        $ProdutosServicos->delete('prosrv_id');

        // redirecionar
        Session::flash('message', 'Produto/Serviço excluído!');
        return Redirect::to('produtosservicos');
    }

    public function ajaxprod()
    {
        $produtos = ProdutoServico::orderBy('prosrv_nome', 'ASC')->get();
        $json = json_enconde($produtos); // transforma em json
        return response()->json($json);  // retornacom o json
    }
}
