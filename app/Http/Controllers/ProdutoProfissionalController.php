<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\ProdutoProfissional;
use App\Profissional;
use Auth;

use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Redirect;

class ProdutoProfissionalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $emp_id = Auth::user()->emp_id;
        $ProdutosProfissionais = ProdutoProfissional::where('emp_id', $emp_id)->paginate(10);
        return view('produtosprofissionais.index')->with('ProdutosProfissionais', $ProdutosProfissionais);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($prosrv_id)
    {        
        $emp_id = Auth::user()->emp_id;
        $jprof = Profissional::where('emp_id',$emp_id)->where('prof_id','>',0)->orderby('prof_nome')->get();
        return view('produtosprofissionais.create', compact('prosrv_id','jprof'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $prosrv_id)
    {
        $prosrv = new ProdutoProfissional;

        $prosrv->prosrv_id          = $prosrv_id;
        $prosrv->prof_id            = $request->prof_id;
        $prosrv->proprof_pcomissao  = str_replace(",",".", str_replace(".","", $request->proprof_pcomissao));
        $prosrv->save();

        Session::flash('message','Comissão definida');
        return Redirect::to('./produtosservicos/'.$prosrv_id.'/edit#Comissoes');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProdutoProfissional  $produtoProfissional
     * @return \Illuminate\Http\Response
     */
    public function show(ProdutoProfissional $produtoProfissional)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProdutoProfissional  $produtoProfissional
     * @return \Illuminate\Http\Response
     */
    public function edit(ProdutoProfissional $produtoProfissional)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProdutoProfissional  $produtoProfissional
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProdutoProfissional $produtoProfissional)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProdutoProfissional  $produtoProfissional
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProdutoProfissional $produtoProfissional)
    {
        //
    }

    public function listaporprof($prof_id)
    {
        $Produtos = ProdutoProfissional::where('prof_id',$prof_id)->orderby('pro');
    }
}
