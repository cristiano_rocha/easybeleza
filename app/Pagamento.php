<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pagamento extends Model
{
    protected $guarded = ['pag_id',  'deleted_at', 'created_at', 'update_at', 'pag_dtpagamento'];
    protected $fillable = ['pag_id', 'cmd_id', 'pag_dtpagamento'];
    protected $table = 'pagamentos';
    protected $primaryKey = 'pag_id';

    public function comanda()
    {        
        return $this->belongsTo('App\Comanda', 'cmd_id', 'cmd_id');
    }

    // Relacionamento N para 1
    public function tipopagamento()
    {        
        return $this->hasOne('App\TipoPagamento', 'tpg_id', 'tpg_id');

    }
}
