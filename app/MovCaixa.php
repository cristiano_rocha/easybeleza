<?php

namespace App;
use App\User;
use Illuminate\Database\Eloquent\Model;

class MovCaixa extends Model
{
    protected $table = 'movcaixas';
    protected $primaryKey = 'movcx_id';
    protected $fillable = ['mov_dinheiroinfo', 'mov_duplicatainfo','mov_chequeinfo','mov_cartaoinfo'];
    // protected $fillable = ['cx_saldo_ini','status','dt_fecha','dt_aberto','cx_cartao','cx_totalliquido','cx_suplemento','cx_pago','cx_totalvenda','cx_dinheiro','cx_duplicata','cx_cheque','status','dt_aberto','dt_fecha'];

    public function users()
    {
        return $this->hasOne('App\User','id','usu_id');
    }

    public function comanda()
    {        
        return $this->belongsTo('App\Comanda', 'movcx_id', 'movcx_id');
    }


    public function caixa($cx_id = FALSE)
    {
        return $this->hasOne('App\Caixa', 'cx_id', 'cx_id');
    }
}
