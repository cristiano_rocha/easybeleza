<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ProdutoServico;
use App\Profissional;

class Item extends Model
{
    protected $guarded = ['itn_id', 'created_at', 'update_at', 'itn_data'];    
    protected $fillable = ['itn_id', 'cmd_id', 'itn_data'];
    protected $table = 'itens';
    protected $primaryKey = 'itn_id';

    public function comanda()
    {        
        return $this->belongsTo('App\Comanda', 'cmd_id', 'cmd_id');
    }

    // Relacionamento N para N
    public function produtoservico()
    {        
        return $this->hasOne('App\ProdutoServico', 'prosrv_id', 'prosrv_id');
    }

    // Relacionamento N para N
    public function profissional()
    {        
        return $this->hasOne('App\Profissional', 'prof_id', 'prof_id');
    }
}
