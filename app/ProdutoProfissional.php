<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ProdutoServico;
use App\Profissional;

class ProdutoProfissional extends Model
{
    protected $table = 'produtosprofissionais';
    protected $fillable = ['proprof_pcomissao'];
    protected $primaryKey = 'proprof_id';
    

    public function produtoservico()
    {        
        return $this->belongsTo('App\ProdutoServico', 'prosrv_id', 'prosrv_id');
    }

    // Relacionamento N para 1
    public function profissional()
    {        
        return $this->hasOne('App\Profissional', 'prof_id', 'prof_id');

    }
    
}
