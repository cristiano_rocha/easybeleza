<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comissao extends Model
{
    protected $table = 'comissoes';
    protected $primaryKey = 'cms_id';

    public function profissional()
    {        
        return $this->belongsTo('App\Profissional', 'prof_id', 'prof_id');
    }
}
