<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoPagamento extends Model
{
    protected $guarded = ['tpg_id',  'created_at', 'update_at'];
    protected $fillable = ['tpg_id', 'tpg_nome', 'tpg_mobile', 'status'];
    protected $table = 'tipospagamentos';
    protected $primaryKey = 'tpg_id';    

    // Relacionamento 1 para N
    public function pagamento()
    {
        return $this->belongsTo('App\Pagamento', 'tpg_id','tpg_id');
    }
}
