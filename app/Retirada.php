<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Retirada extends Model
{
    protected $fillable = ['re_valor', 're_descricao','mov_id'];
    protected $primaryKey = 're_id';
    protected $table = 'retiradas';


    public function MovCaixa()
    {
        return $this->hasOne('App\MovCaixa', 'mov_id', 'mov_id');
    }
}
