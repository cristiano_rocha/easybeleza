<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class MovCaixa extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable   = ['cx_saldo_ini','status','dt_fecha','dt_aberto','cx_cartao','cx_totalliquido','cx_suplemento','cx_pago',
    					     'cx_totalvenda','cx_dinheiro','cx_duplicata','cx_cheque','status','dt_aberto','dt_fecha'];
    public    $timestamps = true;

}
