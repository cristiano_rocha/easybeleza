<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agenda extends Model
{
    protected $guarded = ['agd_id', 'created_at', 'update_at'];
    protected $table = 'agendas';
    protected $primaryKey = 'agd_id';

    // Relacionamento N para 1
    public function cliente()
    {        
        return $this->hasOne('App\Cliente', 'cli_id', 'cli_id');
    }

    // Relacionamento N para N
    public function profissional()
    {        
        return $this->hasOne('App\Profissional', 'prof_id', 'prof_id');
    }

    // Relacionamento N para 1
    public function produtoservico()
    {        
        return $this->hasOne('App\ProdutoServico', 'prosrv_id', 'prosrv_id');
    }
}