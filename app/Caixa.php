<?php

namespace App;
use App\User;

use Illuminate\Database\Eloquent\Model;

class Caixa extends Model
{
    protected $table = 'caixas';
    protected $fillable = ['cx_id','cx_descricao','status'];
    protected $primaryKey = 'cx_id';

    public function users()
    {
        return $this->hasOne('App\User','id','usu_id');
    }

    public function retiradas()
    {
        return $this->belongsTo('App\Retirada', 're_id', 're_id');
    }

    public function suplementos()
    {
        return $this->belongsTo('App\Suplemento', 'su_id', 'su_id');
    }

    public function movcaixa()
    {
        return $this->belongsTo('App\MovCaixa', 'cx_id', 'cx_id');
    }
}
