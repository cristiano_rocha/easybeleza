<?php
 
if (! function_exists('special_ucwords')) {
	function special_ucwords($string)
	{
		$words = explode(' ', strtolower(trim(preg_replace("/\s+/", ' ', $string))));
		$return[] = ucfirst($words[0]);
	 	
		unset($words[0]);
				 
		foreach ($words as $word)
		{
			if (!preg_match("/^([dn]?[aeiou][s]?|em)$/i", $word))
			{
				$word = ucfirst($word);
			}
			$return[] = $word;
		}		 
		return implode(' ', $return);
	}


    if (! function_exists('cpf')) {
    /**
     * Get the path to the base of the install.
     *
     * @param  string  $path
     * @return string
     */
    	function cpf($cpf) {
            if (! $cpf) {
                return '';
            }
            if (strlen($cpf) == 11) {
                return substr($cpf, 0, 3) . '.' . substr($cpf, 3, 3) . '.' . substr($cpf, 6, 3) . '-' . substr($cpf, 9);
            }
            return $cpf;   
        }
    }

    if (! function_exists('cnpj')) {
    /**
     * Get the path to the base of the install.
     *
     * @param  string  $path
     * @return string
     */
        function cnpj($cnpj) {
            if (! $cnpj) {
                return '';
            }
            if (strlen($cnpj) == 14) {
                return substr($cnpj, 0, 2) . '.' . substr($cnpj, 2, 3) . '.' . substr($cnpj, 5, 3) . '/' . substr($cnpj, 8, 4) . '-' . substr($cnpj, 12, 2);
            }
            return $cnpj;
        }    
    }

    if (! function_exists('fone')) {
    /**
     * Get the path to the base of the install.
     *
     * @param  string  $path
     * @return string
     */
        function fone($fone) {
            if (! $fone) {
                return '';
            }
            if (strlen($fone) == 10) {
            return '(' . substr($fone, 0, 2) . ') ' . substr($fone, 2, 4) . '-' . substr($fone, 6);
            }
            if (strlen($fone) == 11) {
                return '(' . substr($fone, 0, 2) . ') ' . substr($fone, 2, 1) . ' ' . substr($fone, 3, 4) . '-' . substr($fone, 7);
            }
            return $fone;
        }    
    }

    if (! function_exists('databr')) {
    /**
     * Get the path to the base of the install.
     *
     * @param  string  $path
     * @return datetime
     */
        function databr($data){
            return date( 'd/m/Y' , strtotime($data));
        }
    }
}