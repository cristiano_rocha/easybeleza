<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suplementos extends Model
{
    protected $fillable = ['supl_valor', 'supl_descricao'];
    protected $table = 'suplementos';
    protected $primaryKey = 'supl_id';

    public function caixa()
    {
        return $this->hasOne('App\Caixa', 'cx_id', 'cx_id');
    }
}
