<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Item;
use App\Cliente;

class Comanda extends Model
{
    protected $guarded = ['cmd_id', 'created_at', 'update_at'];
    protected $table = 'comandas';
    protected $primaryKey = 'cmd_id';

    // Relacionamento 1 para N
    public function itens($cmd_id = FALSE)
    {
        return $this->hasMany('App\Item', 'cmd_id', 'cmd_id');
    }

    // Relacionamento 1 para N
    public function pagamentos($cmd_id = FALSE)
    {
        return $this->hasMany('App\Pagamento', 'cmd_id', 'cmd_id');
    }

    // Relacionamento N para 1
    public function cliente()
    {        
        return $this->hasOne('App\Cliente', 'cli_id', 'cli_id');

    }

    public function users()
    {        
        return $this->hasOne('App\Users', 'id', 'usu_id');
    }

    public function movcaixa()
    {
        return $this->hasOne('App\MovCaixa', 'movcx_id', 'movcx_id');
    }
}
