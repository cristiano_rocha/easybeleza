<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProdutoServico extends Model
{  
    protected $fillable = ['prosrv_nome','prosrv_codbarras','prosrv_descricao','prosrv_ucom','prosrv_custo','prosrv_preco']; // Retirado ,'prosrv_pcomissao'
    protected $guarded = ['prosrv_id', 'created_at', 'update_at'];
    protected $table = 'produtosservicos';
    protected $primaryKey = 'prosrv_id';

    public function item()
    {        
        return $this->belongsTo('App\Item', 'prosrv_id', 'prosrv_id');
    }

    // Relacionamento N para N
    public function produtoprofissional()
    {        
        return $this->hasOne('App\ProdutoProfissional', 'proprof_id', 'proprof_id');
    }
}
