<?php

namespace App\Presenters;

use App\Transformers\MovCaixaTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class MovCaixaPresenter
 *
 * @package namespace App\Presenters;
 */
class MovCaixaPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new MovCaixaTransformer();
    }
}
