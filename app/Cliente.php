<?php

namespace App;

use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Database\Eloquent\Model;

class cliente extends Model
{
    protected $fillable = ['prof_nome','cli_nascimento','cli_telefone','cli_cpfcnpj'];
    protected $table = 'clientes';
    protected $primaryKey = 'cli_id';
    protected $searchable = [
        'columns' => [

            'cliente.nome' => 10,
        ],
    ];

    // Relacionamento 1 para N
    public function comanda()
    {
        return $this->belongsTo('App\Item', 'cli_id','cli_id');
    }

    // Relacionamento 1 para N
    public function agenda()
    {
        return $this->belongsTo('App\Agenda', 'cli_id','cli_id');
    }

    public function users()
    {
        return $this->hasOne('App\Users','id','usu_id');
    }
}

