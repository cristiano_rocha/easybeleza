<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\MovCaixa;

/**
 * Class MovCaixaTransformer
 * @package namespace App\Transformers;
 */
class MovCaixaTransformer extends TransformerAbstract
{

    /**
     * Transform the MovCaixa entity
     * @param App\Entities\MovCaixa $model
     *
     * @return array
     */
    public function transform(MovCaixa $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
