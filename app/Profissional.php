<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profissional extends Model
{
    protected $fillable = ['prof_nome','prof_nascimento','quantprof_celularity'];
    protected $guarded = ['prof_id', 'created_at', 'update_at'];
    protected $table = 'profissionais';
    protected $primaryKey = 'prof_id';

    public function item()
    {        
        return $this->belongsTo('App\Item', 'prof_id', 'prof_id');
    }

    public function agenda()
    {        
        return $this->belongsTo('App\Agenda', 'prof_id', 'prof_id');
    }

    // Relacionamento 1 para N
    public function produtoprofissional()
    {
        return $this->belongsTo('App\ProdutoProfissional', 'prof_id','prof_id');
    }
}
