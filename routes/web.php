<?php

use Illuminate\Support\Facades\Input;
use App\ProdutoServico;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    Session::flash('message','Cache limpo! Obrigado.');
    return view('home');
});

$this->post('logout', 'Auth\LoginController@logout')->name('logout');

Auth::routes();

/*
**  Grupo protegido para usuários logados
**/
Route::group(['middleware' => 'auth'], function()
{	
	Route::get('/home', 'HomeController@index')->name('home');

	Route::get("jsonprod"              ,'ItemController@jsonprod');
	Route::get('ajaxprodutosservicos'  ,'ItemController@ajaxprodutosservicos');
	Route::get("prduncomplete"         ,'ItemController@prduncomplete');
	// Rotas Comanda
	Route::get('/comandas/{id}/fatura'  , 'ComandaController@fatura');
	Route::get('/comandas/{id}/faturar'  , 'ComandaController@faturar');
	Route::post('/comandas/{id}/pagar'  , 'ComandaController@pagar');
	
	Route::resource('/clientes'         ,'ClienteController');
	Route::resource('/comandas'         ,'ComandaController');
	Route::resource('/comandas-itens'    ,'ItemController');
	Route::resource('/profissionais'    ,'ProfissionalController');
	Route::resource('/produtosprofissionais' ,'ProdutoProfissionalController');
	Route::resource('/produtosservicos' ,'ProdutoServicoController');
	Route::resource('/comissoes'         ,'ComissaoController');

	// Rotas do Financeiro
	Route::get('/financeiro/movcaixa/abrir' ,'MovCaixaController@abrir', function() {
		return view('financeiro.caixa.movcaixa.abrir');
	});	

	Route::get('/financeiro/movcaixa/fechar', 'MovCaixaController@fechar', function() {
		return view('financeiro.caixa.movcaixa.fechar');
	});
	Route::post('abertura', array('as' => 'movcaixa.abertura','uses'=>'MovCaixaController@abertura'));
	Route::post('fechamento', array('as' => 'movcaixa.fechamento','uses' => 'MovCaixaController@fechamento'));
	Route::resource('financeiro/movcaixa'	,'MovCaixaController');
	Route::resource('financeiro/caixa'		,'CaixaController');
	Route::resource('/financeiro'			,'FinanceiroController');
	Route::resource('/retirada'			,'RetiradaController');
	Route::resource('/suplemento'		,'SuplementosController');
	
	Route::get("lista",'AgendaController@lista');
	Route::post("lista/busca",'AgendaController@busca');
	Route::resource('/agenda'		,'AgendaController');
	Route::get('search', array('as' => 'search', 'uses' => 'AgendaController@search'));
	Route::get('searchProf', array('as' => 'searchProf', 'uses' => 'AgendaController@searchProf'));

	Route::get('oauth', ['as' => 'oauthCallback', 'uses' => 'gCalendarController@oauth']);

	// Rotas AJAX
	Route::get('autocomplete-find', array('as' => 'autocomplete.find','uses'=>'ConsultaController@index'));
	Route::get('autocomplete-ajax', array('as' => 'autocomplete.ajax','uses'=>'ConsultaController@find'));

	//Layout 2.0
	/*Route::resource('testing/clientes',		'testing\ClientesController');
	Route::resource('testing/profissional',			'testing\ProfissionalController');
	Route::resource('testing/produtosservicos' ,'testing\ProdutoServicoController');
	Route::resource('testing/produtosprofissionais', 'testing\ProdutoProfissionalController');*/
	
	Route::get('/consultar', function() {
		return view('consulta.pesquisa');
	});
});

