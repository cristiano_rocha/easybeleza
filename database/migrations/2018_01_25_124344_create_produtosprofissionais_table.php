<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutosprofissionaisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produtosprofissionais', function (Blueprint $table) {
            $table->increments('proprof_id');

            $table->integer('prosrv_id')->unsigned();
            $table->foreign('prosrv_id')->references('prosrv_id')
                ->on('produtosservicos')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->integer('prof_id')->unsigned();
            $table->foreign('prof_id')->references('prof_id')
                ->on('profissionais')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produtosprofissionais');
    }
}
