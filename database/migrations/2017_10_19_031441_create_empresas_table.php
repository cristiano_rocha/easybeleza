<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas', function (Blueprint $table) {
            $table->increments('emp_id');
            $table->string('emp_nome', 120);
            $table->string('emp_fantasia', 120)->nullable();
            $table->string('emp_cpfcnpj', 14)->unique();
            $table->string('emp_ie', 20);
            $table->string('emp_iest', 20);
            $table->string('emp_im', 20);
            $table->string('emp_cnae', 120);
            $table->string('emp_crt', 10);
            $table->string('emp_tplogadouro', 30)->nullable();
            $table->string('emp_logadouro', 120)->nullable();
            $table->string('emp_numero', 10)->nullable();
            $table->string('emp_complemento', 60)->nullable();
            $table->string('emp_bairro', 10)->nullable();
            $table->string('emp_cep', 10)->nullable();
            $table->string('emp_cidade', 60)->nullable();
            $table->string('emp_estado', 2)->nullable();
            $table->string('emp_pais', 60)->nullable();
            $table->string('emp_telefone', 15)->nullable();
            $table->string('emp_email', 30)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empresas', function (Blueprint $table) {
            //
        });
    }
}
