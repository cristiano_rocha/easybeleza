<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagamentosTable extends Migration
{
    /**
     * Run the migrations.
     * 
     * @return void
     */
    public function up()
    {
        Schema::create('pagamentos', function (Blueprint $table) {
            $table->increments('pag_id');
            $table->integer('cmd_id')->unsigned();
            $table->foreign('cmd_id')->references('cmd_id')
                  ->on('comandas')
                  ->onDelete('cascade')
                  ->onUpdate('no action');
            $table->string('pag_historico',150);
            $table->integer('tpg_id')->unsigned();
            $table->foreign('tpg_id')->references('tpg_id')
                  ->on('tipospagamentos')
                  ->onDelete('cascade')
                  ->onUpdate('no action');
            $table->integer('pag_num');
            $table->integer('pag_qtdedias');
            $table->date('pag_vencimento');
            $table->date('pag_dtpagamento');
            $table->string('pag_numcheque',20);
            $table->string('pag_agcheque',20);
            $table->string('pag_cccheque',20);
            $table->string('pag_cpfcheque',20);
            $table->string('pag_emitcheque',20);
            $table->string('pag_numordcard',20);
            $table->integer('bco_id');
            $table->integer('car_id');
            $table->float('pag_valor');
            $table->float('pag_vlrpago');
            $table->boolean('status');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pagamentos');
    }
}
