<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComandasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comandas', function (Blueprint $table) {
            $table->increments('cmd_id');
            $table->integer('cli_id')->unsigned();
            $table->foreign('cli_id')->references('cli_id')->on('clientes');            
            $table->integer('usu_id')->unsigned();
            $table->foreign('usu_id')->references('id')->on('users');
            $table->date('cmd_data');                   
            $table->float('cmd_subtotal');
            $table->float('cmd_total');
            $table->integer('mov_id')->unsigned();
            $table->foreign('mov_id')->references('mov_id')->on('movcaixas');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comandas');
    }
}