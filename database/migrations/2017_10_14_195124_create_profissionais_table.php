<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfissionaisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profissionais', function (Blueprint $table) {
            $table->increments('prof_id');
            $table->integer('usu_id')->unsigned();
            $table->foreign('usu_id')->references('id')->on('users');
            $table->string('prof_cpf', 15);
            $table->string('prof_rg', 15);
            $table->string('prof_nome', 120);
            $table->date('prof_nascimento');
            $table->string('prof_celular', 15);
            $table->string('prof_email', 50);   
            $table->string('prof_endereco', 150);
            $table->string('prof_cep', 10);
            $table->string('prof_cidade', 150);
            $table->string('prof_bairro', 150);
            $table->string('prof_estado', 15);         
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profissionais');
    }
}