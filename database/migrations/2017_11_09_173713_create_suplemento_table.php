<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuplementoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suplementos', function (Blueprint $table) {
            $table->increments('supl_id');
            $table->integer('movcx_id')->unsigned();
            $table->foreign('movcx_id')->references('movcx_id')->on('movcaixas')->onDelete('cascade');
            $table->string('supl_descricao');
            $table->integer('usu_id')->unsigned();
            $table->foreign('usu_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('emp_id');
            $table->float('supl_valor');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suplementos');
    }
}
