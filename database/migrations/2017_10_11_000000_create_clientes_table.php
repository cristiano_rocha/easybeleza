<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Clientes', function (Blueprint $table) {
            
            $table->increments('cli_id');
            $table->integer('usu_id')->unsigned();
            $table->foreign('usu_id')->references('id')->on('users');
            $table->string('cli_nome', 120);
            $table->date('cli_nascimento');
            $table->string('cli_rg', 15);
            $table->string('cli_cpfcnpj', 60)->unique();
            $table->string('cli_telefone', 15);
            $table->string('cli_email', 50);
            $table->string('cli_endereco', 150);
            $table->string('cli_cep', 10);
            $table->string('cli_cidade', 150);
            $table->string('cli_bairro', 150);
            $table->string('cli_estado', 15);
            // $table->date('cli_dt_add');
            $table->softDeletes();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Clientes');
    }
}