<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgendasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agendas', function (Blueprint $table) {
            $table->increments('agd_id');
            $table->integer('cli_id')->unsigned();
            $table->foreign('cli_id')->references('cli_id')
                ->on('Clientes')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->integer('usu_id')->unsigned();
            $table->foreign('usu_id')->references('id')
                ->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->integer('prof_id')->unsigned();
            $table->foreign('prof_id')->references('prof_id')
                ->on('profissionais')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->integer('prosrv_id')->unsigned();
            $table->foreign('prosrv_id')->references('prosrv_id')
                ->on('produtosservicos')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->integer('emp_id')->unsigned();            
            $table->foreign('emp_id')->references('emp_id')
                ->on('empresas')
                ->onDelete('no action')
                ->onUpdate('no action');
                
            $table->date('agd_data');
            $table->time('agd_hora');
            $table->time('agd_horafim')->nultabe;
            $table->string('agd_eid')->nultabe;
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agendas');
    }
}
