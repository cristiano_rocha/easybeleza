<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AltUsuIdToItens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('itens', 'usu_id'))
        {
            Schema::table('itens', function (Blueprint $table)
            {
                $table->dropColumn('usu_id');
            });
        }

        Schema::table('itens', function (Blueprint $table) {
            $table->integer('usu_id')->unsigned();
            $table->foreign('usu_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('itens', 'usu_id'))
        {
            Schema::table('itens', function (Blueprint $table)
            {
                $table->dropColumn('usu_id');
            });
        }
    }
}
