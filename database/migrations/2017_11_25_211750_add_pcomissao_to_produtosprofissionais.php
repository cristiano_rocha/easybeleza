<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPcomissaoToProdutosprofissionais extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('produtosprofissionais', function($table) {
        $table->float('proprof_pcomissao');
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('produtosprofissionais', function($table) {
        $table->dropColumn('proprof_pcomissao');
    });
    }
}
