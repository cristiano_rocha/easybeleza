<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdicionadoNovosCamposMovcaixas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('movcaixas', function (Blueprint $table) {
            $table->float('mov_dinheiroinfo');
            $table->float('mov_duplicatainfo');
            $table->float('mov_chequeinfo');
            $table->float('mov_cartaoinfo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('movcaixas', function (Blueprint $table) {
            //
        });
    }
}
