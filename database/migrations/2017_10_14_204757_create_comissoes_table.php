<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComissoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Comissoes', function (Blueprint $table) {
            $table->increments('cms_id');                   
            $table->integer('prof_id');          
            $table->integer('cli_id')->unsigned();
            $table->foreign('cli_id')->references('cli_id')->on('Clientes');            
            $table->integer('prosrv_id')->unsigned();
            $table->foreign('prosrv_id')->references('prosrv_id')->on('ProdutosServicos');
            $table->date('cms_data');                       
            $table->float('cms_qtde');
            $table->float('cms_valor');
            $table->float('cms_pcomissao');
            $table->float('cms_vcomissao');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comissoes');
    }
}