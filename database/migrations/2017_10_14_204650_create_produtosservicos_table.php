<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutosservicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ProdutosServicos', function (Blueprint $table) {
            $table->increments('prosrv_id');
            $table->integer('usu_id')->unsigned();
            $table->foreign('usu_id')->references('id')->on('users');            
            $table->string('prosrv_codbarras', 40)->unique();           
            $table->string('prosrv_nome', 120);
            $table->string('prosrv_descricao', 120);            
            $table->string('prosrv_ucom', 20);
            $table->float('prosrv_custo', 10);
            $table->float('prosrv_preco', 10);
            $table->float('prosrv_pcomissao', 19);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produtosservicos');
    }
}