<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMovcaixasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('movcaixas', function (Blueprint $table) {
            $table->renameColumn('cx_saldo_ini',   'mov_saldo_ini');
            $table->renameColumn('cx_sangria',     'mov_sangria');
            $table->renameColumn('cx_suplemento',  'mov_suplemento');
            $table->renameColumn('cx_totalvenda',  'mov_totalvenda');
            $table->renameColumn('cx_pago',        'mov_pago');
            $table->renameColumn('cx_dinheiro',    'mov_dinheiro');
            $table->renameColumn('cx_duplicata',   'mov_duplicata');
            $table->renameColumn('cx_cheque',      'mov_cheque');
            $table->renameColumn('cx_cartao',      'mov_cartao');
            $table->renameColumn('cx_desconto',    'mov_desconto');
            $table->renameColumn('cx_totalliquido','mov_totalliquido');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('movcaixas', function (Blueprint $table) {
            $table->renameColumn('mov_saldo_ini',    'cx_saldo_ini');
            $table->renameColumn('mov_sangria',      'cx_sangria');
            $table->renameColumn('mov_suplemento',   'cx_suplemento');
            $table->renameColumn('mov_totalvenda',   'cx_totalvenda');
            $table->renameColumn('mov_pago',         'cx_pago');
            $table->renameColumn('mov_dinheiro',     'cx_dinheiro');
            $table->renameColumn('mov_duplicata',    'cx_duplicata');
            $table->renameColumn('mov_cheque',       'cx_cheque');
            $table->renameColumn('mov_cartao',       'cx_cartao');
            $table->renameColumn('mov_desconto',     'cx_desconto');
            $table->renameColumn('mov_totalliquido', 'cx_totalliquido');
        });
    }
}