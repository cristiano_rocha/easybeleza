<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRetiradaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('retirada', function (Blueprint $table) {
            $table->increments('re_id');
            $table->integer('cx_id')->unsigned();
            $table->foreign('cx_id')->references('cx_id')->on('caixas')->onDelete('cascade');
            $table->string('re_descricao');
            $table->integer('usu_id')->unsigned();
            $table->foreign('usu_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('emp_id');
            $table->float('re_valor');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('retirada');
    }
}
