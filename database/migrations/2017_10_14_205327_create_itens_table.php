<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('itens', function (Blueprint $table) {
            $table->increments('itn_id');
            $table->integer('cmd_id')->unsigned();
            $table->foreign('cmd_id')->references('cmd_id')->on('Comandas');            
            $table->integer('prof_id');          
            $table->integer('prosrv_id')->unsigned();
            $table->foreign('prosrv_id')->references('prosrv_id')->on('ProdutosServicos');
            $table->date('itn_data');
            $table->float('itn_custo')->nultable;
            $table->float('itn_preco');
            $table->float('itn_qtde');
            $table->float('itn_subtotal');
            $table->float('itn_desconto')->nultable;          
            $table->string('itn_ucom', 20);
            $table->float('itn_total');
            $table->date('itn_dt_alt')->nultable;            
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('itens');
    }
}