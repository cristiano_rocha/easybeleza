<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovCaixasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movcaixas', function (Blueprint $table) {
            $table->increments('movcx_id');
            $table->integer('cx_id')->unsigned();
            $table->foreign('cx_id')->references('cx_id')->on('caixas');
            $table->float('cx_saldo_ini');
            $table->float('cx_sangria');
            $table->float('cx_suplemento');
            $table->float('cx_totalvenda');
            $table->float('cx_pago');
            $table->float('cx_dinheiro');
            $table->float('cx_duplicata');
            $table->float('cx_cheque');
            $table->float('cx_cartao');
            $table->float('cx_desconto');
            $table->float('cx_totalliquido');
            $table->boolean('status');       
            $table->integer('usu_id')->unsigned();
            $table->foreign('usu_id')->references('id')->on('users');
            $table->integer('usu_id_fechou');
            $table->integer('emp_id')->unsigned();
            $table->date('dt_fecha');
            $table->date('dt_aberto');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movcaixas');
    }
}
