<?php

use Illuminate\Database\Seeder;

class EmpresasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	
        DB::table('empresas')->insert([
            'emp_nome'       => 'SiD Soluções LTDA',
            'emp_fantasia'   => 'SiD Soluções', 
            'emp_cpfcnpj'    => ''.mt_rand().'0165', 
            'emp_ie' 	     => mt_rand(), 
            'emp_iest' 	     => '', 
            'emp_im'         => '',
            'emp_cnae'       => '',
            'emp_crt'        => '', 
            'emp_tplogadouro'=> 'RUA', 
            'emp_logadouro'  => 'OTAVIO MANGABEIRA', 
            'emp_numero'     => '218', 
            'emp_complemento'=> '', 
            'emp_bairro'     => 'CENTRO', 
            'emp_cep'        => '45560000', 
            'emp_cidade'     => 'BARRA DO ROCHA',
            'emp_estado'	 => 'BA', 
            'emp_pais'		 => 'BRASIL', 
            'emp_telefone'   => '87999658729', 
            'emp_email'      => str_random(10).'@gmail.com',
            'emp_gtoken'     => '',
        ]);

        // Exibe uma informação no console durante o processo de seed
        $this->command->info('Empresa SiD criada!');
    }
}
