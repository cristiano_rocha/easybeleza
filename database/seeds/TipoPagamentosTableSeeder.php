<?php

use Illuminate\Database\Seeder;

class TipoPagamentosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Cria Dinheiro
        DB::table('tipospagamentos')->insert([
        	'tpg_id'     => 1,
        	'tpg_nome'   => 'Dinheiro',
        	'tpg_mobile' => 1,
        	'status'     => 1,
        ]);
        // Cria Duplicata
        DB::table('tipospagamentos')->insert([
            'tpg_id'     => 2,
            'tpg_nome'   => 'Duplicata',
            'tpg_mobile' => 1,
            'status'     => 1,
        ]);
        // Cria Cheque
        DB::table('tipospagamentos')->insert([
            'tpg_id'     => 3,
            'tpg_nome'   => 'Cheque',
            'tpg_mobile' => 1,
            'status'     => 1,
        ]);
        // Cria Cartão
        DB::table('tipospagamentos')->insert([
            'tpg_id'     => 4,
            'tpg_nome'   => 'Cartão',
            'tpg_mobile' => 1,
            'status'     => 1,
        ]);

        // Exibe uma informação no console durante o processo de seed
        $this->command->info('Tipos de pagamento criados!');
    }
}
