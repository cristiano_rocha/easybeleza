-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Tempo de geração: 12/11/2018 às 16:37
-- Versão do servidor: 5.7.24
-- Versão do PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `easybele_easybeleza`
--
CREATE DATABASE IF NOT EXISTS `easybele_easybeleza` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `easybele_easybeleza`;

-- --------------------------------------------------------

--
-- Estrutura para tabela `agendas`
--

CREATE TABLE IF NOT EXISTS `agendas` (
  `agd_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cli_id` int(10) UNSIGNED NOT NULL,
  `usu_id` int(10) UNSIGNED NOT NULL,
  `prof_id` int(10) UNSIGNED NOT NULL,
  `prosrv_id` int(10) UNSIGNED NOT NULL,
  `emp_id` int(10) UNSIGNED NOT NULL,
  `agd_data` date NOT NULL,
  `agd_hora` time NOT NULL,
  `agd_horafim` time NOT NULL,
  `agd_eid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`agd_id`),
  KEY `agendas_cli_id_foreign` (`cli_id`),
  KEY `agendas_emp_id_foreign` (`emp_id`),
  KEY `agendas_prof_id_foreign` (`prof_id`),
  KEY `agendas_prosrv_id_foreign` (`prosrv_id`),
  KEY `agendas_usu_id_foreign` (`usu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `agendas`
--

INSERT INTO `agendas` (`agd_id`, `cli_id`, `usu_id`, `prof_id`, `prosrv_id`, `emp_id`, `agd_data`, `agd_hora`, `agd_horafim`, `agd_eid`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 465, 4, 10, 39, 2, '2018-01-16', '16:00:00', '16:00:00', 'dj08v44nma9hf10h9h4n15v138', NULL, '2018-01-16 19:07:52', '2018-01-16 19:07:52'),
(2, 133, 4, 10, 42, 2, '2018-01-17', '08:00:00', '08:00:00', '4bfcl0bcc6gncj624cqki22mr0', NULL, '2018-01-16 20:32:28', '2018-01-16 20:32:28'),
(3, 133, 4, 14, 39, 2, '2018-01-17', '08:00:00', '08:00:00', '5r3bsscf0a7c3r3l42kfg74290', NULL, '2018-01-16 20:34:08', '2018-01-16 20:34:08'),
(4, 978, 4, 14, 39, 2, '2018-01-17', '11:00:00', '11:00:00', 'sq7ud01ic6o97bceptjpg70ee4', NULL, '2018-01-16 20:35:28', '2018-01-16 20:35:28'),
(5, 83, 4, 10, 42, 2, '2018-01-19', '09:00:00', '09:00:00', '403qoi61ct9bs6c13on80adnbo', NULL, '2018-01-18 14:45:42', '2018-01-18 14:45:42'),
(6, 1070, 4, 14, 39, 2, '2018-01-19', '09:00:00', '09:00:00', '3nv1s3ljcgnmvrvspeqcsab894', NULL, '2018-01-18 14:46:38', '2018-01-18 14:46:38'),
(7, 257, 4, 14, 42, 2, '2018-01-19', '10:00:00', '10:00:00', 'e6agmgepjdvi5550in82pakfns', NULL, '2018-01-18 14:52:43', '2018-01-18 14:52:43'),
(8, 27, 4, 13, 39, 2, '2018-01-19', '10:00:00', '10:00:00', 'rbi3k6mm4ei2dl1v8v1953n6kg', NULL, '2018-01-18 14:54:10', '2018-01-18 14:54:10'),
(9, 15, 4, 14, 39, 2, '2018-01-19', '11:00:00', '11:00:00', '6sg3mageg8pbepsgcqdtj3qs4k', NULL, '2018-01-18 14:55:43', '2018-01-18 14:55:43'),
(10, 15, 4, 10, 42, 2, '2018-01-19', '11:00:00', '11:00:00', 'j4k07r15v5v594fffqqu5u380g', NULL, '2018-01-18 15:10:24', '2018-01-18 15:10:24'),
(11, 261, 4, 14, 42, 2, '2018-01-19', '13:00:00', '13:00:00', 'bgg0htjrqv14ec1tr5kvghm9ok', NULL, '2018-01-18 15:15:58', '2018-01-18 15:15:58'),
(12, 23, 4, 13, 42, 2, '2018-01-19', '16:00:00', '16:00:00', 'v00f0shuejn8us258kqascft14', NULL, '2018-01-18 16:06:19', '2018-01-18 16:06:19'),
(13, 1075, 4, 14, 68, 2, '2018-01-19', '14:00:00', '14:00:00', 'n36ht0qnvisskj5b0qhrncbkv0', NULL, '2018-01-18 16:15:07', '2018-01-18 16:15:07'),
(14, 1075, 4, 14, 68, 2, '2018-01-19', '15:00:00', '15:00:00', '78c2ceb2eqp21kolv6rr8fr98s', NULL, '2018-01-18 16:18:04', '2018-01-18 16:18:04'),
(15, 1075, 4, 14, 68, 2, '2018-01-19', '16:00:00', '16:00:00', 'mlflfl2bdoge7f747p62gkf7l0', NULL, '2018-01-18 16:26:12', '2018-01-18 16:26:12'),
(16, 347, 4, 3, 33, 2, '2018-01-19', '18:00:00', '18:00:00', 'fsvmum2u6pla6h4d6p0dqi37ng', NULL, '2018-01-18 16:28:34', '2018-01-18 16:28:34'),
(17, 347, 4, 2, 7, 2, '2018-01-19', '18:00:00', '18:00:00', 'ado7larbtfq2pids671gvf1fm4', NULL, '2018-01-18 16:30:09', '2018-01-18 16:30:09'),
(18, 797, 4, 7, 22, 2, '2018-01-19', '13:00:00', '13:00:00', 'njjlvh22153fgb28pk9oosp0rc', NULL, '2018-01-18 16:32:02', '2018-01-18 16:32:02'),
(19, 681, 4, 10, 42, 2, '2018-01-19', '15:00:00', '15:00:00', 'flrms8of89er9gtknekfd4gqa4', NULL, '2018-01-18 16:33:20', '2018-01-18 16:33:20'),
(20, 1076, 4, 10, 42, 2, '2018-01-19', '14:00:00', '14:00:00', 'bfjo75qtfsjc44u30ma9e7faes', NULL, '2018-01-18 16:56:30', '2018-01-18 16:56:30'),
(21, 476, 4, 14, 42, 2, '2018-01-19', '08:00:00', '08:00:00', 'cqdj9egkk17rpbmrgnnqt4a8as', NULL, '2018-01-18 17:00:30', '2018-01-18 17:00:30'),
(22, 69, 4, 13, 42, 2, '2018-01-20', '08:00:00', '08:00:00', '0', NULL, '2018-01-19 17:41:29', '2018-01-19 17:41:29'),
(23, 69, 4, 14, 39, 2, '2018-01-20', '08:00:00', '08:00:00', '0', NULL, '2018-01-19 17:42:10', '2018-01-19 17:42:10'),
(24, 69, 4, 14, 39, 2, '2018-01-20', '08:00:00', '08:00:00', '0', NULL, '2018-01-19 17:43:39', '2018-01-19 17:43:39'),
(25, 861, 4, 8, 69, 2, '2018-01-29', '11:00:00', '11:00:00', '0', NULL, '2018-01-29 15:56:03', '2018-01-29 15:56:03'),
(26, 861, 4, 8, 69, 2, '2018-01-29', '13:00:00', '13:00:00', '0', NULL, '2018-01-29 15:56:50', '2018-01-29 15:56:50'),
(27, 861, 4, 8, 69, 2, '2018-01-29', '14:00:00', '14:00:00', '0', NULL, '2018-01-29 15:57:56', '2018-01-29 15:57:56'),
(28, 227, 4, 8, 69, 2, '2018-01-29', '15:00:00', '15:00:00', '0', NULL, '2018-01-29 16:03:43', '2018-01-29 16:03:43'),
(29, 227, 4, 8, 69, 2, '2018-01-29', '16:00:00', '16:00:00', '0', NULL, '2018-01-29 16:04:55', '2018-01-29 16:04:55'),
(30, 861, 4, 8, 69, 2, '2018-01-29', '11:00:00', '11:00:00', '0', NULL, '2018-01-29 16:10:15', '2018-01-29 16:10:15'),
(31, 319, 4, 8, 49, 2, '2018-01-29', '17:00:00', '17:00:00', '0', NULL, '2018-01-29 16:11:49', '2018-01-29 16:11:49'),
(32, 319, 4, 8, 49, 2, '2018-01-29', '18:00:00', '18:00:00', '0', NULL, '2018-01-29 16:15:08', '2018-01-29 16:15:08'),
(33, 70, 4, 8, 49, 2, '2018-02-03', '08:00:00', '10:00:00', '0', NULL, '2018-02-02 20:27:04', '2018-02-02 20:27:04'),
(34, 70, 4, 8, 50, 2, '2017-03-09', '09:00:00', '10:00:00', '0', NULL, '2018-02-02 20:37:44', '2018-02-02 20:37:44'),
(35, 70, 4, 8, 50, 2, '2018-02-03', '08:00:00', '10:00:00', '0', NULL, '2018-02-02 20:43:15', '2018-02-02 20:43:15'),
(36, 1078, 6, 17, 70, 2, '2018-01-03', '09:00:00', '08:00:00', '0', NULL, '2018-02-03 02:45:41', '2018-02-03 02:45:41'),
(42, 824, 4, 5, 19, 2, '2018-03-03', '08:00:00', '10:00:00', '0', NULL, '2018-02-28 17:14:25', '2018-02-28 17:14:25'),
(43, 379, 4, 8, 74, 2, '2018-03-01', '08:00:00', '10:00:00', '0', NULL, '2018-02-28 17:25:28', '2018-02-28 17:25:28'),
(44, 18, 4, 8, 39, 2, '2018-03-01', '10:00:00', '11:00:00', '0', NULL, '2018-02-28 17:29:19', '2018-02-28 17:29:58'),
(45, 14, 4, 8, 39, 2, '2018-03-01', '11:00:00', '13:00:00', '0', NULL, '2018-02-28 17:33:07', '2018-02-28 17:33:07'),
(46, 916, 4, 8, 74, 2, '2018-03-01', '13:00:00', '15:00:00', '0', NULL, '2018-02-28 17:36:48', '2018-02-28 17:36:48'),
(47, 46, 4, 8, 39, 2, '2018-03-01', '15:00:00', '16:00:00', '0', NULL, '2018-02-28 17:38:24', '2018-02-28 17:38:24'),
(48, 46, 4, 10, 42, 2, '2018-03-01', '15:00:00', '16:00:00', '0', NULL, '2018-02-28 17:41:54', '2018-02-28 17:41:54'),
(49, 72, 4, 13, 42, 2, '2018-03-01', '15:00:00', '16:00:00', '0', NULL, '2018-02-28 17:44:09', '2018-02-28 17:44:09'),
(50, 72, 4, 13, 42, 2, '2018-03-01', '15:00:00', '16:00:00', '0', NULL, '2018-02-28 17:44:10', '2018-02-28 17:44:10'),
(51, 14, 4, 14, 42, 2, '2018-03-01', '11:00:00', '13:00:00', '0', NULL, '2018-02-28 17:46:48', '2018-02-28 17:46:48'),
(53, 373, 4, 2, 4, 2, '2018-03-01', '15:00:00', '17:00:00', '0', NULL, '2018-02-28 18:07:01', '2018-02-28 20:06:44'),
(55, 607, 4, 1, 9, 2, '2018-03-01', '16:00:00', '18:00:00', '0', NULL, '2018-02-28 18:22:45', '2018-02-28 18:22:45'),
(57, 228, 4, 8, 42, 2, '2018-03-02', '08:00:00', '08:00:00', '0', NULL, '2018-02-28 18:41:32', '2018-02-28 18:41:32'),
(58, 1070, 4, 8, 39, 2, '2018-03-02', '09:00:00', '10:00:00', '0', NULL, '2018-02-28 18:45:26', '2018-02-28 18:45:26'),
(59, 83, 4, 10, 42, 2, '2018-03-02', '09:00:00', '10:00:00', '0', NULL, '2018-02-28 18:46:37', '2018-02-28 18:46:37'),
(60, 257, 4, 8, 39, 2, '2018-03-02', '10:00:00', '11:00:00', '0', NULL, '2018-02-28 18:48:05', '2018-02-28 18:48:05'),
(61, 257, 4, 14, 42, 2, '2018-03-02', '10:00:00', '11:00:00', '0', NULL, '2018-02-28 18:54:56', '2018-02-28 18:54:56'),
(62, 147, 4, 8, 39, 2, '2018-03-02', '11:00:00', '11:00:00', '0', NULL, '2018-02-28 19:02:50', '2018-02-28 19:02:50'),
(63, 261, 4, 8, 74, 2, '2018-03-02', '11:00:00', '14:00:00', '0', NULL, '2018-02-28 19:05:37', '2018-02-28 19:05:37'),
(64, 139, 4, 8, 74, 2, '2018-03-02', '08:00:00', '08:00:00', '0', NULL, '2018-02-28 19:12:59', '2018-02-28 19:48:53'),
(65, 607, 4, 1, 9, 2, '2018-03-01', '17:00:00', '18:00:00', '0', NULL, '2018-02-28 20:03:45', '2018-02-28 20:03:45'),
(66, 546, 4, 8, 74, 2, '2018-03-02', '16:00:00', '18:00:00', '0', NULL, '2018-02-28 20:17:39', '2018-02-28 20:17:39'),
(67, 136, 4, 8, 74, 2, '2018-03-02', '18:00:00', '19:00:00', '0', NULL, '2018-02-28 21:25:42', '2018-02-28 21:25:42'),
(68, 45, 4, 8, 39, 2, '2018-02-03', '10:00:00', '11:00:00', '0', NULL, '2018-02-28 21:33:04', '2018-02-28 21:33:04'),
(69, 45, 4, 8, 39, 2, '2018-03-03', '10:00:00', '11:00:00', '0', NULL, '2018-03-01 17:30:29', '2018-03-01 17:30:29'),
(70, 670, 4, 10, 39, 2, '2018-03-03', '09:00:00', '10:00:00', '0', NULL, '2018-03-01 17:35:38', '2018-03-01 17:35:38'),
(71, 670, 4, 13, 42, 2, '2018-03-03', '09:00:00', '10:00:00', '0', NULL, '2018-03-01 17:37:48', '2018-03-01 17:37:48'),
(72, 806, 4, 13, 42, 2, '2018-03-03', '10:00:00', '11:00:00', '0', NULL, '2018-03-01 18:53:08', '2018-03-01 18:53:08'),
(73, 59, 4, 8, 74, 2, '2018-03-03', '11:00:00', '13:00:00', '0', NULL, '2018-03-01 19:41:55', '2018-03-01 19:41:55'),
(74, 28, 4, 8, 39, 2, '2018-03-03', '13:00:00', '14:00:00', '0', NULL, '2018-03-01 19:43:55', '2018-03-01 19:43:55'),
(75, 457, 4, 8, 39, 2, '2018-03-03', '14:00:00', '15:00:00', '0', NULL, '2018-03-01 19:45:12', '2018-03-01 19:45:12'),
(76, 347, 4, 8, 39, 2, '2018-03-03', '15:00:00', '16:00:00', '0', NULL, '2018-03-01 20:02:28', '2018-03-01 20:02:28'),
(77, 193, 4, 8, 74, 2, '2018-03-03', '16:00:00', '18:00:00', '0', NULL, '2018-03-01 20:04:18', '2018-03-01 20:04:18'),
(78, 450, 4, 8, 39, 2, '2018-03-03', '18:00:00', '19:00:00', '0', NULL, '2018-03-01 20:15:46', '2018-03-01 20:15:46'),
(79, 462, 4, 10, 42, 2, '2018-03-03', '08:00:00', '08:00:00', '0', NULL, '2018-03-01 20:37:30', '2018-03-01 20:38:23'),
(80, 59, 4, 13, 42, 2, '2018-03-03', '11:00:00', '11:00:00', '0', NULL, '2018-03-01 21:03:58', '2018-03-01 21:03:58'),
(81, 28, 4, 8, 74, 2, '2018-04-14', '13:00:00', '15:00:00', '0', NULL, '2018-03-12 21:00:15', '2018-03-12 21:00:15');

-- --------------------------------------------------------

--
-- Estrutura para tabela `caixas`
--

CREATE TABLE IF NOT EXISTS `caixas` (
  `cx_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cx_descricao` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emp_id` int(10) UNSIGNED DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `usu_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`cx_id`),
  KEY `caixas_usu_id_foreign` (`usu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `caixas`
--

INSERT INTO `caixas` (`cx_id`, `cx_descricao`, `emp_id`, `status`, `usu_id`, `created_at`, `updated_at`) VALUES
(1, 'Caixa EasyBeleza', 1, 1, 1, '2017-11-09 22:56:48', '2017-11-21 14:08:14'),
(2, 'Caixa geral', 2, 1, 4, '2017-11-09 22:56:48', '2017-11-09 22:56:48');

-- --------------------------------------------------------

--
-- Estrutura para tabela `clientes`
--

CREATE TABLE IF NOT EXISTS `clientes` (
  `cli_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cli_cpfcnpj` varchar(14) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cli_nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cli_endereco` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cli_cep` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cli_cidade` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cli_estado` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cli_bairro` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cli_rg` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cli_email` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cli_telefone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cli_dt_add` date DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `emp_id` int(10) UNSIGNED NOT NULL,
  `usu_id` int(11) NOT NULL,
  `cli_nascimento` date DEFAULT NULL,
  PRIMARY KEY (`cli_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1079 AVG_ROW_LENGTH=84 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `clientes`
--

INSERT INTO `clientes` (`cli_id`, `cli_cpfcnpj`, `cli_nome`, `cli_endereco`, `cli_cep`, `cli_cidade`, `cli_estado`, `cli_bairro`, `cli_rg`, `cli_email`, `cli_telefone`, `cli_dt_add`, `deleted_at`, `created_at`, `updated_at`, `emp_id`, `usu_id`, `cli_nascimento`) VALUES
(1, '01135383405', 'KENIA CALINE DA SILVA', '', '', NULL, NULL, NULL, '', '', '7499165046', NULL, NULL, NULL, NULL, 2, 0, '1982-05-11'),
(2, '84363495553', 'GRAZIELA BENEVIDES DE AZEVEDO', '', '', NULL, NULL, NULL, '', 'GRAZIAZE@hotmail.com', '', NULL, NULL, NULL, NULL, 2, 0, '1998-06-26'),
(3, '04409643509', 'INDIRA DA PAIXAO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1986-12-08'),
(4, '09039733406', 'TATIANA DE SOUZA GAMA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1997-02-19'),
(5, '01962265463', 'NIOBE ALBUQUERQUE', '', '', NULL, NULL, NULL, '', 'niobealbuquerque@hotmail.com', '', NULL, NULL, NULL, NULL, 2, 0, '0190-04-18'),
(6, '4960268344', 'THAIS HELENA T SECHE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1967-01-16'),
(7, '984 879 224 49', 'ADRIANA DE CALDAS PEREIRA FALCA', '', '', NULL, NULL, NULL, '', 'adrianasanderfalcao@hotmail.com', '', NULL, NULL, NULL, NULL, 2, 0, '1900-10-11'),
(8, '', 'RAQUINEIDE GOMES DA SILVA', '', '', NULL, NULL, NULL, '', 'rakneidegomes@hotmail.com', '', NULL, NULL, NULL, NULL, 2, 0, '1989-11-30'),
(9, '88405796991', 'LUCIMARA VITORI DE  CAMPOS', '', '', NULL, NULL, NULL, '', 'LUCYMARACAMPOS@hotmail.com', '', NULL, NULL, NULL, NULL, 2, 0, '1970-02-27'),
(10, '', 'CARINA ALVES DE SOLZA', '', '', NULL, NULL, NULL, '', 'CARINAKANVC@hotmail.com', '', NULL, NULL, NULL, NULL, 2, 0, '1996-12-05'),
(11, '', 'MARIA SELMA ALVES SILVA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1969-02-12'),
(12, '00800351401', 'SAMANTHA MENDES FREIRE', '', '', NULL, NULL, NULL, '', 'SMFREIRES@hotmail.com', '87991452459', NULL, NULL, NULL, NULL, 2, 0, '1980-06-19'),
(13, '01463176376', 'JAQUELINE NASCIMENTO SOUZA', '', '', NULL, NULL, NULL, '', 'JAQUELINESOLZA@IMBRAPABR', '', NULL, NULL, NULL, NULL, 2, 0, '1986-06-29'),
(14, '35956780444', 'SOCORRO SANTOS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1990-05-06'),
(15, '09745884405', 'JAQUELINE LIRA MARQUES', '', '', NULL, NULL, NULL, '', 'JACKELINY_66@hotmail.com', '', NULL, NULL, NULL, NULL, 2, 0, '1991-11-04'),
(16, '', 'KARLA RODRIGUES', '', '', NULL, NULL, NULL, '', 'CARLACAVALCANTI27@YAHOOCOMBR', '', NULL, NULL, NULL, NULL, 2, 0, '1990-07-07'),
(17, '05268601466', 'TATIANE MONIQUE PEREIRA DE SA BARRETO', '', '', NULL, NULL, NULL, '', 'TATYMONIK@hotmail.com', '', NULL, NULL, NULL, NULL, 2, 0, '1990-05-16'),
(18, '81150245549', 'MARIA AMERICA SENTOSE  VALVERDE', '', '', NULL, NULL, NULL, '', 'MECAVALVERDE@hotmail.com', '', NULL, NULL, NULL, NULL, 2, 0, '1983-06-25'),
(19, '68765382515', 'ELAINE CLM RODRIGUES', '', '', NULL, NULL, NULL, '', 'ELAINYMASCEDO@hotmail.com', '', NULL, NULL, NULL, NULL, 2, 0, '1975-07-11'),
(20, '', 'CAROLINE TONIZZO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(21, '30243211600', 'ISMARINA DE FATIMA SILVA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(22, '073 848 754 63', 'SARAH GABRIELA DA COSTA AGUIAR', '', '', NULL, NULL, NULL, '', 'SARAH _AGUIAR@hotmail.comBR', '8799200200', NULL, NULL, NULL, NULL, 2, 0, '1990-03-12'),
(23, '02685068414', 'FABIANA RIBEIRO', '', '', NULL, NULL, NULL, '', 'FABIANARIBEIRO@hotmail.com', '87988783195', NULL, NULL, NULL, NULL, 2, 0, '1990-02-06'),
(24, '10289704472', 'MARIA DO CARMO SANTOS SOUZA (CARMINHA)', '', '', NULL, NULL, NULL, '', 'CARMOPETRO1@hotmail.com', '38612992', NULL, NULL, NULL, NULL, 2, 0, '1990-10-17'),
(25, '', 'ROSEANE ALVES GONDIM', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1990-05-12'),
(26, '', 'ELIANE DE ARAUJO SOUZA MARIZO', '', '', NULL, NULL, NULL, '', '', '87999982899', NULL, NULL, NULL, NULL, 2, 0, '1990-06-18'),
(27, '', 'TARCIANA TORRES SILVA', '', '', NULL, NULL, NULL, '', 'TARCIANATORRES@Gmail.com', '', NULL, NULL, NULL, NULL, 2, 0, '1971-05-04'),
(28, '06509192462', 'ALINA REBECA MONTEIRO', '', '', NULL, NULL, NULL, '', 'LINARMN2@ALTLOOKCOM', '', NULL, NULL, NULL, NULL, 2, 0, '1990-04-06'),
(29, '05813497412', 'BRUNA MESQUITA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1990-04-26'),
(30, '04325061444', 'POLIANA REIS BARBOSA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1981-12-03'),
(31, '', 'MARINA MESQUITA DE ARAUJO', '', '', NULL, NULL, NULL, '', 'MARINAMESQUITA@Gmail.com', '', NULL, NULL, NULL, NULL, 2, 0, '1990-04-08'),
(32, '80359566553', 'ALESSANDRA PALMA CAVALCANTE DE MELO', '', '', NULL, NULL, NULL, '', 'lekademelo@hotmail.com', '', NULL, NULL, NULL, NULL, 2, 0, '1982-12-02'),
(33, '', 'SUSANA MARLO FREIRE ARAUJO', '', '', NULL, NULL, NULL, '', 'susanamfa@gmail.com', '', NULL, NULL, NULL, NULL, 2, 0, '1992-05-27'),
(35, '', 'LUCIA MARIA VASCONCELOS CAVALCANTE', '', '', NULL, NULL, NULL, '', 'cvmlucia@hotmail.com', '', NULL, NULL, NULL, NULL, 2, 0, '1990-03-07'),
(36, '', 'ZENAIDE FREIRE DE CARVALHO', '', '', NULL, NULL, NULL, '', 'zenaidefreire@hotmail.com', '', NULL, NULL, NULL, NULL, 2, 0, '1990-01-18'),
(37, '08318181409', 'PAULA LUCENA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1990-02-22'),
(38, '', 'HELLIENAY FREIRE', '', '', NULL, NULL, NULL, '', 'elienaifreire@hotmail.com', '', NULL, NULL, NULL, NULL, 2, 0, '1975-09-05'),
(39, '', 'ELIZABETE DO NASCIMENTO(ALBA)', '', '', NULL, NULL, NULL, '', '', ',', NULL, NULL, NULL, NULL, 2, 0, NULL),
(40, '', 'MAELY ANDRADE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1994-09-24'),
(41, '', 'EUZA LINS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1990-05-09'),
(42, '', 'LUCIANA BOMPASTOR CARNEIRO CANTEIRIO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1950-08-25'),
(43, '', 'PEPITA DIAS', '', '', NULL, NULL, NULL, '', '', '39831415', NULL, NULL, NULL, NULL, 2, 0, NULL),
(44, '22662324520', 'SUELY VMONICA SENTOSE VALVERDE DIAS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1962-05-25'),
(45, '01215867409', 'DORGYVANIA IESY DE OLIVEIRA BARROS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1982-09-01'),
(46, '22392653453', 'BERNADETE GUEDES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1956-09-10'),
(47, '07583245451', 'KILVA COELHO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1991-09-09'),
(48, '', 'LUIZA BOMPASTOR', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(49, '', 'MONICA TOMAS PEDROSA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(50, '', 'RUBIANE CLECIA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(51, '39308570578', 'SUZETE BOFIM', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1999-03-30'),
(52, '', 'MARIJARA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(53, '', 'MARIANA SOLTO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1989-05-06'),
(54, '04814563418', 'LARISSA LAVOR', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1987-06-14'),
(55, '81098677587', 'PRISCILA BRISAK', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(56, '86716069453', 'ANA CELIS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(57, '', 'MARILDA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(58, '', 'RIZETE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(59, '03957439400', 'ROBERTA CRISTINA PESSOA', '', '', NULL, NULL, NULL, '', '', '8799684505', NULL, NULL, NULL, NULL, 2, 0, '1980-10-26'),
(60, '', 'ISABELE FILHA DE CELIS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(61, '', 'CELIA MARIA SOARES DRUBE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1961-08-25'),
(62, '', 'VITORIA VALVERDE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(63, '', 'MONICA MARIA DA SILVA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1990-07-19'),
(64, '101 396 784 41', 'CAROLINE MACEDO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(65, '03418756433', 'RIZELMA CARVALHO FREIRE', '', '', NULL, NULL, NULL, '', '', '988183954', NULL, NULL, NULL, NULL, 2, 0, '1974-03-05'),
(67, '19533705434', 'ANTONIA MARIA CAVALCANTE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1959-11-08'),
(68, '', 'EVA FERREIRA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1984-12-23'),
(69, '04183628903', 'DANIELA MEDEIROS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1984-01-18'),
(70, '08748289450', 'DANIELA DE ARAUJO SOUZA PARISIO', '', '', NULL, NULL, NULL, '', 'danielaparisio@hotmail.com', '38615194', NULL, NULL, NULL, NULL, 2, 0, '1993-07-05'),
(71, '320596923491', 'TEREZA ALENCAR', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1962-10-29'),
(72, '', 'BETINHA FARIAS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(73, '', 'NARA RAMOS CAVALCANTE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(74, '84408880400', 'ALESSANDRA FALCAO REIS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1972-12-25'),
(75, '03618002408', 'JOSENILVA SANTOS EVANGELISTA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1980-04-19'),
(76, '27114449534', 'GILMA DA SILVA OLIVEIRA ELPIDIO', '', '', NULL, NULL, NULL, '', '', '8738627440', NULL, NULL, NULL, NULL, 2, 0, '1963-04-20'),
(77, '07436519441', 'MANUELA ALBUQUERQUE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1985-01-31'),
(78, '97778052334', 'MANOELA SOARES DO N ROCHA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1999-09-29'),
(79, '98520083404', 'HELLANE MARINHO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(80, '06657551479', 'ANYELLE S DA S CAVALCANTE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1987-10-13'),
(81, '', 'ISABELA FLORES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(83, '03604329485', 'ANA CARLA RODRIGUES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1979-07-07'),
(84, '', 'VANIA CORREIA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(85, '00035044411', 'DIANARA ALBUQUERQUE LEITE CAVALCANTE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1999-07-25'),
(86, '943 902 314 72', 'ANA MARIA MACEDO DE POCILHO', '', '', NULL, NULL, NULL, '', '', '8788352646', NULL, NULL, NULL, NULL, 2, 0, NULL),
(87, '54831148415', 'GENILDE VASCONCELOS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(88, '', 'ROSANGELA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(89, '08273673413', 'SARA CAMPOS DE NONATO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1989-09-14'),
(90, '', 'VILDETE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(91, '035 793 304 46', 'ANDREZA LIMA  FARIAS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(92, '', 'FATIMA FARIAS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(93, '00791518507', 'THAIS BOMPASTOR', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1979-08-07'),
(94, '340 171 604 25', 'MARLUCE FERNANDES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(95, '', 'BRUNA TORRES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(96, '', 'CAROL LUCENA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(97, '', 'VERUSCA RIBEIRO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(98, '', 'LUCINHA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(99, '10584728875', 'BEATRIZ PARANHOS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1964-03-09'),
(100, '', 'ESTELA MARCIA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(101, '', 'BRUNA BRISAK', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(102, '06658987467', 'VANESSA TAYANE DE O FREIRE', '', '', NULL, NULL, NULL, '', '', '8788263644', NULL, NULL, NULL, NULL, 2, 0, '1986-11-08'),
(103, '', 'LUANA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(104, '', 'VINICIUS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(105, '', 'PAULA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(106, '019 725 495 02', 'MARIA CAROLINE GOMES VASCONCELOS', '', '', NULL, NULL, NULL, '', '', '7488042176', NULL, NULL, NULL, NULL, 2, 0, NULL),
(107, '', 'MARINEVES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(108, '', 'ZILDETE APARECIDA DE FARIA LIMA', '', '', NULL, NULL, NULL, '', '', '8788294039', NULL, NULL, NULL, NULL, 2, 0, '1971-09-28'),
(109, '', 'ISLAYNE SANTOS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1999-07-15'),
(110, '', 'VIVIAN', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(111, '', 'IANARA IRMA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(112, '', 'PAULA BOSIO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(113, '02491937441', 'NEIDE NUNES CAIXA', '', '', NULL, NULL, NULL, '', '', '87886', NULL, NULL, NULL, NULL, 2, 0, '1979-12-09'),
(114, '00240717570', 'ANDREIA MONTEIRO(BLOSSOM)', '', '', NULL, NULL, NULL, '', '', '8788385099', NULL, NULL, NULL, NULL, 2, 0, NULL),
(115, '00355969530', 'ADRIANA ARAUJO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1983-04-17'),
(116, '808 428 605 68', 'LILIAN KOSHIYAMA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1990-07-16'),
(117, '02145786406', 'ADRIANA MENESES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1976-09-14'),
(118, '11092938796', 'MARCELA FOLY', '', '', NULL, NULL, NULL, '', '', '8788055594', NULL, NULL, NULL, NULL, 2, 0, '1985-09-20'),
(119, '91765641420', 'CARMEM MIRANDA', '', '', NULL, NULL, NULL, '', '', '8799730802', NULL, NULL, NULL, NULL, 2, 0, '1969-10-21'),
(120, '07398072422', 'ALANA GUERRA BASTOS FREIRE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1989-06-03'),
(121, '47566396404', 'GAL MACEDO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1966-08-11'),
(122, '', 'MERLISE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(123, '', 'GABRIELA COIMBRA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(124, '', 'DINANE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(125, '', 'REBECA IRMA DE ROBERTA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(126, '', 'MARIA CLARA sechi', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(127, '', 'ISABELA LOIRA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(128, '', 'ERIK', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(129, '', 'ELISIANE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(130, '', 'JOSELITA RAMOS BRANDAO LIMA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1956-05-26'),
(131, '', 'DONATILA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(132, '01413385559', 'JAMILLE FREIRE SILVA DE ALMEIDA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1984-02-11'),
(133, '', 'IANA PADILHA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(134, '', 'CECILIA COSENTINA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(135, '', 'IONE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(136, '', 'LIVIA BRINGEL', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1990-01-09'),
(137, '249 089 754 34', 'MARIA SOLANGE BEZERRA', '', '', NULL, NULL, NULL, '', '', '8788477721', NULL, NULL, NULL, NULL, 2, 0, NULL),
(138, '', 'MARINA NOGUEIRA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(139, '49182170525', 'SUELY WALDMANN', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1968-04-20'),
(140, '', 'IVANEIDE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(141, '00742464458', 'UBIRANALVA BRAGA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1979-12-21'),
(142, '', 'BEATRIZ FILHA DE CLAUDIA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(143, '', 'TAMARA MENEZES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1991-03-02'),
(144, '05927316425', 'HELGA LOUISE SANTOS BRAGARD', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1990-11-07'),
(145, '', 'NARA PATRIOTA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(146, '', 'GABY', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(147, '20969600453', 'VALERIA DE ALBUQUERQUE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(148, '', 'ROSELIA FILHA DE VALERIA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(149, '', 'ANA PAULA MONTEIRO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(150, '', 'FERNADA LARA DE CARVALHO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1971-12-06'),
(151, '', 'MARIA DO SOCORRO CAUBE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(152, '59894938434', 'NADJA FERNANDES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1968-02-26'),
(153, '01135720185', 'NADIA SERRA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1983-12-03'),
(154, '07395017471', 'FERNANDA VALVERDE BRANDÇO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(155, '09336626450', 'LUMA HANNAH SILZA SANTOS ', '', '', NULL, NULL, NULL, '', '', '87', NULL, NULL, NULL, NULL, 2, 0, '1991-04-25'),
(156, '', 'IRIS SILVA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(157, '71117121453', 'SILVANA LUCENA', '', '', NULL, NULL, NULL, '', '', '8796401955', NULL, NULL, NULL, NULL, 2, 0, '1970-04-07'),
(158, '', 'GRACA ELMA VASCONCELOS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(159, '', 'ADRIANA TORQUATO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(160, '42768780472', 'ROSEMARY REIS MONTEIRO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(161, '07455017456', 'JULIANY LIRA', '', '', NULL, NULL, NULL, '', '', '8798000660', NULL, NULL, NULL, NULL, 2, 0, '1989-08-28'),
(162, '', 'MARIA LUIZA REIS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(163, '', 'MARIANA REIS VALGUEIRO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(164, '04759787496', 'JULIANA NOGUEIRA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(165, '', 'SILVIA BARROS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(166, '', 'LARA RODRIGUES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(167, '', 'LUIZA TORRES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1996-06-24'),
(168, '', 'LUIZA SANTO SE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(169, '33250750591', 'CECILIA VALVERDE MELO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1963-12-20'),
(170, '', 'VANESSA DURANDO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(171, '', 'VALQUIRIA BRITO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(172, '', 'ANDRESSA DE CARVALHO MANISOBA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1996-03-01'),
(173, '00801656486', 'SOCORRO DELMONDES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1982-12-28'),
(174, '', 'LORENA VILELA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(175, '', 'SANDRA ITAU', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(176, '', 'MARIA CECILIA FALCAO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(177, '05553399475', 'ANDIRA BRASILEIRO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1986-01-07'),
(178, '05251367465', 'CLARA VASCONCELOS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1983-12-29'),
(179, '', 'CLARA   (CECILIA)', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(180, '', 'AURISTELA / RAQUEL', '', '', NULL, NULL, NULL, '', '', '879 9128 7202', NULL, NULL, NULL, NULL, 2, 0, NULL),
(181, '61954209568', 'CLEIDE HORA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1971-04-28'),
(182, '', 'LANIELA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(183, '', 'MARIANA MEL', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(184, '07450319438', 'ANNE CAROLINE MARTINS TAVARES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1987-06-23'),
(185, '07208409455', 'FLAVIA TEXEIRAS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1988-09-06'),
(186, '', 'JESSICA GIODANO PARANHOS', '', '', NULL, NULL, NULL, '', '', '87988789645', NULL, NULL, NULL, NULL, 2, 0, '1991-10-16'),
(187, '', 'NEILZA SILVA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(188, '', 'ROBERTA ODILON', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(189, '', 'ISADORA CAVALCANTE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1993-08-17'),
(190, '', 'MAYARA GOMES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(191, '', 'MAYNA BISPO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(192, '', 'LAZARA GURRA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(193, '665 264 18449', 'ANGELA MARIA CASTRO SILVA', '', '', NULL, NULL, NULL, '', '', '87883', NULL, NULL, NULL, NULL, 2, 0, NULL),
(194, '06931336436', 'TIALA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1986-08-27'),
(195, '04777437493', 'CAROLINA MELO BRITO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1986-09-13'),
(196, '', 'ELINEIDE MARIA DOS SANTOS MIGUEL', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(197, '40974461857', 'NAIARA SETOGUCHI', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1992-06-04'),
(198, '', 'CLICIA MELO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(199, '02677688506', 'DENNIZE ROCHA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(200, '', 'VANESSA RIBEIRO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(201, '21789082846', 'MARIA VANUZIA BARBOSA', '', '', NULL, NULL, NULL, '', '', '8781108080', NULL, NULL, NULL, NULL, 2, 0, NULL),
(202, '', 'MICHELE MARQUES', '', '', NULL, NULL, NULL, '', '', '8799061051', NULL, NULL, NULL, NULL, 2, 0, '1985-04-15'),
(203, '34176292415', 'JUSSARIA AZEVEDO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1960-11-18'),
(204, '', 'IVANIA VILELA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(205, '667 542 405 04', 'ELANE ANDRADE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(206, '', 'RENATA ARAUJO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(207, '487 659 963 72', 'FABRICIA MENDES(TOK FINALY)', '', '', NULL, NULL, NULL, '', '', '8738617679', NULL, NULL, NULL, NULL, 2, 0, NULL),
(208, '22663223468', 'ELIANE PARISIO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1959-06-18'),
(209, '', 'ARIDIANE FERRAZ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1989-02-17'),
(210, '90042492572', 'LEIDE NASCIMENTO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1999-05-24'),
(211, '017 925 145 73', 'RENATA MOREIRA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(212, '', 'AUDICLEIA R DE M TORRES(AFRANIO)', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1999-05-18'),
(213, '', 'LOURA IRMA DE IVANIA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(214, '', 'SONIA RODRIGUES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(215, '', 'PATRICIA DUARTE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(216, '', 'LUZINEIDE VASCONCELOS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(217, '', 'LUCELIA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(218, '698 688 085 53', 'RITA DE CASIA BARBOSA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(219, '', 'VICTORIA ANDRESSA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(220, '08813429479', 'MARINA DE SOLZA COELHO CORREIA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(221, '56321630', 'GRAZIELA VASCONCELOS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1981-11-05'),
(222, '', 'EDNETE SANTANA DE ARAUJO', '', '', NULL, NULL, NULL, '', '', '8738615623', NULL, NULL, NULL, NULL, 2, 0, NULL),
(223, '', 'TULIO ARAUJO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(224, '74795317453', 'MARCIA VANUSIA VIEIRA', '', '', NULL, NULL, NULL, '', '', '7488167370', NULL, NULL, NULL, NULL, 2, 0, '1999-03-01'),
(225, '', 'SUYLLA GOMES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(226, '', 'MARLEIDE SANTOS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(227, '43402348500', 'VANIA ALVES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(228, '', 'JAIRO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(229, '', 'FABIANA SILVA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1990-12-10'),
(230, '00772824428', 'CAMILLA DAYANNA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1987-09-15'),
(231, '060 736 404 12', 'AMANDA SANTOS ARAUJO', '', '', NULL, NULL, NULL, '', '', '8788014006', NULL, NULL, NULL, NULL, 2, 0, '1986-06-11'),
(232, '07816061447', 'BRUNA LARISSA GOMES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1992-07-14'),
(233, '', 'LAURA SA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1992-05-24'),
(234, '038 753 584 57', 'DEBORA SOARES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(235, '02499950420', 'CLEIA RODRIGUES MAGALHAES TERTO(COMPOSE)', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1974-04-09'),
(236, '', 'MARIA ZITA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(237, '038 641 734 26', 'KLEBIA DE SOLZA MURICY PEIXINHO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(238, '01231252359', 'IONARA GUERRA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1985-08-24'),
(239, '', 'OZANILDA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1990-09-07'),
(240, '', 'MARCIA PATRICIA ALVES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(241, '', 'MARLINDA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(242, '', 'ROSIANA ALCANTARA', '', '', NULL, NULL, NULL, '', '', '74988172781', NULL, NULL, NULL, NULL, 2, 0, '1990-07-22'),
(243, '', 'ANA LUCIA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(244, '', 'SORAIA ALVES LINS', '', '', NULL, NULL, NULL, '', '', '8738615193', NULL, NULL, NULL, NULL, 2, 0, '1968-12-25'),
(245, '20354410563', 'NEIDE PEREIRA SANTOS MACEDO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(246, '', 'NEUMA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(247, '04279290407', 'ANA LUZIA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1983-12-13'),
(248, '171 469 093 87', 'ALBERTINA RODRIGUES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(249, '', 'ROBSON', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(250, '70509839415', 'JUCELMA RAMOS BATISTA RIBEIRO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1970-08-21'),
(251, '', 'MARIANA DE OLIVEIRA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1985-12-11'),
(252, '', 'JULIANA TORRES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(253, '01042120471', 'KESIA BRAGA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1980-12-29'),
(254, '', 'ANA CLAUDIA PERCIO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(255, '03273639407', 'TAIS FARIAS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1980-11-30'),
(256, '', 'NAYRE GERRA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1973-11-10'),
(257, '', 'TACIANA TORRES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1971-05-04'),
(258, '', 'POLIANA GURGEL', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1973-07-26'),
(259, '', 'JESSICA LAURINDA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1996-06-28'),
(260, '0756101409', 'NEILA ABRANTES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1975-11-09'),
(261, '00035984465', 'KARINE ALCANTARA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1978-05-22'),
(262, '038 405 824 82', 'ANNEAUREA BARBOSA BRITO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1982-03-16'),
(263, '03514491593', 'CARLA LEAL', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1998-01-31'),
(264, '041 350 514 64', 'ADRIANA PAIXAO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '87988091376', NULL, NULL, NULL, '2017-10-21 06:22:10', 2, 0, '1970-01-01'),
(265, '', 'ROSE CASTRO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1985-08-30'),
(266, '', 'CICERA ALVES VIEIRA CAVALCANTE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(267, '', 'ANA PAULA PEREIRA SANTOS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1991-04-08'),
(268, '', 'ISABELA PEIXOTO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '2000-05-03'),
(269, '', 'LUANA ALBURQUEQUE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(270, '', 'VALDANIA GOMES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(271, '', 'KELLY ANNE COSTA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1995-06-06'),
(272, '', 'LARA BRITO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1990-12-22'),
(273, '', 'MONA MIRELA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(274, '', 'FERNANDA LIMA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(275, '', 'PAULINHA MONTEIRO (FILHA ROSIMERE)', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1998-05-30'),
(276, '', 'DORA Sµ (TIA ANDIRA)', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1556-05-29'),
(277, '', 'SUZENIR LIMA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1979-05-24'),
(278, '58042202415', 'LUZINEIDE SOARES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1967-10-29'),
(279, '', 'MARCELA FARIA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(280, '04971815686', 'LUCIANA ANDRADE MENDO€A(UNIMED)', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(281, '04939252442', 'PRISCILA LUCENO MOURA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1987-12-26'),
(282, '', 'CRISTIANE REIS (JUAZEIRO)', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1975-12-05'),
(283, '913 790 405 15', 'IANDRA CARLA LIMBORIO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1978-02-06'),
(284, '', 'FABIANE DANTAS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(285, '02180647484', 'KAREN COSTA GOMES TORRES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1975-05-30'),
(286, '', 'CESAR TORRES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1999-03-19'),
(287, '', 'MICHELE BORGES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(288, '20342837400', 'CARLA SANTOS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(289, '', 'RAFAELA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(290, '093 483 445 87', 'NORMA M TAVARES VALVERDE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(291, '', 'NILSOM RODRIGUES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(292, '02616365456', 'CLEIDE VASCONCELOS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1972-01-12'),
(293, '93593252520', 'VALERIA PATRICIA FREITAS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1990-11-22'),
(294, '', 'JESSICA CARVALHO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(295, '06931336436', 'THYALLA DE SOLZA GONSALVES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1986-08-27'),
(296, '', 'NAIRA NAISA DE CARVALHO', '', '', NULL, NULL, NULL, '', 'NAIRA_NAISA@hotmail.com', '8781446930', NULL, NULL, NULL, NULL, 2, 0, '1991-01-26'),
(298, '03577102314', 'RAFAELY AMARO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1988-10-12'),
(299, '770 384 874 34', 'NARA FERNANDES CRUZ NOGUEIRA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(300, '', 'CRISTINA MESQUITA', '', '', NULL, NULL, NULL, '', '', '87', NULL, NULL, NULL, NULL, 2, 0, NULL),
(301, '', 'ALINE CARVALHO ANGELIM LUZ', '', '', NULL, NULL, NULL, '', '', '8788888888', NULL, NULL, NULL, NULL, 2, 0, NULL),
(302, '', 'MARIA DE LARA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1995-09-13'),
(303, '', 'MARIA DA GLORIA SILVA ELPIDIO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1955-09-12'),
(304, '', 'ESTENIA CAVALCANTE NOVAES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1972-02-18'),
(305, '02682778429', 'VALDERLUCIA RODRIGUES DOS SANTOS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1990-02-13'),
(306, '', 'GLORIA FLORES', '', '', NULL, NULL, NULL, '', '', '8', NULL, NULL, NULL, NULL, 2, 0, NULL),
(307, '', 'REBECA RODRIGUES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1990-08-07'),
(308, '47812460100', 'MARCIA MINODA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1969-05-11'),
(309, '', 'MARIA FILHA DE PATRICIA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(310, '11035526433', 'ANA LUIZA FERREIRA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1998-09-22'),
(311, '', 'LUCIA MARIANO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(312, '', 'DAILANE FERNANDES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(313, '93350767672', 'DEBORA COSTA BASTOS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(314, '', 'CATARINA LUCENA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(315, '', 'CAMILA LUCENA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(316, '09866828450', 'JOSENILDA DOS SANTOS (Jâ)', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1989-02-14'),
(317, '01554796520', 'CAMILA BORBOSA DEOLINO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1996-05-22'),
(318, '', 'MARIA BARBOSA DEOLINO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1994-08-24'),
(319, '09005822430', 'SULLY DAIANE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1991-03-14'),
(320, '01554800579', 'mariana barbosa deolino', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1994-08-24'),
(321, '', 'ISABELA CAMPOS (LUCIMARA)', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '2002-10-22'),
(322, '10087453401', 'JESSICA AZEVEDO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1992-06-03'),
(323, '', 'LETICIA VILELA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(324, '', 'JULIANE MINODA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(325, '036 054 134 80', 'ELIANE AGUIAR', '', '', NULL, NULL, NULL, '', '', '8788140514', NULL, NULL, NULL, NULL, 2, 0, '1981-02-09'),
(326, '13710702453', 'MARINEVES MORAIS', '', '', NULL, NULL, NULL, '', '', '8187433686', NULL, NULL, NULL, NULL, 2, 0, '1957-04-11'),
(327, '', 'MEIRILENE MAGALHAES ( MEIRE)', '', '', NULL, NULL, NULL, '', '', '87988035132', NULL, NULL, NULL, NULL, 2, 0, '1970-04-11'),
(328, '', 'LEDA TERTO', '', '', NULL, NULL, NULL, '', '', '81996573650', NULL, NULL, NULL, NULL, 2, 0, '1973-06-19'),
(329, '', 'TICIANA DE SOUZA', '', '', NULL, NULL, NULL, '', '', '87988734362', NULL, NULL, NULL, NULL, 2, 0, '1976-03-02'),
(330, '06088993480', 'ALINE LAZZARI', '', '', NULL, NULL, NULL, '', '', '87981024440', NULL, NULL, NULL, NULL, 2, 0, '1986-08-11'),
(331, '25737988449', 'MONICA MARIA DA PAZ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(332, '06890537467', 'ANDRESSA CAVALCANTE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1988-03-23'),
(333, '82184895415', 'LUCIA METIE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1968-02-05'),
(334, '', 'LIVIA ARCOVERDE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(335, '', 'NAILENE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(336, '', 'MARIANA FILGUEIRA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(337, '55906036415', 'SARA MOREIRA CABRAL FARIAS', '', '', NULL, NULL, NULL, '', '', '38581157', NULL, NULL, NULL, NULL, 2, 0, NULL),
(338, '07399345465', 'CAMILA RODRIGUES CASAL TORCEDA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1988-10-15'),
(339, '07540336439', 'AMANDA THAISA DE OLIVEIRA CRUZ', '', '', NULL, NULL, NULL, '', '', '8798061620', NULL, NULL, NULL, NULL, 2, 0, '1990-01-30'),
(340, '', 'LAURA FILHA DE MARISTELA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(341, '', 'SILVANA ITAU', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(342, '', 'APARECIDA TORRES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(344, '06099434488', 'HELOISA PEREIRA MARQUES TORRES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1985-09-29'),
(345, '', 'MARIA ZILDA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(346, '', 'BEATRIZ ALENCAR', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(347, '00247042889', 'IARA FONTES', '', '', NULL, NULL, NULL, '', '', '8799993931', NULL, NULL, NULL, NULL, 2, 0, '1951-10-29'),
(348, '039 506 074 55', 'LILIANE SOLTO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(349, '', 'MARGARETE CASTRO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(350, '', 'EVANIE DA SILVA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1900-08-26'),
(351, '', 'THAIANA PEREIRA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(352, '066 476 984 50', 'TAMIRES CARVALHO  COELLHO', '', '', NULL, NULL, NULL, '', '', '71982874242', NULL, NULL, NULL, NULL, 2, 0, '1987-06-11'),
(353, '06096172490', 'ISABELA VALENCIA ', '', '', NULL, NULL, NULL, '', '', '8796497271', NULL, NULL, NULL, NULL, 2, 0, '1987-05-28'),
(354, '', 'LARISSA LEAL', '', '', NULL, NULL, NULL, '', '', '87988551998', NULL, NULL, NULL, NULL, 2, 0, '1998-08-25'),
(355, '', 'REBECA FLORES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(356, '', 'ENAILE RAFAELA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1999-01-17'),
(357, '08343510488', 'BRUNA  MOURA', '', '', NULL, NULL, NULL, '', '', '8799182069', NULL, NULL, NULL, NULL, 2, 0, '1996-05-24'),
(358, '', 'GEOVANA VILANOVA', '', '', NULL, NULL, NULL, '', '', '87988342538', NULL, NULL, NULL, NULL, 2, 0, '1997-02-16'),
(359, '', 'LAURA PERCIO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(360, '', 'ANDREA REIS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(361, '', 'AURENICE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(362, '45986274415', 'NARA DURANDO', '', '', NULL, NULL, NULL, '', '', '8788156461', NULL, NULL, NULL, NULL, 2, 0, '1999-07-14'),
(363, '49539060559', 'LUCIMAR COELHO MOURA RIBEIRO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1972-10-10'),
(364, '', 'THELVIA SECH', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(365, '00968649408', 'CARLA COELHO', '', '', NULL, NULL, NULL, '', '', '8199633779', NULL, NULL, NULL, NULL, 2, 0, '1999-05-21'),
(366, '', 'CINDIANE ALBERTI', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1989-05-08'),
(367, '41823710468', 'MARILEIDE DE SOLZA CORREIA', '', '', NULL, NULL, NULL, '', '', '8738615747', NULL, NULL, NULL, NULL, 2, 0, '1965-03-31'),
(368, '', 'MARIA LUIZA FLOR DASILVA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '2002-01-26'),
(369, '', 'MONALISA MEDRADO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1998-08-26'),
(370, '03468579535', 'GISLENE MENDES DA SILVA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1985-12-24'),
(371, '053 076 304 41', 'MARIANA ALVES ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(372, '045 033 464 31', 'TACIANA VELOSO', '', '', NULL, NULL, NULL, '', '', '9292055597', NULL, NULL, NULL, NULL, 2, 0, '1971-01-03'),
(373, '00790426439', 'PAULA MUNIZ  COELHO', '', '', NULL, NULL, NULL, '', '', '8799598080', NULL, NULL, NULL, NULL, 2, 0, '1980-02-19'),
(374, '278 749 234 72', 'MARIA HELENA MUNIZ COELHO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1999-12-17'),
(375, '', 'LUCIANA MENDON€A', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(376, '06759791452', 'GABRIELA RODRIGUES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1988-04-22'),
(377, '39585506300', 'VALDARIA GOMES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1972-04-05'),
(378, '017 014 345 71', 'ANA CAROLINA OLIVEIRA', '', '', NULL, NULL, NULL, '', '', '7488080809', NULL, NULL, NULL, NULL, 2, 0, '1979-10-31'),
(379, '03882838663', 'DANIELA FINOTTI  RESENDE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(380, '042 792 904 07', 'MARIA LUIZA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(381, '04288312906', 'JESSICA BRUINJE BIM DE CARVALHO', '', '', NULL, NULL, NULL, '', '', '8788233151', NULL, NULL, NULL, NULL, 2, 0, '1999-05-23'),
(382, '', 'GILDETE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(383, '65712544334', 'RAQUEL TAVARES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1985-11-13'),
(384, '', 'CARMINHA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(385, '', 'RAIA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(386, '40663698553', 'CLAUDIA SILVA DE SOUZA', '', '', NULL, NULL, NULL, '', 'colegio plenios 74 38623467', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(387, '08083335428', 'LETICIA SECCHI', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1992-10-30'),
(388, '', 'LAIS SECH', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(389, '07044684416', 'LARA SECCHI', '', '', NULL, NULL, NULL, '', '', '8791235749', NULL, NULL, NULL, NULL, 2, 0, '1988-11-08'),
(390, '', 'PERCIO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(391, '05354073430', 'CARLA FERNANDA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1985-07-25'),
(392, '74735942491', 'VALDELICE de brito santos ribeiro', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1966-03-13'),
(393, '', 'MAYANA BORGES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(394, '', 'LIVIA SOBRAN', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(395, '', 'MARLY LEAL', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(396, '88323773491', 'ILAIDE MARIA MOREIRA PEIXOTO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1972-08-16'),
(397, '', 'JULIANA ALEXANDRA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(398, '', 'SARA JACO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(399, '003 488 520 02', 'ALINE CAPELARIO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(400, '', 'CELMA FERREIRA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(401, '', 'SUZANA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(402, '', 'LETICIA AMIGA DEBORA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(403, '', 'SAMIA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(404, '', 'ROSEANE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(405, '', 'MAEVE MELO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(406, '', 'VICTORIA E MONICA SOB', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(407, '336 954 904 20', 'ARLENE ARAUJO LIMA ROCHA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(408, '', 'MARA TORRES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(409, '', 'ROMANA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(410, '', 'CAMILA AMIGA DE DEBORAH', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(411, '', 'RUMANA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(412, '08635231457', 'NATALIA ARAUJO L ROCHA COELHO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1990-10-14'),
(413, '88322556420', 'EDVANIA NOGUEIRA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1974-04-05'),
(414, '', 'MARISA COELHO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(415, '', 'ELLEN LIBORIO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1996-08-01'),
(416, '', 'KALINE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(417, '', 'SARA FILHA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(418, '33252238500', 'IRACEMA ALVES DE QUEIROZ', '', '', NULL, NULL, NULL, '', '', '7488119768', NULL, NULL, NULL, NULL, 2, 0, '1965-03-31'),
(419, '', 'MARILENSE MARQUE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1980-08-13'),
(420, '', 'SANDRA CAVALCANTE ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(421, '', 'NAIR', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1962-09-06'),
(422, '', 'PATRICIA AMORIN', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(423, '02751171460', 'SANCHA PETRONILA C R LISBOA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1976-08-19'),
(424, '', 'JUVENILHA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(425, '13666967434', 'SONIA BARBOSA GONCALVES VIEIRA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(426, '86701240259', 'DEVAC', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(427, '', 'VICTORIA FILHA MARINETE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(428, '', 'NEIDE GOMES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1966-06-17'),
(429, '77966430553', 'CLEIA CANUTO RIBEIRO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1977-12-05'),
(430, '09327873475', 'GABRIELA CARVALHO', '', '', NULL, NULL, NULL, '', '', '8788613222', NULL, NULL, NULL, NULL, 2, 0, '1990-05-19'),
(431, '08964465490', 'DAISEANE PINHEIRO DE AMORIN', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1990-09-03'),
(432, '51274922615', 'CLAUDIA SOARES PENA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL);
INSERT INTO `clientes` (`cli_id`, `cli_cpfcnpj`, `cli_nome`, `cli_endereco`, `cli_cep`, `cli_cidade`, `cli_estado`, `cli_bairro`, `cli_rg`, `cli_email`, `cli_telefone`, `cli_dt_add`, `deleted_at`, `created_at`, `updated_at`, `emp_id`, `usu_id`, `cli_nascimento`) VALUES
(433, '07278674409', 'PATRICIA KATEHERINE RAMOS VITAL', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1991-07-12'),
(434, '10087453401', 'ANA JESSICA MAMEDIO AZEVEDO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1992-06-03'),
(435, '', 'ANA AGRA BAWDEKUIN', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(436, '03960545444', 'FLAVIA ROCHA MOURA SILVA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(437, '07984183465', 'DEYLLYANE ALANE AGRA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1991-01-09'),
(438, '08140813478', 'MARIANE ALICE PEREIRA DOS SANTOS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1989-07-21'),
(439, '', 'NILZA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(440, '', 'JOANELI GUIMARAES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(441, '06466210548', 'DAYANNA NEVES GOMES DE SOUZA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1985-12-08'),
(442, '89504577415', 'SILVANEIDE VASCONCELOS BARROS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1973-11-25'),
(443, '90041224434', 'CIANA LOUREIRO', '', '', NULL, NULL, NULL, '', '', '8788212281', NULL, NULL, NULL, NULL, 2, 0, '1999-12-10'),
(444, '', 'CAROLINE ALENCAR', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1989-11-19'),
(445, '', 'EDUARDA CHAVES FILHA VALERIA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1997-12-29'),
(446, '02117437478', 'RENATA REZENDE', '', '', NULL, NULL, NULL, '', '', '8788042994', NULL, NULL, NULL, NULL, 2, 0, NULL),
(447, '37063138472', 'MARIA HELENA AMORIM', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1948-11-11'),
(448, '08986838400', 'SHAMARA CRYSTYNNA CARDOSO SANTOS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1990-11-10'),
(449, '07379959476', 'LARISSA FEREIRRA BRINGEL', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1993-05-18'),
(450, '32357532300', 'VERONICA CASTRO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1966-10-23'),
(451, '14112935846', 'SONIA MARINO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1970-11-22'),
(452, '', 'TEREZA (GABI)', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(453, '03806857440', 'GEIZA RODRIGUES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1980-10-02'),
(454, '06673456477', 'LUCIANA CAVALCANTI', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1988-03-02'),
(455, '010 761 265 88', 'LUANA MOTA', '', '', NULL, NULL, NULL, '', '', '7488044930', NULL, NULL, NULL, NULL, 2, 0, NULL),
(456, '97772240520', 'RITA DE CASSIAS MATOS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1980-01-26'),
(457, '01048977757', 'ALESSANDRA GOMES MARQUES PACHECO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1971-04-08'),
(458, '', 'LUCIA REZENDE', '', '', NULL, NULL, NULL, '', '', '1196188444', NULL, NULL, NULL, NULL, 2, 0, NULL),
(459, '24917559049', 'NELSA PRETO CAPPELLARO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1956-05-14'),
(460, '10739096460', 'MARIA VICKTORIA DE SOUZA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1998-05-18'),
(461, '22656286468', 'EDNALVA ALEXANDRE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1955-10-07'),
(462, '03176224478', 'PAULA ARAUJO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1977-01-22'),
(463, '', 'GEANE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(464, '17022074082', 'MARIA LAURA FALCÇO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(465, '08874274483', 'GABRIELA SOARES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1990-07-12'),
(466, '11063423553', 'MARIA RIBEIRO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(467, '900 679 834 72', 'CLECIANE NUNES', '', '', NULL, NULL, NULL, '', '', '8197852286', NULL, NULL, NULL, NULL, 2, 0, NULL),
(468, '', 'LORENA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(469, '', 'RANA FORMANDA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(470, '', 'ERICA FERNANDA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '2017-11-16'),
(471, '', 'SOLANGE ASSUNSAO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(472, '01435652477', 'NATALIA GOMES CAVALCANTE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1988-07-17'),
(473, '00353318523', 'VALDENICE FELIX', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1982-01-30'),
(474, '91652359320', 'GISELI  ANGELA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1982-02-28'),
(475, '02443271475', 'AURICELIA RAMOS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1977-01-24'),
(476, '', 'ROBERTA RODRIGUES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(477, '07349744420', 'DANYELLE MARIA CARVALHO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1990-05-13'),
(478, '', 'MARCIA PONTES DA SILVA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1980-10-01'),
(479, '', 'DEUZIENA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(480, '', 'SOCORRO ARGELI', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(481, '', 'ROBERTA VALVERDE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(482, '054 926 204 01', 'LIVIA VASCONCELOS ALENCAR', '', '', NULL, NULL, NULL, '', '', '8799510362', NULL, NULL, NULL, NULL, 2, 0, NULL),
(483, '47190418415', 'AUREA MARIA RAMOS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1965-04-23'),
(484, '31619459809', 'FRANCYS TORRES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1986-04-02'),
(485, '27206781802', 'CAROLINA VIANNA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1978-04-18'),
(486, '', 'ANA ANGELICA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1987-06-01'),
(487, '', 'IANE RODRIGUES BASTOS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1977-01-31'),
(488, '', 'JACKELINE SILVA JUAZEIRO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(489, '05184976400', 'MAYARA EUGENIA DA SILVA SOUZA( FILHA JOSELITA)', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1955-03-30'),
(490, '', 'ROZINEIDE COELHO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(491, '06256602420', 'ANA LUIZA CAMAL', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1990-04-02'),
(492, '', 'ROBERTA ALENCAR', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1987-07-29'),
(493, '', 'JANDA IARA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(494, '', 'LUZINEIDE CAIXA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(495, '051 849 774 73', 'MARA CRISTINA SILVA SOUZA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1985-10-15'),
(496, '', 'AMANDA ISABELA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(497, '01328711650', 'MARCELA VIANA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1981-02-21'),
(498, '', 'FABIANA SANTOS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(499, '', 'EDIEL', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(500, '59897554491', 'FRANCISCA VALQUIRIA NUNES DA SILVA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1964-10-16'),
(501, '', 'RAISSA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(502, '07277878450', 'PATRICIA APARECIDA NUNES TORRES', '', '', NULL, NULL, NULL, '', '', '8994181278', NULL, NULL, NULL, NULL, 2, 0, '1988-10-12'),
(503, '292 689 278 02', 'TELMA RODRIGUES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(504, '15246353400', 'MARIA HELENA RAMOS VIANA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1957-08-22'),
(505, '56149530434', 'HELENA FRAN€A MONTEIRO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1946-02-09'),
(506, '94164452587', 'ALEXSSANDRA ROSENDO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1976-07-18'),
(507, '09441174572', 'ELIZA MARIA CARREIRA CARDOSO', '', '', NULL, NULL, NULL, '', '', '8788035999', NULL, NULL, NULL, NULL, 2, 0, '1941-01-20'),
(508, '', 'MARIAZINHA RODRIGUES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(509, '', 'DAMA(02)', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(510, '33699720420', 'LEDA VIRGINIA CAVALCANTE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1964-01-15'),
(511, '11618247409', 'ANA CATARINA FIGUEIREDO CORNELIO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1997-08-26'),
(512, '', 'THAYANE RAPHAELA DE ALMEIDA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1995-11-23'),
(513, '', 'ALESANDRA ROSENDO', '', '', NULL, NULL, NULL, '', '', '8', NULL, NULL, NULL, NULL, 2, 0, '1976-07-18'),
(514, '', 'KELIANE COSTA', '', '', NULL, NULL, NULL, '', '', '8796138541', NULL, NULL, NULL, NULL, 2, 0, '1995-06-06'),
(515, '05756607404', 'KARLA DANIELLE OLIVEIRA CRUZ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(516, '', 'MELISSA WALDMANN', '', '', NULL, NULL, NULL, '', '', '8788645665', NULL, NULL, NULL, NULL, 2, 0, '1997-12-04'),
(517, '', 'MARIA CLARA WALDMANN', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(518, '2538000572', 'OLGA GOMES CARVALHO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1961-11-05'),
(519, '05756607404', 'KARLA DANIELLE OLIVEIRA CRUZ', '', '', NULL, NULL, NULL, '', '', '8788732068', NULL, NULL, NULL, NULL, 2, 0, '1986-12-18'),
(520, '', 'POLIANA GONSALVES', '', '', NULL, NULL, NULL, '', '', '8788011290', NULL, NULL, NULL, NULL, 2, 0, '1979-05-07'),
(521, '', 'THAIANA LOPES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(522, '68181434404', 'MARIA DO SOCORRO RODRIGUES RIBEIRO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(523, '01148076476', 'MARIANA SANTANA MACEDO', '', '', NULL, NULL, NULL, '', '', '8796753083', NULL, NULL, NULL, NULL, 2, 0, '1985-06-20'),
(524, '04177418471', 'JULIANA CAMPOS GUEDES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1982-03-12'),
(525, '03753918431', 'FABIANA ECI GOMOES', '', '', NULL, NULL, NULL, '', '', '8788143492', NULL, NULL, NULL, NULL, 2, 0, '1980-11-13'),
(526, '03742159364', 'ALANA CECILIA BATISTA LIMA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1989-04-26'),
(527, '56287720549', 'MARGARETE SANTO SE CORDEIRO E OLIVEIRA', '', '', NULL, NULL, NULL, '', '', '7488039091', NULL, NULL, NULL, NULL, 2, 0, '1973-09-20'),
(528, '00449030503', 'DORY SECHI', '', '', NULL, NULL, NULL, '', '', '8788133191', NULL, NULL, NULL, NULL, 2, 0, '1999-10-30'),
(529, '', 'GABRIELA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(530, '68181736400', 'ALBANI RODRIGUES DA CRUZ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '87988555755', NULL, NULL, NULL, '2017-10-21 06:22:34', 2, 0, '1970-01-01'),
(531, '', 'VERONICA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(532, '00281855528', 'EVA DIAS DE LIMA', '', '', NULL, NULL, NULL, '', '', '8788513511', NULL, NULL, NULL, NULL, 2, 0, '0199-03-24'),
(533, '08140813478', 'ALICE NARYANE', '', '', NULL, NULL, NULL, '', '', '8788193772', NULL, NULL, NULL, NULL, 2, 0, '1989-07-21'),
(534, '03956183479', 'MARCIA DANTAS DA SILVA', '', '', NULL, NULL, NULL, '', '', '8788683347', NULL, NULL, NULL, NULL, 2, 0, '1980-10-01'),
(535, '', 'JULIA BRISACK', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(536, '04986967401', 'ANA CATARINA SIMOES', '', '', NULL, NULL, NULL, '', '', '8199225316', NULL, NULL, NULL, NULL, 2, 0, '1983-11-08'),
(537, '79991360425', 'MARLY MARIA DE SOLZA', '', '', NULL, NULL, NULL, '', '', '8788156339', NULL, NULL, NULL, NULL, 2, 0, '1999-05-08'),
(538, '48434671468', 'MARIA DO CARMO RODRIGUES', '', '', NULL, NULL, NULL, '', '', '8788392217', NULL, NULL, NULL, NULL, 2, 0, NULL),
(539, '98411721515', 'ALEXSOM CLEITOM DA PAIXAO', '', '', NULL, NULL, NULL, '', '', '7488010864', NULL, NULL, NULL, NULL, 2, 0, '1998-03-12'),
(540, '06940964401', 'ELIANE GOES DE SIQUEIRA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1995-01-02'),
(541, '92253547468', 'YANNA MARIA PADILHA', '', '', NULL, NULL, NULL, '', '', '8738663500', NULL, NULL, NULL, NULL, 2, 0, '1999-03-11'),
(542, '892 763 023 87', 'SUENIA FRAN€A ', '', '', NULL, NULL, NULL, '', '', '8796232681', NULL, NULL, NULL, NULL, 2, 0, '1999-09-15'),
(543, '43044751500', 'RENATA VALDERDE', '', '', NULL, NULL, NULL, '', '', '7488076121', NULL, NULL, NULL, NULL, 2, 0, NULL),
(544, '', 'TATIANE PAPEL PAREDE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(545, '07401712407', 'DAIANA LIMA DE SOUZA', '', '', NULL, NULL, NULL, '', '', '8788213846', NULL, NULL, NULL, NULL, 2, 0, '1988-10-02'),
(546, '812 342 445 00', 'MAYANA CASTYELO BRANCO', '', '', NULL, NULL, NULL, '', '', '7499210001', NULL, NULL, NULL, NULL, 2, 0, NULL),
(548, '02803417413', 'TARCIANA FONSECA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(549, '', 'GRAZIELA KATIUSCIA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(550, '', 'GLEICIANE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(551, '43399835515', 'CLAUDENIA DE SOUZA FUJISAWA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1967-07-15'),
(552, '', 'EUGENIA LINS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(553, '', 'LUIZA REIS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(554, '', 'EDIVANIA MOREIRA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(555, '', 'MODELOS PROPAGANDA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(556, '793 177 585 68', 'FERNANDA CRISTINA MEIRELES', '', '', NULL, NULL, NULL, '', '', '8788356635', NULL, NULL, NULL, NULL, 2, 0, NULL),
(557, '06091000500', 'JULIANA OLIVEIRA ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(558, '', 'MAYARA MEDRADO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(559, '26884569468', 'LUCIA MARIA DE ANDRADE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1960-06-17'),
(560, NULL, 'LUCIA BOTELHO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, ',', NULL, NULL, NULL, '2017-10-25 19:54:03', 2, 0, '1970-01-01'),
(561, '08964465490', 'DAISEANE PINHEIRO DE AMORIM', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1990-09-03'),
(562, '', 'SAMARA CAVALCANTE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(563, '', 'ISABELA FILHA DE CLECIA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(564, '', 'LUCIA BOTELHO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(565, '073 922 384 42', 'ANA CAROLINA PEREIRA DE LINS', '', '', NULL, NULL, NULL, '', '', '8582171775', NULL, NULL, NULL, NULL, 2, 0, NULL),
(566, '', 'IRMA DE ANY', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(567, '', 'EDNA MAE DO NOIVO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(568, '', 'EUGENIA CASAMENTO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(569, '', 'ANA CAROLINE REIS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(570, '11161977406', 'NATA LISBOA SILVA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(571, '068 529 594 06', 'GISELE MORAO', '', '', NULL, NULL, NULL, '', '', '8798112275', NULL, NULL, NULL, NULL, 2, 0, NULL),
(572, '01903955599', 'GABRIELLE SALDANHA KISTNER', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(573, '', 'ELIANE ALVES', '', '', NULL, NULL, NULL, '', '', ',', NULL, NULL, NULL, NULL, 2, 0, NULL),
(574, '044 105 074 71', 'INGRID GUIMARAES DE ALMEIDA ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(575, '562 522 775 68', 'SILVANA LEANDRO FREIRE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(576, '009 465 114 12', 'ANDREA MEDEROS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(577, '065 521 944 71', 'ANA PAULA MORAIS ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(578, '', 'VITORIA PINHEIRO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(579, '022 171 744 79', 'CRISTIANE MUNIZ VIANA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(580, '058 043 624 10', 'FABRICIA LOPES MARQUES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(581, '10020492456', 'RAIRES ROCHA PEREIRA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1992-08-19'),
(582, '047 399 955 22', 'JUSICLEIDE ALVES POSSIDANO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(583, '', 'KEREM SOLZA TORRES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(584, '052 454 994 01', 'IARA CIRILO GONSALVES SILVA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(585, '', 'SIMONE GADE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(586, '964 179 785 91', 'KEILA BASTOS DE  MARQUES LIMA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(587, '', 'JASI IRMA DE CLEIDE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(588, '219 354 164 72', 'OSANILDA FERREIRA DE LIMA COSTA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(589, '030 765 204 24', 'NAZARETH MACEDO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(590, '0827368432', 'indira sa', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1989-09-24'),
(591, '', 'NEIDE MACEDO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(592, '028 273 555 07', 'MAYARA SOLZA SILVA ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(593, '', 'AUDENORA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(594, '', 'SHEILA NOGUEIRA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(595, '051 795 665 01', 'HELIA MAIARA NERY AMORIM', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(596, '', 'APARECIDA LUCENA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(597, '', 'MARIA APERECIDA LUCENA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(598, '016 294 805 09', 'CRISTIANE RIBEIRO DA SILVA NUNES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(599, '', 'IRMA DA NOIVA/ MAE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(600, '008 435 884 03', 'EMANUELLA COIMBRA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(601, '092 406 404 87', 'MARIA VILANI DA COSTA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(602, '', 'GRAZIELA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(603, '026 871 974 89', 'KEILA MFERRAZ TORRES', '', '', NULL, NULL, NULL, '', '', ',', NULL, NULL, NULL, NULL, 2, 0, NULL),
(604, '', 'MAYLA SAMARA DOS SANTOS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '2002-09-07'),
(605, '013 267 324 05', 'ERICA SANTOS DA SILVA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1985-09-28'),
(606, '', 'JUNIOR', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(607, '338 797 024 20', 'SANGELA VIRGINIA MDA SCAVAL(BIBI)', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(608, '047 960 574 20', 'RAQUEL GONSALVES TORRES DE AMORIM', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1983-02-11'),
(609, '073 589 654 26', 'RAFAELA ARAUJO NUNES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(610, '036 222 765 92', 'ANA LUIZA ARANHA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(611, '', 'TANARA MENESES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(612, '', 'PATRICIO ESPOSO DE MARIANA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(613, '457 438 864 15', 'PAULA RODRIGUES MACEDO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(614, '845 519 844 34', 'MARIA AZENEIDE MIRANDA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(615, '', 'VANESSA KIKUTE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(616, '', 'ALEXANDRA GONSALEVES TORRES ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(617, '094 550 734 80', 'TARCILA GOMES DE TAMARIDO', '', '', NULL, NULL, NULL, '', '', '8799498897', NULL, NULL, NULL, NULL, 2, 0, NULL),
(618, '084 793 814 00', 'ISABELA COUTINHO ', '', '', NULL, NULL, NULL, '', '', '38611505', NULL, NULL, NULL, NULL, 2, 0, NULL),
(619, '', 'MARIA TAVARES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(620, '621 461 224 04', 'LUCIANA OLIVEIRA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(621, '', 'MAGNILDE MAE DE MANOELA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(622, '', 'ISMERIDA ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(623, '', 'LAURINDA ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(624, '', 'MARISTELA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(625, '039 917 164 95', 'JULIANA BASTOS ', '', '', NULL, NULL, NULL, '', '', '8788487399', NULL, NULL, NULL, NULL, 2, 0, NULL),
(626, '021 441 764 69', 'POLIANA ALEXANDRE BEZERRA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(627, '', 'JUJU', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(628, '', 'IRINETE PEREIRA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(629, '', 'JULIETA MARIA DE LIMA', '', '', NULL, NULL, NULL, '', '', '8788377007', NULL, NULL, NULL, NULL, 2, 0, NULL),
(630, '', 'MARIA JULIA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(631, '', 'JULIEN  IAMUT', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(632, '', 'MARIANA ELIZA ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(633, '', 'MAIANA ELIZA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(634, '', 'MARLA CARVALHO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(635, '', 'BENILDA RODRIGUES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(636, NULL, 'HELENA FERNADES', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-25 19:53:46', 2, 0, '1970-01-01'),
(637, '041 121 715 10', 'SHANA PINHEIRO LOBO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(638, '', 'SOGRA DE ANNE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(639, '050 949 145 61', 'DIANE MARIA ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(640, '07139068470', 'GENESIA COELHO ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1987-11-05'),
(641, '', 'TATIELE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(642, '', 'ALINE DIAS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(643, '073 799 404 57', 'MARIA IZABEL ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(644, '092 910 764 06', 'MONIQUE MORAIS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(645, '481 647 71449', 'MARIA NEVOLANDACARDOSO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(646, '', 'POLIANA BORGES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(647, '', 'ALINE REBECA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(648, '', 'ATONIA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(649, '', 'DIVA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(650, '', 'MARIA JOSE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(651, '055 983 054 88', 'HELEM RAMOS BRANDAO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(652, '', 'MONALIZA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(653, '', 'ROBERTA NOGUEIRA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(654, '', 'MARINEIDE SOGRA DE MARIANA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(655, '', 'CRISTIANE DO SORTEIO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(656, '', 'LAISE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(657, '', 'LARISSA DE SOLZA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(658, '', 'IANKA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(659, '080 096 464 07', 'FRANCI MARTINELE SANTOS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(660, '047 599 144 39', 'CAMILA MEDEIROS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(661, '', 'ALDENORA COSMA ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(662, '29615623334', 'MARIA DO SOCORRO nunes ferreira correia', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1965-08-04'),
(663, '', 'GEOVANA GOMES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(664, '', 'ISABELA TERTO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(665, '', 'AMANDA COELHO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(666, '690 026 144 20', 'LUSINEIDE CARMO ANDRADE DE LACERDA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1967-06-24'),
(667, '', 'MARIZETE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(668, '', 'EUKE CAVALCANTE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(669, '686 772 275 04', 'PETRICIA LUZ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(670, '271 248 658 75', 'MONICA CRISTINA ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(671, '', 'ISABEL', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(672, '902 358 865 72', 'CARINE LIMA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(673, '', 'INGRID 15 ANOS (MAE)', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(674, '', 'ALINE KELLY FERREIRA MATOS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(675, '', 'JAMILE LIMA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(676, '048 954 614 59', 'JESSICA MARQUES DEOS SANT6OSW', '', '', NULL, NULL, NULL, '', '', '87988645177', NULL, NULL, NULL, NULL, 2, 0, '1985-05-03'),
(677, '239 032 104 63', 'ANEZITE RAIMUNDA DE SOLZA ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(678, '931 940 134 98', 'PATRICIA GARCIA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(679, '', 'LUDMILA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(680, '', 'ROSA MAE DE AMANDA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(681, '812 342 445 00', 'MAYNA CASTELO BRANCO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(682, '10457379413', 'NAYARA LISBOA SILVA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1993-03-16'),
(683, '', 'LITA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(684, '', 'CASSIA RIBEIRO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(685, '075 759 568 59', 'JACIARA MELO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(686, '084 250 814 75', 'MARIANA LIMA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(687, '', 'MAYARA CASTRO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(688, '', 'GILMARA CARVALHO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(689, '003 711 995 81', 'NEIVA MAIA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(690, '', 'ADRIANA ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(691, '', 'FABIOLA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(692, '', 'LARISSA FILHA DECLEIA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(693, '', 'CRISTINA TRINDADE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(694, '', 'LEDA AMORIM', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(695, '', 'TETE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(696, '', 'CICERA 15 ANOS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(697, '089 868 384 90', 'SHAMARA CRYSTYNNA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(698, '', 'TALITA VASCONCELOS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(699, '402 623 164 04', 'MARIA ZILMAR SIQUEIRA SILVA ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(700, '024 863 744 42', 'JOSEMILDA MARIA SANTOS ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(701, '', 'ELHO MARIDO DE MONICA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(702, '', 'DANIEL FILHO DE NALVA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(703, '025 952 254 64', 'LUCIANA TENORIO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(704, '007 462 484 90', 'RITA ROCHA', '', '', NULL, NULL, NULL, '', '', '8781017071', NULL, NULL, NULL, NULL, 2, 0, NULL),
(705, '', 'FERNANDA REIS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(706, '', 'YANE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(707, '', 'MARILENE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(708, '291 631 614 00', 'GLEIDE MACEDO DE SOLZA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(709, '', 'CLIENTE CASA NOVA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(710, '', 'CAROL LIMA RODRIGUES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(711, '', 'MARCIA /JULIANE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(712, '030 051 174 48', 'ALINNE DURANDO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(713, '', 'RITA MARIA MONTEIRO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(714, '', 'MASSU MALHEIRO ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(715, '715 513 364 72', 'CLAUDENICE HELENA FRAN€A', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(716, '', 'EMANUELE FRANCA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(717, '', 'CLAUDENICE E EMANUELE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(718, '064 621 054 88', 'DAYANA GOMES DE SOLSA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(719, '', 'LIDIANE CARLA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(720, '029 399 415 30', 'ISIS NERY', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(721, '029 399 415 30', 'IRIS NERY', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(722, '', 'CAMILA MACEDO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(723, '', 'ILMA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(724, '', 'ISABELA FILHA DE HILAIDE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(725, '', 'DUDA FILHA DE ALESSANDRA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(726, '', 'ADRIANA SOGRA DE LUIZA REIS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(727, '', 'CARLA LEITE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(728, '', 'FERNANDA TAVARES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(729, '', 'JULIETA BEZZERRA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(730, '418 634 294 68', 'ANTONIA DA SILVA ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(731, '027 977 874 74', 'FLAVIA MIRANDA', '', '', NULL, NULL, NULL, '', '', '8799276728', NULL, NULL, NULL, NULL, 2, 0, NULL),
(732, '', 'SOFIA SECHI', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(733, '', 'ANA CLARA SECHI', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(734, '075 300 114 48', 'MONISE CARVALHO MAGALHOES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1989-03-30'),
(735, '057 648 094 01', 'ISABELA ALENCAR', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(737, '581 270 546 15', 'JULIA TOZIZZO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(738, '', 'CLAUDIA SAMPAIO DE ALENCAR', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(739, '', 'LUIZA SOBRINHA DE ANDREIA BLOSSOM', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(740, '', 'CIBELLE COIMBRA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(741, '62790528420', 'VALQUIRIA AMORIM ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(742, '', 'DAFE SSENA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(743, '', 'ROSANA DIAS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(744, '', 'NATALIA LISBOA SILVA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1996-08-26'),
(745, '70509832415', 'CELMA RAMOS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(746, '', 'VICTOR', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(747, '', 'BRENDA REGIS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(748, '01490665463', 'LUDIMILA AMORIM MAGALHOES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1987-12-23'),
(749, '', 'GEISE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(750, '', 'TALITA CARVALHO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(751, '', 'DMAZZA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(752, '', 'ZILDETE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(753, '', 'GIANE NUNES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(754, '08686740448', 'KELLY DINIZ COELHO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(755, '', 'ERI', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(756, '03690704456', 'PATRICIA CAVALCANTE', '', '', NULL, NULL, NULL, '', '', '8733264151', NULL, NULL, NULL, NULL, 2, 0, NULL),
(757, '', 'ANA CLARA ROSA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(758, '08686740404448', 'WESLANY KELLY DINIZ COELHO ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(759, '', 'DøPECIO E LAURA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(760, '', 'SILVANA E FILHA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(761, '06936815430', 'CASSIA EMANUELA RAMOS', '', '', NULL, NULL, NULL, '', '', '8788029363', NULL, NULL, NULL, NULL, 2, 0, NULL),
(762, '', 'FERNADO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(763, '03037302429', 'JULIANA GODOY BEZERRA ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(764, '', 'MICHELE DE SOLZA BRITO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(765, '', 'ANA PAULA PESSOA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(766, '', 'LAYSA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(767, '', 'FERNDA LINS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(768, '', 'RITA MONTEIRO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(769, '', 'CRISTINA PESSOA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(770, '04182361512', 'JOICE REQUIAO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(771, '05413809581', 'ANA ARIELA AGUIAR MAGALHAES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(772, '', 'AYLA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(773, '', 'JULIANE AGUIAR DE MOURA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(774, '', 'MARIA EDUARDA ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(775, '', 'ANTONIA SAMPAIO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(776, '', 'SHIRLEY', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(777, '', 'LIVIA CELMA RODRIGUES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(778, '', 'LAYANE GONSALVES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(779, '', 'TARCIANA VELOSO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(780, '', 'MARIA DEOLINO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(781, '', 'MARILIA ROBERTA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(782, '', 'ANGELA COELHO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(783, '', 'MONISE MAGALHAES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(784, '', 'AUXILIADORA TIA DE ANDIRA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(785, '', 'KETLEY', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(786, '', 'FILHA DE KAREM', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(787, '07817360480', 'MORGANA ALMEIDA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(788, '', 'EDUARDA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(789, '80345166434', 'EDNA SANTOS DE BARROS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(790, '02931646490', 'MIRNA COELHO ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(791, '', 'KASSIA BAHIA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(792, '', 'BELA TIA DE LORENA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(793, '46451099449', 'DINANI gomes AMORIM', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(794, '', 'THAIS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(795, '', 'JESSICA SIQUEIRA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(796, '962 062 924 87', 'LECIA REIS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(797, '044 700 004 77', 'RAFAELA MORAIS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(798, '002 286 033 90', 'JOANA HELOISA ALVES PAIXAO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(799, '', 'FABRICIA SOARES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(800, '401 608 155 68', 'GRACIETE OLIVEIRA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(801, '', 'JULIANE NUNES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(802, '', 'CARLINHA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(803, '', 'ROBERTA GRACIELLE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(804, '', 'CELIA REGINA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(805, '', 'MARIA HELENA BRADÇO ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(806, '', 'CAMILA CORREIA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(807, '', 'BEATRIZ NOGUEIRA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(808, '', 'MICHELLE BRITO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(809, '', 'MARLY SOARES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(810, '', 'JOSE RAU', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(811, '', 'ANA LETICIA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(812, '591 371 264 15', 'AVANIRA PEREIRA DE LIMA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(813, '456 217 724 15', 'TELMA MOREIRA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(814, '', 'GEOVANA DURANDO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(815, '070 935 994 27', 'EREICA CAVALCANTE ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(816, '', 'FILHA DE MANOELA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(817, '', 'SOBRINHA DE MANOELA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(818, '', 'JAMILY BEZZERRA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(819, '026 420 645 22', 'LARA CAFE ANTUNES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(820, '', 'JESIANE RODRIGUES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(821, '620 365 305 68', 'VERONICA MESQUITA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(822, '', 'ALINE BRASILEIRO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(823, '', 'CAROL GOMES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(824, '', 'JAQUELINE RIOS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(825, '', 'GRAZIELA TORRES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(826, '', 'LANA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(827, '', 'RENATA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(828, '', 'LIVIA CIRILO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(829, '', 'LETICIA GUIMARAES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(830, '', 'DAYANNA KELY', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(831, '', 'MARIA CAROLINA GOMES VASCONCELOS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(832, '', 'FERNANDA RAMOS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(833, '', 'MARLY MAYA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(834, '', 'LETICIA COELHO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(835, '01477090592', 'PAULA SOUSA GASPAR SANJUAN ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1986-09-09'),
(836, '', 'GABRIELA TORRES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(837, '070 874 845 75', 'mikaely felix', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(838, '202 224 675 68', 'LUIZIANA PEREIRA GOMES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(839, '747 212 914 34', 'APARECIDA GOMES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(840, '', 'JESSICA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(841, '', 'LIDIANE GOMDIM', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(842, '', 'TATIANA SOLZA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(843, '', 'JULIA VELOSO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(844, '', 'FATIMA SECHE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(845, '', 'KATIA MATOS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(846, '', 'MARIANA FILHA DE THELVIA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(847, '', 'VALENTINA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(848, '', 'TARCIANA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(849, '', 'MATEUS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(850, '', 'LAISSA BRAZ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(851, '454 454 354 15', 'LUCINHA CASCÇO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(852, '', 'FERNANDA BRADÇO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(853, '', 'ANA CROLINA MARTINS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(854, '', 'PAULA FIORI', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(855, '', 'PATRICIA PORTURI', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(856, '', 'MILENE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(857, '', 'TARCIANA FILHA E AMIGA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(858, '', 'NILZA LUZ DE AMORIM', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(859, '', 'FATIMA SOBRAL', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(860, '', 'JOANA SOBRAL', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(861, '', 'IVONETE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(862, '', 'KATIA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(863, '', 'HELENA CAPELAR', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(864, '', 'THAISE CERQUEIRA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(865, '', 'CARMEM', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(866, '', 'PAULA PRIORI', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(867, '', 'FERNANDA COELHO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(868, '', 'LINDA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(869, '', 'SILVANA CIHIM', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(870, '', 'IONARA CARRARO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(871, '', 'GLEICE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(872, '', 'CAROLINE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(873, '', 'FERNANDA ELIZA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(874, '', 'JOANA PORTELA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(875, '', 'MAZZA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(876, '', 'MARIA LUIZA COELHO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(877, '', 'MONACITA MOURA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(878, '', 'FILHO DE IANDRA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(879, '', 'LIVIA IRMA DE CLEIA AFRANIO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(880, '', 'LENO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(881, '', 'MARIANA ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL);
INSERT INTO `clientes` (`cli_id`, `cli_cpfcnpj`, `cli_nome`, `cli_endereco`, `cli_cep`, `cli_cidade`, `cli_estado`, `cli_bairro`, `cli_rg`, `cli_email`, `cli_telefone`, `cli_dt_add`, `deleted_at`, `created_at`, `updated_at`, `emp_id`, `usu_id`, `cli_nascimento`) VALUES
(882, '', 'EDNEIA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(883, '', 'GEISIANE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(884, '', 'KAELLENY', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(885, '', 'DEILIANE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(886, '', 'JULIANA OLIVEIRA DE ANDRADE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(887, '', 'BIBI', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(888, '', 'PRISCILA BARACHO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(889, '007 798 444 75', 'NALENE COSTA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(890, '', 'CRISTINA DINIZ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(891, '', 'VITORIA CAROLINA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(892, '', 'JENIFFER NOIVA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(893, '', 'LARA AMIGA DE CAROL TONIZZO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(894, '', 'CINTHIA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(895, '', 'CLEICIANE REIS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(896, '', 'DANIELLE CARVALHO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(897, '', 'MORGANA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(898, '', 'GLEIDE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(899, '', 'LUCINDA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(900, '', 'jessica richelle', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(901, '', 'aldi', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(902, '38910438568', 'FATIMA BRITO ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1967-03-09'),
(903, '', 'lorena desdara', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(904, '', 'lucieide', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(905, '', 'ivonete e raysa', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(906, '', 'erika leite', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(907, '', 'ANDRESSA SALES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(908, '', 'FERNANDA CAVALCANTE MASANGANO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(909, '', 'JOILDA avelino cabral', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(910, '', 'HELIO JARBAS MACEDO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(911, '', 'FERNADA LUIZA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(912, '03255207474', 'CINTIA MIKAELE GRANDE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(913, '', 'LETICIA VILARIN', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(914, '', 'ELKE CAVALCANTE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(915, '99084805534', 'ARIANE EVANGELISTA DA SILVA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(916, '', 'juliana lira', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(917, '', 'tamires coelho', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(918, '10337948488', 'MARIA DAS GRACAS PEREIRA PACHECO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(919, '', 'RAFAELA ALBUQUERQUE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(920, '', 'RITA CARDIN', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(921, '', 'ivaneide nunes leal', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(922, '', 'thais sechec', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(923, '00867421402', 'geovana martins', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(924, '', 'cassia guimaraes', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(925, '043521124435', 'jane lira', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(926, '', 'samara cristina', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(927, '', 'LUDMILA VIANA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(928, '', 'MARINA ALVES AMADOR', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(929, '', 'VALQUIRIA NUNES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(930, '', 'GEISY RODRIGUES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(931, '05603771429', 'sofia durando ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(932, '70428995470', 'alice arlergo tavares', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(933, '', 'mariana reis', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(934, '', 'carine falcao', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(935, '', 'gabriela cosentno', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(936, '10481061606', 'julia bueno silveira', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(937, '', 'MARCIA MINODA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(938, '', 'MARIA EUGENIA LINS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(939, '01204801401', 'LIDIANE DE ALMEIDA CAMELO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1981-03-23'),
(940, '09540353483', 'LAIANA FERRARI ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1994-11-30'),
(941, '17366976334', 'MARCIA MARIA VASCONCELOS DE DEUS', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1962-08-10'),
(942, '122287', 'GIOVANA COSTA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-25 19:53:33', 2, 0, '1970-01-01'),
(943, '122287', 'GIOVANA COSTA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(944, '', 'JADY SOUZA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(945, '04541394464', 'jennifan freire', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1984-02-04'),
(946, '77052170582', 'JOEDNA GASPAR', '', '', NULL, NULL, NULL, '', '', '38618357', NULL, NULL, NULL, NULL, 2, 0, '1974-03-04'),
(947, '12150788462', 'AMANDA ROCHA PEREIRA ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1996-10-18'),
(948, '09749556470', 'BEATRIZ COSTA DE CARVALHO SILVA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1999-08-26'),
(949, '073511344405', 'larisa lourena carvalho', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1990-05-11'),
(950, '', 'LUSINEIDE LACARD', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(951, '05816622536', 'JULIANA SANTOS SOUSA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1993-10-14'),
(952, '', 'FRAM', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(953, '07927197436', 'bruna gomes e sa matos', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1992-11-20'),
(954, '05640714476', 'gabriela batista carvalho dantas torres', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1989-01-11'),
(955, '03804524486', 'AURIANA NUNES DE SOUSA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1982-11-12'),
(956, '00787465402', 'DANIELA MOUSINH0', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(957, '03500503500', 'JACYARA ALVES POSIDONE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1984-08-28'),
(958, '63275880420', 'GEORGIA CRISTINA FRANCA DE ARAUJO GOMES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1966-12-21'),
(959, '', 'ELIZABETH CUNHADA DE ANA MARIA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(960, '87010618300', 'lilian melo ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1981-11-24'),
(961, '05874236414', 'Adna Allana Coelho Andrade', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-25 19:54:26', 2, 0, '1986-10-24'),
(962, '07404552494', 'caroline gonsalves da silva carvalho', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1988-10-11'),
(963, '02946971449', 'nelma maria da costa ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1943-05-05'),
(964, '01434557456', 'bernardo de possidio estrela lustosa', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(965, '05485131719', 'patricia brettas sesto', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(966, '', 'erica e sofia ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(967, '08133783461', 'graca coelho', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1988-03-28'),
(968, '41816781487', 'lucelia ramos estrela', '', '', NULL, NULL, NULL, '', '', '999920764', NULL, NULL, NULL, NULL, 2, 0, '1958-07-18'),
(969, '09164907422', 'wildy mello', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1990-04-18'),
(970, '', 'ana roberta', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(971, '00841237530', 'renata lecrim de amorim lim viana ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1993-10-03'),
(972, '', 'joao vitor soares', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(973, '09969685473', 'aline forte', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1994-02-12'),
(974, '10315903406', 'jessica jardim marques venezes ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(975, '', 'niuciene marques', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(976, '24878405449', 'TEREZA NEUMA SANTANA GAMA', '', '', NULL, NULL, NULL, '', '', '8799031266', NULL, NULL, NULL, NULL, 2, 0, NULL),
(977, '', 'sara noiva', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(978, '', 'kailine falcao', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(979, '', 'luiza viana', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(980, '00621895571', 'erica passos', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(981, '31084940400', 'geovaneide de carvalho', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(982, '', 'lora irma de ivania', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(983, '03643267436', 'IVY ALVES', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1983-08-19'),
(984, '44663382487', 'AMILDA SOLANO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1964-11-25'),
(985, '75728168272', 'ligiane marino pires', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(986, '05699294406', 'tereza cavalcanti de andrade', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(987, '05498286562', 'gleise naldi', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(988, '91848024568', 'france angolo sa ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1979-04-26'),
(989, '', 'aline matos', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(990, '91892147491', 'tarciana passos ribeiro', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(991, '', 'erika ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(992, '06107291440', 'maira thiebaut', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(993, '05979739467', 'TARCIANA GOUVEIA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1984-10-18'),
(994, '', 'lara valquiria', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(995, '05958567489', 'paloma nunes ramos ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1986-12-08'),
(996, '', 'ivamice jatoba', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(997, '', 'francisca gilvanizia delmondes', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(998, '00035035420', 'KESIA SAMPAIO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1975-12-02'),
(999, '', 'lusileide reis', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(1000, '62833405472', 'joselita santos ferreira', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1969-06-20'),
(1001, '05636032404', 'debora brito', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1985-12-02'),
(1002, '', 'lis carvalho', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(1003, '00305478303', 'aurea vilela ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1983-07-22'),
(1004, '', 'SAMARA RAQUEL', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(1005, '', 'ana lima', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(1006, '04895542530', 'luma katiuse mendes paiva ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1993-11-03'),
(1007, '', 'barbara cruz ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(1008, '', 'regina valenca', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(1009, '02761961803', 'ivonete almeida lima gomes ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(1010, '90599012587', 'katia cristina de araujo', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1975-09-14'),
(1011, '03653387450', 'CLEIDE SELMA SOUSA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1981-07-16'),
(1012, '01742729100', 'FRANCIELE PARIZZOTTO', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1987-06-22'),
(1013, '', 'maria aparecida de souza silva', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(1014, '02410015522', 'janda yara nunes da silva', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1987-01-27'),
(1015, '06498643447', 'TALINE CLARA LIRA DE NASCIMENTO ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1987-11-30'),
(1016, '', 'MARIA TEREZA CAVALCANTE DE ANDRADE', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(1017, '', 'maria alves pereira moreira', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(1018, '', 'carol', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(1019, '03345155567', 'dulce maria nascimento pires sumiya', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1987-04-23'),
(1020, ' 06493941431', 'karla prscila drumond ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(1021, '', 'joelma nascimento', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(1022, '07980542479', 'janaina de sousa nunes ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1990-12-21'),
(1023, '09219736403', 'FERNANDA FELIX', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1991-10-15'),
(1024, '06716262482', 'ILKA JULIANA FERREIRA RODRIGUES ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1987-05-05'),
(1025, '36173029472', 'LETICIA DE POSSIDEO ESTRELAS LUSTOSA', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1961-02-15'),
(1026, '05764809401', 'IZABELA CATARINA CARVALHO DE ALENCAR ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(1027, '', 'erica sabrina noiva', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(1028, '', 'eda cristina oliva ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(1029, '77666429568', 'guardalupe braga ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(1030, '', 'valeria plenus ', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(1031, '82582173487', 'esmelinda de araujo coelho', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, '1974-06-29'),
(1032, '', 'elielza almeida alves', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, 2, 0, NULL),
(1033, '34634589', 'Daivert', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-25 15:28:24', '2017-11-01 05:51:29', 1, 1, '2017-10-01'),
(1037, NULL, 'ADOBIANE  MARIA CARVALHO', 'RUA ESTRELA FENIS', NULL, NULL, '0', '101 APARTAMENTO', NULL, NULL, '88544967', NULL, NULL, '2018-01-05 16:12:06', '2018-01-05 16:12:06', 2, 4, '2018-01-01'),
(1038, NULL, 'ADRIANA ARAUJO', 'AVENIDA CARDOSO DE A N 1065', NULL, NULL, '0', 'CENTRO', NULL, NULL, '88316695', NULL, NULL, '2018-01-05 16:16:07', '2018-01-05 16:16:07', 2, 4, '2018-01-01'),
(1039, NULL, 'ADRIANA COELHO', NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-05 16:20:15', '2018-01-05 16:20:15', 2, 4, '2018-01-01'),
(1040, NULL, 'ADRIANA DE CALDAS PEREIRA', 'AVENIDA CARDOSO DE SA N 1175', NULL, NULL, '0', 'ORLA', NULL, NULL, '88192023', NULL, NULL, '2018-01-05 16:21:38', '2018-01-05 16:21:38', 2, 4, '2018-01-01'),
(1041, NULL, 'ADRIANA MENESES', 'RUA LUCAS ROBERTO DE ARAUJO', NULL, NULL, '0', 'PETROLINA', NULL, NULL, '88029992', NULL, NULL, '2018-01-05 16:27:06', '2018-01-05 16:27:06', 2, 4, '2018-01-01'),
(1042, NULL, 'ADRIANA PAIXAO', 'RUA 01 N 81', NULL, NULL, '0', 'VILA BARBOSA', NULL, NULL, '88091376', NULL, NULL, '2018-01-05 16:28:24', '2018-01-05 16:28:24', 2, 4, '2018-01-01'),
(1043, NULL, 'ADRIANA TORQUATO', NULL, NULL, NULL, '0', NULL, NULL, NULL, '88360036', NULL, NULL, '2018-01-05 16:30:59', '2018-01-05 16:30:59', 2, 4, '2018-01-01'),
(1044, NULL, 'ALANA CECILIA BATISTA LIMA', 'R SENADOR CANDIDO FERRAZ N 97', NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-05 16:34:45', '2018-01-05 16:34:45', 2, 4, '2018-01-01'),
(1045, NULL, 'ALANA GUERRA BASTOS FREIRE', 'RUA MANUEL ANTONIO GALDINO N 70', NULL, NULL, '0', 'ATRAS DA BANCA', NULL, NULL, '99527040', NULL, NULL, '2018-01-05 16:36:00', '2018-01-05 16:36:00', 2, 4, '2018-01-01'),
(1046, NULL, 'ALBANI RODRIGUES DA CRUZ', 'RUA DO BARNCO N 140', NULL, NULL, '0', 'CAMINHO DO SOL', NULL, NULL, '88555755', NULL, NULL, '2018-01-05 16:37:06', '2018-01-05 16:37:06', 2, 4, '2018-02-01'),
(1047, NULL, 'ALBERTINA RODRIGUES', 'RUA PRESIDENTE COSTA E SILVA', NULL, NULL, '0', 'LOTEAMENTO', NULL, NULL, NULL, NULL, NULL, '2018-01-05 16:38:16', '2018-01-05 16:38:16', 2, 4, '2018-01-01'),
(1048, NULL, 'ALESSANDRA ROSENDO', 'RUA RAIMUNDO LACERDA', NULL, NULL, '0', 'VILA DOS INGAS', NULL, NULL, NULL, NULL, NULL, '2018-01-05 16:39:11', '2018-01-05 16:39:11', 2, 4, '2018-01-01'),
(1049, NULL, 'ALESSANDRA CIRILO', 'CASA 26', NULL, NULL, '0', 'VILA EULALIA', NULL, NULL, '981383838', NULL, NULL, '2018-01-05 16:40:20', '2018-01-05 16:40:20', 2, 4, '2018-01-01'),
(1050, NULL, 'ALESANDRA FALCAO REIS', 'RUA VALERIO PEREIRA N 122', NULL, NULL, '0', 'CENTRO', NULL, NULL, '88353793', NULL, NULL, '2018-01-05 16:42:46', '2018-01-05 16:42:46', 2, 4, '2018-01-01'),
(1051, NULL, 'ALESANDRA GOMES MARQUES', 'ELIS REGINA COM PORTAL DAS AGUAS', NULL, NULL, '0', 'PEDRA DO BODE', NULL, NULL, '88439038', NULL, NULL, '2018-01-05 16:44:09', '2018-01-05 16:44:09', 2, 4, '2018-01-01'),
(1052, NULL, 'ALESSANDRA PALMAS', 'AVENIDA CARDOSO DE SA 1125 APART 1203', NULL, NULL, '0', 'CENTRO', NULL, NULL, NULL, NULL, NULL, '2018-01-05 16:45:18', '2018-01-05 16:45:18', 2, 4, '2018-01-01'),
(1053, NULL, 'ALEXSON CLEITON DA PAIXAO', 'AVENIDA MIGUEL SILVA SOLSA N 100', NULL, NULL, '0', 'VILLA BELA', NULL, NULL, '7488010664', NULL, NULL, '2018-01-05 16:46:41', '2018-01-05 16:46:41', 2, 4, '2018-01-01'),
(1054, NULL, 'ALICE ALERGO', 'RUA CANDINDO MIRANDA N 240', NULL, NULL, '0', 'PETROLINA', NULL, NULL, '38622748', NULL, NULL, '2018-01-05 16:48:10', '2018-01-05 16:48:10', 2, 4, '2018-01-01'),
(1055, NULL, 'ALICE NARYANE', 'RUA ARCO IRIS', NULL, NULL, '0', 'CANTRE CLUBE', NULL, NULL, '88193772', NULL, NULL, '2018-01-05 16:48:57', '2018-01-05 16:48:57', 2, 4, '2018-01-01'),
(1056, NULL, 'ALINA REBECA MONTEIRO', 'AVE. CARDOSO DE SA APART, 202 N 125', NULL, NULL, '0', 'CENTRO', NULL, NULL, '8796255939', NULL, NULL, '2018-01-05 16:51:05', '2018-01-05 16:51:05', 2, 4, '2018-01-01'),
(1057, NULL, 'ALINE BRASIELIRO', NULL, NULL, NULL, '0', NULL, NULL, NULL, '8396005648', NULL, NULL, '2018-01-05 16:53:15', '2018-01-05 16:53:15', 2, 4, '2018-01-01'),
(1058, NULL, 'ALINE CAPELARIO', 'AVEN DA INTEGRACAO N 685', NULL, NULL, '0', 'SAO JOSE', NULL, NULL, '88069478', NULL, NULL, '2018-01-05 16:54:14', '2018-01-05 16:54:14', 2, 4, '2018-01-01'),
(1059, NULL, 'ALINE CARVALHO NANGELIM LUZ', 'RUA DA JETUANA', NULL, NULL, '0', 'AREIA BRANCA', NULL, NULL, '8796066468', NULL, NULL, '2018-01-05 16:55:22', '2018-01-05 16:55:22', 2, 4, '2018-01-01'),
(1060, NULL, 'ALINE FORTE', NULL, NULL, NULL, '0', NULL, NULL, NULL, '8688240465', NULL, NULL, '2018-01-05 16:55:58', '2018-01-05 16:55:58', 2, 4, '2018-01-01'),
(1061, NULL, 'ALINE KELLY FERREIRA MATOS', 'RUA ESTRELA FENIX  N 140', NULL, NULL, '0', 'VILA EDUARDO', NULL, NULL, '88427173', NULL, NULL, '2018-01-05 16:57:04', '2018-01-05 16:57:04', 2, 4, '2018-01-01'),
(1062, NULL, 'ALINE LAZARE', 'AVE. DA INTEGRACAO 685 APART.  1605 BLOCO 02', NULL, NULL, '0', 'SAO JOSE', NULL, NULL, '8781024440', NULL, NULL, '2018-01-05 16:58:30', '2018-01-05 16:58:30', 2, 4, '2018-01-01'),
(1063, NULL, 'ALINNE DURANDO', 'TRAVESSA DO REDENTO N 75', NULL, 'PETROLINA', '0', 'PALHINHAS', NULL, NULL, '88217544', NULL, NULL, '2018-01-05 18:02:47', '2018-01-05 18:02:47', 2, 4, '2018-01-01'),
(1064, NULL, 'AMANDA ROCHA PEREIRA', 'RUA ANANIAS', NULL, 'PETROLINA', '0', 'VILA DEBORA N 151', NULL, NULL, '99598176', NULL, NULL, '2018-01-05 18:04:48', '2018-01-05 18:04:48', 2, 4, '2018-01-01'),
(1065, NULL, 'AMANDA SANTOS ARAUJO', 'AVENIDA CARDOSO DE SA N 1065', NULL, NULL, '0', 'ORLA', NULL, NULL, '880140060', NULL, NULL, '2018-01-05 18:05:58', '2018-01-05 18:05:58', 2, 4, '2018-01-01'),
(1066, NULL, 'AMANDA THAISA DE OLIVEIRA', 'CONDOMINIO SOL NASCENTE  N 220 ETAPA 02', NULL, 'PETROLINA', '0', 'CIDADE', NULL, NULL, '98061620', NULL, NULL, '2018-01-05 18:07:18', '2018-01-05 18:07:18', 2, 4, '2018-01-01'),
(1067, NULL, 'AMILDA SOLANO', 'AV MEUQUIDES QUIRINO 320', NULL, NULL, '0', NULL, NULL, NULL, '988516387', NULL, NULL, '2018-01-05 18:19:32', '2018-01-05 18:19:32', 2, 4, '2018-01-01'),
(1068, NULL, 'ANA ANGELICA', 'AVE. CARDOSO DE SA   1125 APART. 1904', NULL, 'PETROLINA', '0', 'CENTRO', NULL, NULL, '8788074585', NULL, NULL, '2018-01-05 18:22:22', '2018-01-05 18:22:22', 2, 4, '2018-01-01'),
(1069, NULL, 'ANA ARIELA AGUIAR MAGALHAES', 'AVENIDA JATOBA N 121', NULL, NULL, '0', 'CIDADE', NULL, NULL, NULL, NULL, NULL, '2018-01-05 18:23:53', '2018-01-05 18:23:53', 2, 4, '2018-01-01'),
(1070, NULL, 'ANA CARLA RODRIGUES', 'AVENIDA CARDOSO DE SA 13', NULL, NULL, '0', 'CENTRO', NULL, NULL, NULL, NULL, NULL, '2018-01-05 18:24:48', '2018-01-05 18:24:48', 2, 4, '2018-01-01'),
(1071, NULL, 'ANA CAROLINA OLIVEIRA', 'RUA MARCOS PASSOS N 446', NULL, NULL, '0', 'VILA EDUARDO', NULL, NULL, '7488080809', NULL, NULL, '2018-01-05 18:29:21', '2018-01-05 18:29:21', 2, 4, '2018-01-01'),
(1072, NULL, 'ANA CAROLINA PEREIRA DE LINS', 'RUA GENERAL SILVA JR N 640', NULL, NULL, '0', 'FATIMA', NULL, NULL, '8582171775', NULL, NULL, '2018-01-05 18:38:12', '2018-01-05 18:38:12', 2, 4, '2018-01-01'),
(1073, NULL, 'ANA CATARINA SIMOES', 'RUA VALERIO PEREIRA APART. 601', NULL, 'PETROLINA', '0', NULL, NULL, NULL, '8199225316', NULL, NULL, '2018-01-05 18:40:00', '2018-01-05 18:40:00', 2, 4, '2018-01-01'),
(1074, NULL, 'ANA JESSICA MAMEDIO AZEVEDO', 'IPE PARQUE AZEVEDO 251', NULL, NULL, '0', 'COHAB MASSANGANO', NULL, NULL, '8799904878', NULL, NULL, '2018-01-05 18:46:53', '2018-01-05 18:46:53', 2, 4, '2018-01-01'),
(1075, NULL, 'denia reis', NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-18 16:07:56', '2018-01-18 16:07:56', 2, 4, '2018-01-01'),
(1076, NULL, 'graca noiva', NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-18 16:47:51', '2018-01-18 16:47:51', 2, 4, '2018-01-01'),
(1077, NULL, 'maria de lourdes torres', 'rua do agave 296', '56150330', 'PETROLINA', '0', 'area branca', NULL, NULL, '38641897', NULL, NULL, '2018-01-26 17:07:34', '2018-01-26 17:07:34', 2, 4, '1948-11-15'),
(1078, '22222', 'Cliente_Teste', 'Rua Beethoven, 76', '0492849', 'São Paulo', '25', 'Vahalla', '22222', 'cliente@gmail.com', '2323232', NULL, NULL, '2018-02-03 02:40:30', '2018-02-03 02:40:30', 2, 6, '1970-05-12');

-- --------------------------------------------------------

--
-- Estrutura para tabela `comandas`
--

CREATE TABLE IF NOT EXISTS `comandas` (
  `cmd_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `movcx_id` int(11) NOT NULL,
  `cli_id` int(10) UNSIGNED NOT NULL,
  `usu_id` int(10) UNSIGNED NOT NULL,
  `cmd_data` date NOT NULL,
  `cmd_subtotal` double(8,2) NOT NULL,
  `cmd_total` double(8,2) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `emp_id` int(10) UNSIGNED NOT NULL,
  `cmd_status` int(11) NOT NULL,
  PRIMARY KEY (`cmd_id`),
  KEY `comandas_cli_id_foreign` (`cli_id`),
  KEY `comandas_usu_id_foreign` (`usu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `comandas`
--

INSERT INTO `comandas` (`cmd_id`, `movcx_id`, `cli_id`, `usu_id`, `cmd_data`, `cmd_subtotal`, `cmd_total`, `deleted_at`, `created_at`, `updated_at`, `emp_id`, `cmd_status`) VALUES
(4, 1, 1033, 1, '2017-10-27', 30.00, 30.00, NULL, '2017-10-25 16:24:37', '2017-11-21 23:12:36', 1, 2),
(8, 1, 1033, 1, '2017-11-03', 10.00, 10.00, NULL, '2017-11-01 05:45:27', '2017-11-28 05:52:02', 1, 2),
(9, 2, 540, 4, '2017-11-25', 0.00, 0.00, NULL, '2017-11-25 14:14:16', '2017-11-25 14:16:00', 2, 2),
(10, 2, 620, 4, '2017-11-25', 0.00, 0.00, NULL, '2017-11-25 14:27:13', '2017-11-25 14:27:49', 2, 2),
(13, 2, 961, 4, '2017-11-28', 45.00, 45.00, NULL, '2017-11-28 16:59:53', '2017-11-28 17:20:12', 2, 2),
(16, 8, 1024, 4, '2018-02-21', 45.00, 45.00, NULL, '2018-02-21 16:59:53', '2018-02-21 17:02:41', 2, 1),
(17, 8, 379, 4, '2018-03-01', 0.00, 0.00, NULL, '2018-03-02 00:47:08', '2018-03-02 00:47:08', 2, 1),
(18, 8, 207, 4, '2018-03-02', 0.00, 0.00, NULL, '2018-03-02 22:01:21', '2018-03-02 22:01:21', 2, 1),
(19, 8, 207, 4, '2018-03-02', 0.00, 0.00, NULL, '2018-03-02 22:09:50', '2018-03-02 22:09:50', 2, 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `comissoes`
--

CREATE TABLE IF NOT EXISTS `comissoes` (
  `cms_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `prof_id` int(10) UNSIGNED NOT NULL,
  `cli_id` int(10) UNSIGNED NOT NULL,
  `prosrv_id` int(10) UNSIGNED NOT NULL,
  `cms_data` date NOT NULL,
  `cms_qtde` double(8,2) NOT NULL,
  `cms_valor` double(8,2) NOT NULL,
  `cms_pcomissao` double(8,2) NOT NULL,
  `cms_vcomissao` double(8,2) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `emp_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`cms_id`),
  KEY `comissoes_prof_id_foreign` (`prof_id`),
  KEY `comissoes_cli_id_foreign` (`cli_id`),
  KEY `comissoes_prosrv_id_foreign` (`prosrv_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `comissoes`
--

INSERT INTO `comissoes` (`cms_id`, `prof_id`, `cli_id`, `prosrv_id`, `cms_data`, `cms_qtde`, `cms_valor`, `cms_pcomissao`, `cms_vcomissao`, `deleted_at`, `created_at`, `updated_at`, `emp_id`) VALUES
(1, 16, 1033, 70, '2017-11-28', 1.00, 10.00, 30.00, 3.00, NULL, '2017-11-28 05:52:02', '2017-11-28 05:52:02', 1),
(2, 10, 961, 42, '2017-11-28', 1.00, 23.00, 42.00, 9.66, NULL, '2017-11-28 17:20:11', '2017-11-28 17:20:11', 2);

-- --------------------------------------------------------

--
-- Estrutura para tabela `empresas`
--

CREATE TABLE IF NOT EXISTS `empresas` (
  `emp_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `emp_nome` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `emp_fantasia` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emp_cpfcnpj` varchar(14) COLLATE utf8_unicode_ci NOT NULL,
  `emp_ie` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `emp_iest` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `emp_im` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `emp_cnae` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `emp_crt` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `emp_tplogadouro` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emp_logadouro` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emp_numero` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emp_complemento` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emp_bairro` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emp_cep` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emp_cidade` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emp_estado` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emp_pais` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emp_telefone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emp_email` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `emp_gtoken` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'uadlets9sjl8kodmncmvoe6pb8@group.calendar.google.com',
  PRIMARY KEY (`emp_id`),
  UNIQUE KEY `empresas_emp_cpfcnpj_unique` (`emp_cpfcnpj`)
) ENGINE=InnoDB AUTO_INCREMENT=5 AVG_ROW_LENGTH=8192 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `empresas`
--

INSERT INTO `empresas` (`emp_id`, `emp_nome`, `emp_fantasia`, `emp_cpfcnpj`, `emp_ie`, `emp_iest`, `emp_im`, `emp_cnae`, `emp_crt`, `emp_tplogadouro`, `emp_logadouro`, `emp_numero`, `emp_complemento`, `emp_bairro`, `emp_cep`, `emp_cidade`, `emp_estado`, `emp_pais`, `emp_telefone`, `emp_email`, `deleted_at`, `created_at`, `updated_at`, `emp_gtoken`) VALUES
(1, 'SiD Soluções', 'SiD Soluções', '08664531000190', 'ISENTO', '', '', '', '1', 'Avenida', 'Av. Integração', '286', 'B', 'Jar Maravi', '56306150', 'Petrolina', 'PE', 'BR', '87996623680', NULL, NULL, NULL, NULL, 'uadlets9sjl8kodmncmvoe6pb8@group.calendar.google.com'),
(2, 'ANA MARIA & SOLANGE BRITO', 'ANA MARIA & SOLANGE BRITO', '24154739000116', 'ISENTO', '', '', '', '1', 'RUA', 'CANDIDO MIRANDA', '218', '', 'VILA INGÁS', '56300000', 'Petrolina', 'BA', 'BR', '87988414333', 'ede.brasil21@hotmail.com', NULL, '2017-10-20 01:36:00', NULL, '8mi7iimahkjmif3t91qg08i6ng@group.calendar.google.com'),
(3, 'Escovaria do Vale', 'Escovaria do Vale', '00000000000100', 'ISENTO', '', '', '', '1', 'Avenida', 'Av. Integração', '286', 'B', 'Jar Maravi', '56306150', 'Petrolina', 'PE', 'BR', '87996623680', NULL, NULL, NULL, NULL, 'uadlets9sjl8kodmncmvoe6pb8@group.calendar.google.com'),
(4, 'Espaço de Beleza Josane Sampaio', 'Espaço de Beleza Josane Sampaio', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Ipiaú', 'BA', NULL, '73999923126', 'jo.sampaio12@hotmail.com', NULL, NULL, NULL, 'uadlets9sjl8kodmncmvoe6pb8@group.calendar.google.com');

-- --------------------------------------------------------

--
-- Estrutura para tabela `historicos`
--

CREATE TABLE IF NOT EXISTS `historicos` (
  `hist_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cli_id` int(10) UNSIGNED NOT NULL,
  `hist_data` date NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`hist_id`),
  KEY `historicos_cli_id_foreign` (`cli_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `itens`
--

CREATE TABLE IF NOT EXISTS `itens` (
  `itn_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cmd_id` int(10) UNSIGNED NOT NULL,
  `prof_id` int(10) UNSIGNED NOT NULL,
  `prosrv_id` int(10) UNSIGNED NOT NULL,
  `itn_data` date NOT NULL,
  `itn_custo` double(8,2) DEFAULT NULL,
  `itn_preco` double(8,2) NOT NULL,
  `itn_qtde` double(8,2) DEFAULT NULL,
  `itn_subtotal` double(8,2) NOT NULL,
  `itn_desconto` double(8,2) NOT NULL,
  `itn_ucom` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `itn_total` double(8,2) NOT NULL,
  `itn_dt_alt` date NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `usu_id` int(10) UNSIGNED NOT NULL,
  `itn_vcomissao` float DEFAULT '0',
  PRIMARY KEY (`itn_id`),
  KEY `itens_prof_id_foreign` (`prof_id`),
  KEY `itens_prosrv_id_foreign` (`prosrv_id`),
  KEY `itens_usu_id_foreign` (`usu_id`),
  KEY `itens_cmd_id_foreign` (`cmd_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `itens`
--

INSERT INTO `itens` (`itn_id`, `cmd_id`, `prof_id`, `prosrv_id`, `itn_data`, `itn_custo`, `itn_preco`, `itn_qtde`, `itn_subtotal`, `itn_desconto`, `itn_ucom`, `itn_total`, `itn_dt_alt`, `deleted_at`, `created_at`, `updated_at`, `usu_id`, `itn_vcomissao`) VALUES
(8, 4, 16, 70, '2017-10-29', 0.00, 10.00, 3.00, 30.00, 0.00, 'UN', 30.00, '2017-10-29', NULL, '2017-10-29 22:01:25', '2017-10-29 22:01:25', 1, 0),
(9, 8, 16, 70, '2017-11-28', 0.00, 10.00, 1.00, 10.00, 0.00, 'UN', 10.00, '2017-11-28', NULL, '2017-11-28 05:43:57', '2017-11-28 05:43:57', 1, 3),
(10, 13, 10, 39, '2017-11-28', 0.00, 22.00, 1.00, 22.00, 0.00, 'UN', 22.00, '2017-11-28', NULL, '2017-11-28 17:00:48', '2017-11-28 17:00:48', 4, 8.58),
(11, 13, 10, 42, '2017-11-28', 0.00, 23.00, 1.00, 23.00, 0.00, 'UN', 23.00, '2017-11-28', NULL, '2017-11-28 17:16:06', '2017-11-28 17:16:06', 4, 9.66),
(15, 16, 14, 39, '2018-02-21', 0.00, 22.00, 1.00, 22.00, 0.00, 'UN', 22.00, '2018-02-21', NULL, '2018-02-21 17:02:19', '2018-02-21 17:02:19', 4, 8.8),
(16, 16, 13, 42, '2018-02-21', 0.00, 23.00, 1.00, 23.00, 0.00, 'UN', 23.00, '2018-02-21', NULL, '2018-02-21 17:02:40', '2018-02-21 17:02:40', 4, 11.5);

-- --------------------------------------------------------

--
-- Estrutura para tabela `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 AVG_ROW_LENGTH=1638 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_10_11_000000_create_clientes_table', 1),
(4, '2017_10_11_123239_create_sidsoluc_webservices_table', 1),
(5, '2017_10_14_195124_create_profissionais_table', 2),
(6, '2017_10_14_204450_create_historicos_table', 3),
(7, '2017_10_14_204650_create_produtosservicos_table', 4),
(8, '2017_10_14_204757_create_comissoes_table', 5),
(9, '2017_10_14_205048_create_comandas_table', 6),
(10, '2017_10_14_205327_create_itens_table', 7);

-- --------------------------------------------------------

--
-- Estrutura para tabela `movcaixas`
--

CREATE TABLE IF NOT EXISTS `movcaixas` (
  `movcx_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cx_id` int(10) UNSIGNED NOT NULL,
  `mov_saldo_ini` double(8,2) NOT NULL,
  `mov_sangria` double(8,2) DEFAULT NULL,
  `mov_suplemento` double(8,2) DEFAULT NULL,
  `mov_totalvenda` double(8,2) DEFAULT NULL,
  `mov_pago` double(8,2) DEFAULT NULL,
  `mov_dinheiro` double(8,2) DEFAULT NULL,
  `mov_duplicata` double(8,2) DEFAULT NULL,
  `mov_cheque` double(8,2) DEFAULT NULL,
  `mov_cartao` double(8,2) DEFAULT NULL,
  `mov_desconto` double(8,2) DEFAULT NULL,
  `mov_totalliquido` double(8,2) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `usu_id` int(10) UNSIGNED DEFAULT NULL,
  `usu_id_fechou` int(11) DEFAULT NULL,
  `emp_id` int(10) UNSIGNED DEFAULT NULL,
  `dt_fecha` date DEFAULT NULL,
  `dt_aberto` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`movcx_id`),
  KEY `movcaixas_cx_id_foreign` (`cx_id`),
  KEY `movcaixas_usu_id_foreign` (`usu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `movcaixas`
--

INSERT INTO `movcaixas` (`movcx_id`, `cx_id`, `mov_saldo_ini`, `mov_sangria`, `mov_suplemento`, `mov_totalvenda`, `mov_pago`, `mov_dinheiro`, `mov_duplicata`, `mov_cheque`, `mov_cartao`, `mov_desconto`, `mov_totalliquido`, `status`, `usu_id`, `usu_id_fechou`, `emp_id`, `dt_fecha`, `dt_aberto`, `created_at`, `updated_at`) VALUES
(1, 1, 1.00, 0.00, 0.00, 10.00, 0.00, 250.00, 0.00, 0.00, 0.00, 0.00, 10.00, 1, 1, NULL, 1, NULL, '2017-11-21', '2017-11-21 22:47:33', '2017-11-28 05:52:02'),
(8, 2, 28.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 1, 4, NULL, 2, NULL, '2018-02-02', '2018-02-02 18:43:10', '2018-02-02 18:43:10');

-- --------------------------------------------------------

--
-- Estrutura para tabela `pagamentos`
--

CREATE TABLE IF NOT EXISTS `pagamentos` (
  `pag_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cmd_id` int(10) UNSIGNED NOT NULL,
  `pag_historico` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tpg_id` int(10) UNSIGNED DEFAULT NULL,
  `pag_num` int(11) DEFAULT NULL,
  `pag_qtdedias` int(11) DEFAULT NULL,
  `pag_vencimento` date DEFAULT NULL,
  `pag_dtpagamento` date DEFAULT NULL,
  `pag_numcheque` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pag_agcheque` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pag_cccheque` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pag_cpfcheque` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pag_emitcheque` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pag_numordcard` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bco_id` int(11) DEFAULT NULL,
  `car_id` int(11) DEFAULT NULL,
  `pag_valor` double(8,2) NOT NULL,
  `pag_vlrpago` double(8,2) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`pag_id`),
  KEY `pagamentos_cmd_id_foreign` (`cmd_id`),
  KEY `pagamentos_tpg_id_foreign` (`tpg_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `pagamentos`
--

INSERT INTO `pagamentos` (`pag_id`, `cmd_id`, `pag_historico`, `tpg_id`, `pag_num`, `pag_qtdedias`, `pag_vencimento`, `pag_dtpagamento`, `pag_numcheque`, `pag_agcheque`, `pag_cccheque`, `pag_cpfcheque`, `pag_emitcheque`, `pag_numordcard`, `bco_id`, `car_id`, `pag_valor`, `pag_vlrpago`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(2, 4, 'Prestação serviço', 1, 1, 0, NULL, '2017-11-21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30.00, 30.00, 1, NULL, '2017-11-21 23:12:15', '2017-11-21 23:12:15'),
(3, 8, 'Prestação serviço', 1, 1, 0, NULL, '2017-11-28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10.00, 10.00, 1, NULL, '2017-11-28 05:44:11', '2017-11-28 05:44:11'),
(4, 13, 'Prestação serviço', 1, 1, 0, NULL, '2017-11-28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25.00, 25.00, 1, NULL, '2017-11-28 17:16:56', '2017-11-28 17:16:56'),
(5, 13, 'Prestação serviço', 2, 1, 0, NULL, '2017-11-28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 20.00, 20.00, 1, NULL, '2017-11-28 17:17:20', '2017-11-28 17:17:20'),
(6, 16, 'Prestação serviço', 1, 1, 0, NULL, '2018-10-02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25.00, 25.00, 1, NULL, '2018-10-02 22:00:49', '2018-10-02 22:00:49'),
(7, 16, 'Prestação serviço', 4, 1, 0, NULL, '2018-10-02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 20.00, 20.00, 1, NULL, '2018-10-02 22:01:07', '2018-10-02 22:01:07');

-- --------------------------------------------------------

--
-- Estrutura para tabela `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('phdorocha@gmail.com', '$2y$10$bxOgPbYCrbW6S3GoxtBO7u5H868suJQdlLA3iymjn/dE4h6MWJ6ye', '2017-11-01 05:44:15');

-- --------------------------------------------------------

--
-- Estrutura para tabela `produtosprofissionais`
--

CREATE TABLE IF NOT EXISTS `produtosprofissionais` (
  `proprof_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `prosrv_id` int(10) UNSIGNED NOT NULL,
  `prof_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `proprof_pcomissao` double(8,2) NOT NULL,
  PRIMARY KEY (`proprof_id`),
  KEY `produtosprofissionais_prof_id_foreign` (`prof_id`),
  KEY `produtosprofissionais_prosrv_id_foreign` (`prosrv_id`)
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `produtosprofissionais`
--

INSERT INTO `produtosprofissionais` (`proprof_id`, `prosrv_id`, `prof_id`, `created_at`, `updated_at`, `proprof_pcomissao`) VALUES
(4, 72, 16, '2017-11-27 07:35:34', '2017-11-27 07:35:34', 20.00),
(5, 70, 16, '2017-11-28 05:39:59', '2017-11-28 05:39:59', 30.00),
(6, 39, 10, '2017-11-28 16:29:48', '2017-11-28 16:29:48', 50.00),
(7, 39, 14, '2017-11-28 16:35:34', '2017-11-28 16:35:34', 40.00),
(8, 42, 10, '2017-11-28 17:15:23', '2017-11-28 17:15:23', 50.00),
(9, 37, 7, '2018-01-05 05:27:11', '2018-01-05 05:27:11', 50.00),
(10, 65, 7, '2018-01-05 05:28:39', '2018-01-05 05:28:39', 50.00),
(11, 23, 7, '2018-01-05 05:29:23', '2018-01-05 05:29:23', 40.00),
(12, 37, 5, '2018-01-05 05:30:36', '2018-01-05 05:30:36', 50.00),
(13, 37, 1, '2018-01-05 05:31:05', '2018-01-05 05:31:05', 50.00),
(14, 65, 5, '2018-01-05 05:32:20', '2018-01-05 05:32:20', 50.00),
(15, 65, 1, '2018-01-05 05:32:45', '2018-01-05 05:32:45', 50.00),
(16, 20, 7, '2018-01-05 05:34:28', '2018-01-05 05:34:28', 40.00),
(17, 20, 5, '2018-01-05 05:34:54', '2018-01-05 05:34:54', 40.00),
(18, 20, 1, '2018-01-05 05:35:17', '2018-01-05 05:35:17', 40.00),
(19, 19, 7, '2018-01-05 05:35:54', '2018-01-05 05:35:54', 40.00),
(20, 19, 5, '2018-01-05 05:36:05', '2018-01-05 05:36:05', 40.00),
(21, 19, 1, '2018-01-05 05:36:15', '2018-01-05 05:36:15', 40.00),
(22, 18, 7, '2018-01-05 05:36:56', '2018-01-05 05:36:56', 40.00),
(23, 18, 5, '2018-01-05 05:37:06', '2018-01-05 05:37:06', 40.00),
(24, 18, 1, '2018-01-05 05:37:15', '2018-01-05 05:37:15', 40.00),
(25, 23, 5, '2018-01-05 05:38:01', '2018-01-05 05:38:01', 40.00),
(26, 23, 1, '2018-01-05 05:38:10', '2018-01-05 05:38:10', 40.00),
(27, 24, 7, '2018-01-05 05:38:31', '2018-01-05 05:38:31', 40.00),
(28, 24, 5, '2018-01-05 05:39:02', '2018-01-05 05:39:02', 40.00),
(29, 24, 1, '2018-01-05 05:39:09', '2018-01-05 05:39:09', 40.00),
(30, 22, 1, '2018-01-05 05:39:26', '2018-01-05 05:39:26', 40.00),
(31, 22, 7, '2018-01-05 05:39:42', '2018-01-05 05:39:42', 40.00),
(32, 22, 5, '2018-01-05 05:40:05', '2018-01-05 05:40:05', 40.00),
(33, 21, 1, '2018-01-05 05:40:49', '2018-01-05 05:40:49', 40.00),
(34, 21, 7, '2018-01-05 05:41:03', '2018-01-05 05:41:03', 40.00),
(35, 21, 5, '2018-01-05 05:41:33', '2018-01-05 05:41:33', 40.00),
(36, 1, 7, '2018-01-05 05:42:25', '2018-01-05 05:42:25', 30.00),
(37, 1, 5, '2018-01-05 05:43:44', '2018-01-05 05:43:44', 30.00),
(38, 1, 1, '2018-01-05 05:49:11', '2018-01-05 05:49:11', 30.00),
(39, 63, 7, '2018-01-05 05:49:44', '2018-01-05 05:49:44', 30.00),
(40, 63, 5, '2018-01-05 05:50:05', '2018-01-05 05:50:05', 30.00),
(41, 33, 7, '2018-01-05 05:52:56', '2018-01-05 05:52:56', 30.00),
(42, 33, 5, '2018-01-05 05:53:09', '2018-01-05 05:53:09', 30.00),
(43, 33, 1, '2018-01-05 05:53:21', '2018-01-05 05:53:21', 30.00),
(44, 6, 7, '2018-01-05 05:54:28', '2018-01-05 05:54:28', 50.00),
(45, 6, 2, '2018-01-05 05:55:08', '2018-01-05 05:55:08', 40.00),
(46, 6, 1, '2018-01-05 05:56:24', '2018-01-05 05:56:24', 40.00),
(47, 57, 7, '2018-01-05 05:58:12', '2018-01-05 05:58:12', 40.00),
(48, 57, 5, '2018-01-05 05:58:30', '2018-01-05 05:58:30', 40.00),
(49, 36, 7, '2018-01-05 05:59:20', '2018-01-05 05:59:20', 30.00),
(50, 36, 1, '2018-01-05 05:59:57', '2018-01-05 05:59:57', 40.00),
(51, 35, 7, '2018-01-05 06:00:22', '2018-01-05 06:00:22', 30.00),
(52, 35, 1, '2018-01-05 06:00:31', '2018-01-05 06:00:31', 40.00),
(53, 34, 7, '2018-01-05 06:00:50', '2018-01-05 06:00:50', 30.00),
(54, 34, 1, '2018-01-05 06:01:13', '2018-01-05 06:01:13', 40.00),
(55, 38, 7, '2018-01-05 06:01:58', '2018-01-05 06:01:58', 40.00),
(56, 38, 5, '2018-01-05 06:02:07', '2018-01-05 06:02:07', 40.00),
(57, 38, 1, '2018-01-05 06:02:16', '2018-01-05 06:02:16', 40.00),
(58, 56, 7, '2018-01-05 06:03:18', '2018-01-05 06:03:18', 30.00),
(59, 56, 5, '2018-01-05 06:03:45', '2018-01-05 06:03:45', 30.00),
(60, 56, 1, '2018-01-05 06:04:09', '2018-01-05 06:04:09', 30.00),
(61, 39, 7, '2018-01-05 06:04:57', '2018-01-05 06:04:57', 50.00),
(62, 39, 13, '2018-01-05 06:05:26', '2018-01-05 06:05:26', 50.00),
(63, 39, 12, '2018-01-05 06:05:54', '2018-01-05 06:05:54', 50.00),
(64, 42, 7, '2018-01-05 06:09:05', '2018-01-05 06:09:05', 50.00),
(65, 42, 13, '2018-01-05 06:10:13', '2018-01-05 06:10:13', 50.00),
(66, 42, 12, '2018-01-05 06:10:34', '2018-01-05 06:10:34', 50.00),
(67, 60, 7, '2018-01-05 06:21:12', '2018-01-05 06:21:12', 40.00),
(68, 60, 1, '2018-01-05 06:21:51', '2018-01-05 06:21:51', 40.00),
(69, 61, 7, '2018-01-05 06:23:10', '2018-01-05 06:23:10', 40.00),
(70, 61, 1, '2018-01-05 06:23:48', '2018-01-05 06:23:48', 40.00),
(71, 45, 10, '2018-01-05 06:27:58', '2018-01-05 06:27:58', 30.00),
(72, 45, 13, '2018-01-05 06:28:08', '2018-01-05 06:28:08', 30.00),
(73, 46, 10, '2018-01-05 06:28:51', '2018-01-05 06:28:51', 30.00),
(74, 46, 13, '2018-01-05 06:29:20', '2018-01-05 06:29:20', 30.00),
(75, 47, 10, '2018-01-05 06:30:40', '2018-01-05 06:30:40', 50.00),
(76, 47, 13, '2018-01-05 06:30:50', '2018-01-05 06:30:50', 50.00),
(77, 15, 2, '2018-01-05 06:37:01', '2018-01-05 06:37:01', 30.00),
(78, 16, 2, '2018-01-05 06:37:19', '2018-01-05 06:37:19', 30.00),
(79, 17, 2, '2018-01-05 06:37:36', '2018-01-05 06:37:36', 40.00),
(80, 12, 2, '2018-01-05 06:37:49', '2018-01-05 06:37:49', 30.00),
(81, 62, 2, '2018-01-05 06:38:13', '2018-01-05 06:38:13', 30.00),
(82, 58, 2, '2018-01-05 06:38:34', '2018-01-05 06:38:34', 40.00),
(83, 13, 2, '2018-01-05 06:38:54', '2018-01-05 06:38:54', 30.00),
(84, 14, 2, '2018-01-05 06:39:16', '2018-01-05 06:39:16', 30.00),
(85, 7, 2, '2018-01-05 06:41:35', '2018-01-05 06:41:35', 40.00),
(86, 7, 1, '2018-01-05 06:41:59', '2018-01-05 06:41:59', 40.00),
(87, 9, 1, '2018-01-05 06:42:55', '2018-01-05 06:42:55', 40.00),
(88, 8, 2, '2018-01-05 06:44:16', '2018-01-05 06:44:16', 40.00),
(90, 10, 12, '2018-01-05 06:45:13', '2018-01-05 06:45:13', 40.00),
(92, 10, 2, '2018-01-05 06:47:41', '2018-01-05 06:47:41', 30.00),
(93, 2, 1, '2018-01-05 06:49:25', '2018-01-05 06:49:25', 40.00),
(94, 67, 12, '2018-01-05 06:50:19', '2018-01-05 06:50:19', 50.00),
(95, 4, 10, '2018-01-05 06:50:58', '2018-01-05 06:50:58', 40.00);

-- --------------------------------------------------------

--
-- Estrutura para tabela `produtosservicos`
--

CREATE TABLE IF NOT EXISTS `produtosservicos` (
  `prosrv_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `prosrv_codbarras` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prosrv_nome` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `prosrv_descricao` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prosrv_ucom` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prosrv_custo` double(8,2) NOT NULL,
  `prosrv_preco` double(8,2) NOT NULL,
  `prosrv_pcomissao` double(8,2) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `emp_id` int(10) UNSIGNED NOT NULL,
  `usu_id` int(11) NOT NULL,
  `prof_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`prosrv_id`),
  UNIQUE KEY `produtosservicos_prosrv_codbarras_unique` (`prosrv_codbarras`)
) ENGINE=InnoDB AUTO_INCREMENT=75 AVG_ROW_LENGTH=8192 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `produtosservicos`
--

INSERT INTO `produtosservicos` (`prosrv_id`, `prosrv_codbarras`, `prosrv_nome`, `prosrv_descricao`, `prosrv_ucom`, `prosrv_custo`, `prosrv_preco`, `prosrv_pcomissao`, `deleted_at`, `created_at`, `updated_at`, `emp_id`, `usu_id`, `prof_id`) VALUES
(1, '1', 'HIDRATACAO DE PELE', 'HIDRATACAO DE PELE', 'UN', 120.00, 120.00, 30.00, NULL, NULL, NULL, 2, 0, 0),
(2, '2', 'LIMPEZA DE PELE', 'LIMPEZA DE PELE', 'UN', 130.00, 130.00, 30.00, NULL, NULL, NULL, 2, 0, 0),
(3, '3', 'DRENAGEM LINFATICA', 'DRENAGEM LINFATICA', 'UN', 70.00, 70.00, 40.00, NULL, NULL, NULL, 2, 0, 0),
(4, '4', 'MASSAGEM RELAXANTE', 'MASSAGEM RELAXANTE', 'UN', 70.00, 70.00, 40.00, NULL, NULL, NULL, 2, 0, 0),
(5, '5', 'DRENAGEM FACIAL', 'DRENAGEM FACIAL', 'UN', 70.00, 70.00, 30.00, NULL, NULL, NULL, 2, 0, 0),
(6, '6', 'MAQUIAGEM', 'MAQUIAGEM', 'UN', 100.00, 100.00, 40.00, NULL, NULL, NULL, 2, 0, 0),
(7, '7', 'SOBRANCELHA FAZER', 'SOBRANCELHA FAZER', 'UN', 20.00, 20.00, 0.00, NULL, NULL, '2018-01-05 06:41:08', 2, 0, 0),
(8, '8', 'SOBRANCELIA PIGMENTADA', 'SOBRANCELIA PIGMENTADA', 'UN', 20.00, 20.00, 40.00, NULL, NULL, NULL, 2, 0, 0),
(9, '9', 'SOBRANCELIA MICROPIGMENTADA', 'SOBRANCELIA MICROPIGMENTADA', 'UN', 600.00, 600.00, 40.00, NULL, NULL, NULL, 2, 0, 0),
(10, '10', 'BANHO DE LUA', 'BANHO DE LUA', 'UN', 120.00, 120.00, 30.00, NULL, NULL, NULL, 2, 0, 0),
(11, '11', 'TRATAMENTO CORPORAL COM APARELHO', 'TRATAMENTO CORPORAL COM APARELHO', 'UN', 75.00, 75.00, 40.00, NULL, NULL, NULL, 2, 0, 0),
(12, '12', 'DEPILACAO CANTO', 'DEPILACAO CANTO', 'UN', 35.00, 35.00, 30.00, NULL, NULL, NULL, 2, 0, 0),
(13, '13', 'DEPILACAO MEIA PERNA', 'DEPILACAO MEIA PERNA', 'UN', 15.00, 15.00, 0.00, NULL, NULL, '2018-01-05 06:35:54', 2, 0, 0),
(14, '14', 'DEPILACAO PERNA TODA', 'DEPILACAO PERNA TODA', 'UN', 30.00, 30.00, 30.00, NULL, NULL, NULL, 2, 0, 0),
(15, '15', 'DEPILACAO AXILA', 'DEPILACAO AXILA', 'UN', 15.00, 15.00, 30.00, NULL, NULL, NULL, 2, 0, 0),
(16, '16', 'DEPILACAO BUCO CERA', 'DEPILACAO BUCO CERA', 'UN', 10.00, 10.00, 30.00, NULL, NULL, NULL, 2, 0, 0),
(17, '17', 'DEPILACAO BUCO COM LINHA', 'DEPILACAO BUCO COM LINHA', 'UN', 15.00, 15.00, 40.00, NULL, NULL, NULL, 2, 0, 0),
(18, '18', 'ESCOVA GRADAT. PEQUENA', 'ESCOVA GRADAT. PEQUENA', 'UN', 200.00, 200.00, 40.00, NULL, NULL, NULL, 2, 0, 0),
(19, '19', 'ESCOVA GRADAT. MEDIA', 'ESCOVA GRADAT. MEDIA', 'UN', 250.00, 250.00, 40.00, NULL, NULL, NULL, 2, 0, 0),
(20, '20', 'ESCOVA GRADAT.  GRANDE', 'ESCOVA GRADAT.  GRANDE', 'UN', 280.00, 280.00, 40.00, NULL, NULL, NULL, 2, 0, 0),
(21, '21', 'ESCOVA PEQUENA', 'ESCOVA PEQUENA', 'UN', 25.00, 25.00, 40.00, NULL, NULL, NULL, 2, 0, 0),
(22, '22', 'ESCOVA MEDIA', 'ESCOVA MEDIA', 'UN', 30.00, 30.00, 40.00, NULL, NULL, NULL, 2, 0, 0),
(23, '23', 'ESCOVA GRANDE', 'ESCOVA GRANDE', 'UN', 35.00, 35.00, 40.00, NULL, NULL, NULL, 2, 0, 0),
(24, '24', 'ESCOVA GRANDE GRANDE', 'ESCOVA GRANDE GRANDE', 'UN', 40.00, 40.00, 40.00, NULL, NULL, NULL, 2, 0, 0),
(25, '25', 'TRAT. CABELO JOICO', 'TRAT. CABELO JOICO', 'UN', 120.00, 120.00, 30.00, NULL, NULL, NULL, 2, 0, 0),
(26, '26', 'TRAT. LANZA', 'TRAT. LANZA', 'UN', 120.00, 120.00, 30.00, NULL, NULL, NULL, 2, 0, 0),
(27, '27', 'TRAT. SENSCIENCE', 'TRAT. SENSCIENCE', 'UN', 150.00, 150.00, 30.00, NULL, NULL, NULL, 2, 0, 0),
(28, '28', 'TRAT. BONACURE', 'TRAT. BONACURE', 'UN', 120.00, 120.00, 30.00, NULL, NULL, NULL, 2, 0, 0),
(29, '29', 'TRAT. LOREAL POS QUIMICA', 'TRAT. LOREAL POS QUIMICA', 'UN', 120.00, 120.00, 30.00, NULL, NULL, NULL, 2, 0, 0),
(30, '30', 'TRAT. LOREAL', 'TRAT. LOREAL', 'UN', 70.00, 70.00, 30.00, NULL, NULL, NULL, 2, 0, 0),
(31, '31', 'TRAT. VITA DERM', 'TRAT. VITA DERM', 'UN', 70.00, 70.00, 30.00, NULL, NULL, NULL, 2, 0, 0),
(32, '32', 'COLORACAO PEQUENA', 'COLORACAO PEQUENA', 'UN', 90.00, 90.00, 30.00, NULL, NULL, NULL, 2, 0, 0),
(33, '33', 'COLORACAO MEDIA', 'COLORACAO MEDIA', 'UN', 120.00, 120.00, 30.00, NULL, NULL, NULL, 2, 0, 0),
(34, '34', 'MECHAS PEQUENA', 'MECHAS PEQUENA', 'UN', 220.00, 220.00, 30.00, NULL, NULL, NULL, 2, 0, 0),
(35, '35', 'MECHAS MEDIA', 'MECHAS MEDIA', 'UN', 280.00, 280.00, 30.00, NULL, NULL, NULL, 2, 0, 0),
(36, '36', 'MECHAS GRANDES', 'MECHAS GRANDES', 'UN', 330.00, 330.00, 30.00, NULL, NULL, NULL, 2, 0, 0),
(37, '37', 'CORTE CABELO', 'CORTE CABELO', 'UN', 50.00, 50.00, 50.00, NULL, NULL, NULL, 2, 0, 0),
(38, '38', 'PENTEADO', 'PENTEADO', 'UN', 70.00, 70.00, 40.00, NULL, NULL, NULL, 2, 0, 0),
(39, '39', 'MAO', 'MAO', 'UN', 22.00, 22.00, 0.00, NULL, NULL, '2018-01-05 21:20:34', 2, 0, 0),
(42, '42', 'PE', 'PE', 'UN', 23.00, 23.00, 50.00, NULL, NULL, NULL, 2, 0, 0),
(45, '45', 'SPAR DOS PES', 'SPAR DOS PES', 'UN', 25.00, 25.00, 30.00, NULL, NULL, NULL, 2, 0, 0),
(46, '46', 'SPAR MAOS', 'SPAR MAOS', 'UN', 25.00, 25.00, 30.00, NULL, NULL, NULL, 2, 0, 0),
(47, '47', 'SMALTACAO', 'SMALTACAO', 'UN', 12.00, 12.00, 50.00, NULL, NULL, NULL, 2, 0, 0),
(49, '49', 'UNHA EM GEL', 'UNHA EM GEL', 'UN', 150.00, 150.00, 50.00, NULL, NULL, NULL, 2, 0, 0),
(50, '50', 'UNHA EM GEL', 'UNHA EM GEL', 'UN', 150.00, 150.00, 80.00, NULL, NULL, NULL, 2, 0, 0),
(51, '51', 'UNHAS DE PORCELANA', 'UNHAS DE PORCELANA', 'UN', 150.00, 150.00, 50.00, NULL, NULL, NULL, 2, 0, 0),
(52, '52', 'UNHA PORCELANAS', 'UNHA PORCELANAS', 'UN', 150.00, 150.00, 80.00, NULL, NULL, NULL, 2, 0, 0),
(53, '53', 'MANUTENCAO UNHAS EM GEL E PORCELANA', 'MANUTENCAO UNHAS EM GEL E PORCELANA', 'UN', 80.00, 80.00, 50.00, NULL, NULL, NULL, 2, 0, 0),
(54, '54', 'MANUTENCAO UNHA PORCELANA E GEL', 'MANUTENCAO UNHA PORCELANA E GEL', 'UN', 80.00, 80.00, 80.00, NULL, NULL, NULL, 2, 0, 0),
(55, '55', 'LANCHES', 'LANCHES', 'UN', 6.00, 6.00, 0.00, NULL, NULL, NULL, 2, 0, 0),
(56, '56', 'LAVAR CABELO ESPECIAL', 'LAVAR CABELO ESPECIAL', 'UN', 25.00, 25.00, 30.00, NULL, NULL, NULL, 2, 0, 0),
(57, '57', 'APLICACAO DE PRODUTO', 'APLICACAO DE PRODUTO', 'UN', 30.00, 30.00, 40.00, NULL, NULL, NULL, 2, 0, 0),
(58, '58', 'DEPILACAO LINHA AXILA', 'DEPILACAO LINHA AXILA', 'UN', 15.00, 15.00, 40.00, NULL, NULL, NULL, 2, 0, 0),
(60, '60', 'CILIOS POSTICOS INTEIRO', 'CILIOS POSTICOS INTEIRO', 'UN', 40.00, 40.00, 40.00, NULL, NULL, NULL, 2, 0, 0),
(61, '61', 'CILIOS POSTICOS DE TUFO', 'CILIOS POSTICOS DE TUFO', 'UN', 80.00, 80.00, 40.00, NULL, NULL, NULL, 2, 0, 0),
(62, '62', 'DEPILACAO COMPLETE', 'DEPILACAO COMPLETE', 'UN', 95.00, 95.00, 30.00, NULL, NULL, NULL, 2, 0, 0),
(63, '63', 'RISAGEM', 'RISAGEM', 'UN', 70.00, 70.00, 30.00, NULL, NULL, NULL, 2, 0, 0),
(65, '65', 'CORTE BORDADO', 'CORTE BORDADO', 'UN', 100.00, 100.00, 50.00, NULL, NULL, NULL, 2, 0, 0),
(66, '66', 'GOMAGEM', 'GOMAGEM', 'UN', 60.00, 60.00, 40.00, NULL, NULL, NULL, 2, 0, 0),
(67, '67', 'MASSAGEM PE', 'MASSAGEM PE', 'UN', 20.00, 20.00, 50.00, NULL, NULL, NULL, 2, 0, 0),
(68, '68', 'MANUTENCAO UNHA GEL', 'MANUTENCAO UNHA GEL', 'UN', 80.00, 80.00, 65.00, NULL, NULL, NULL, 2, 0, 0),
(69, '69', 'UNHA DE VIBRA DE VIDRO', 'UNHA DE VIBRA DE VIDRO', 'UN', 200.00, 200.00, 50.00, NULL, NULL, NULL, 2, 0, 0),
(70, NULL, 'Corte de cabelo', 'Corte de cabelo', 'UN', 10.00, 10.00, 10.00, NULL, '2017-10-25 16:36:44', '2017-10-25 16:36:44', 1, 1, 0),
(72, '0000009', 'Corte feminino', 'Corte feminino', 'UN', 0.00, 50.00, NULL, NULL, '2017-11-27 07:35:07', '2017-11-27 07:35:07', 1, 1, NULL),
(73, '00000', 'Teste', 'teste', 'UN', 0.00, 0.00, NULL, NULL, '2017-12-02 15:16:50', '2017-12-02 15:16:50', 2, 6, NULL),
(74, NULL, 'MANUTENOCAO DE FIBRA DE VIDRO', NULL, NULL, 50.00, 100.00, 0.00, NULL, '2018-02-28 17:23:11', '2018-02-28 17:23:11', 2, 4, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `profissionais`
--

CREATE TABLE IF NOT EXISTS `profissionais` (
  `prof_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `usu_id` int(10) UNSIGNED DEFAULT NULL,
  `prof_cpf` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prof_rg` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prof_nome` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `prof_nascimento` date DEFAULT NULL,
  `prof_celular` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prof_email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prof_endereco` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prof_bairro` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prof_cep` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prof_cidade` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prof_estado` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `emp_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`prof_id`),
  KEY `profissionais_usu_id_foreign` (`usu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 AVG_ROW_LENGTH=1489 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `profissionais`
--

INSERT INTO `profissionais` (`prof_id`, `usu_id`, `prof_cpf`, `prof_rg`, `prof_nome`, `prof_nascimento`, `prof_celular`, `prof_email`, `prof_endereco`, `prof_bairro`, `prof_cep`, `prof_cidade`, `prof_estado`, `deleted_at`, `created_at`, `updated_at`, `emp_id`) VALUES
(1, 2, '90067754449', '', 'ANA MARIA TORRES DE LIMA', '1974-07-20', '87-9884-14333', '', '', '', '', 'PETROLINA', 'PE', NULL, NULL, NULL, 2),
(2, 2, '05557900454', '', 'ROSA MARIA TORRES PEREIRA', '1983-06-20', '87-9883-66343', '', 'RUA ALGODAO DA PRAIA, 360', 'NOVA VIDA I JOAO DE DEUS', '56302000', 'PETROLINA', 'PE', NULL, NULL, NULL, 2),
(3, 2, '', '', 'LUCICLEIDE CARLOS DE SOUZA PEREIRA', '1978-09-10', '87-9883-66944', '', 'RUA 11, 220', 'VALE DOURADO', '', 'PETROLINA', 'PE', NULL, NULL, NULL, 2),
(5, 2, '02586425480', NULL, 'MARIA ELIZABETE FERREIRA DA SILVA', '1974-11-10', '87-9887-55643', NULL, 'RUA CATARINA QUEIROS, BLOCO 3, 001 A', NULL, NULL, 'PETROLINA', 'PE', NULL, NULL, '2018-01-10 17:27:08', 2),
(7, 2, '01643397524', '6033821', 'MARIA CLAUDIA PEIXOTO', '1983-07-28', '87-9882-12365', '', 'RUA AMAZONAS Nº 200', 'JOSE IMARIA', '', 'PETROLINA', 'PE', NULL, NULL, NULL, 2),
(8, 2, '', '', 'SOLANGE DE JESUS BRITO', NULL, '87-9881-68280', '', '', '', '', 'PETROLINA', 'PE', NULL, NULL, NULL, 2),
(9, 2, '', '', 'NARA AMORIN', '1978-09-13', '87-9880-38085', '', '', '', '', 'PETROLINA', 'PE', NULL, NULL, NULL, 2),
(10, 2, '', '', 'ALEXANDRA', NULL, '', '', '', '', '', 'PETROLINA', 'PE', NULL, NULL, NULL, 2),
(12, 2, '07406554427', '7180584', 'MARIA APARECIDA DE  SOUZA SILVA', '1988-05-24', '87-8805-2494', NULL, 'RUA THOMAS ANTONIO GONZAGA Nº 200', 'VILA DEBORA', NULL, 'PETROLINA', 'PE', NULL, NULL, '2018-02-28 17:08:10', 2),
(13, 2, '069 891 424 48', '8249071', 'DEBORA JAFIA DE OLIVEIRA SILVA', '1988-07-03', '87-8811-1858', NULL, 'RUA CONTINHA MANGABEIRA Nº 16', 'COABH', NULL, 'PETROLINA', 'PE', NULL, NULL, '2018-01-10 17:26:38', 2),
(14, NULL, '', '', 'JACK', NULL, '', '', '', '', '', 'PETROLINA', 'PE', NULL, NULL, NULL, 2),
(16, 1, NULL, NULL, 'Cristiano', '2017-10-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-25 15:28:53', '2017-10-25 15:28:53', 1),
(17, 6, '222222', '3333333333', 'Profissional_Teste', '1987-09-10', '4343434343', 'profissional@gmail.com', 'Rua Conde', 'Bethania', '4343434', 'Porto Alegre', 'RS', NULL, '2018-02-03 02:43:37', '2018-02-03 02:43:37', 2);

-- --------------------------------------------------------

--
-- Estrutura para tabela `retiradas`
--

CREATE TABLE IF NOT EXISTS `retiradas` (
  `re_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `movcx_id` int(10) UNSIGNED NOT NULL,
  `re_descricao` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `usu_id` int(10) UNSIGNED NOT NULL,
  `emp_id` int(11) NOT NULL,
  `re_valor` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`re_id`),
  KEY `retiradas_movcx_id_foreign` (`movcx_id`),
  KEY `retiradas_usu_id_foreign` (`usu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `suplementos`
--

CREATE TABLE IF NOT EXISTS `suplementos` (
  `supl_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `movcx_id` int(10) UNSIGNED NOT NULL,
  `supl_descricao` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `usu_id` int(10) UNSIGNED NOT NULL,
  `emp_id` int(11) NOT NULL,
  `supl_valor` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`supl_id`),
  KEY `suplementos_movcx_id_foreign` (`movcx_id`),
  KEY `suplementos_usu_id_foreign` (`usu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `tipospagamentos`
--

CREATE TABLE IF NOT EXISTS `tipospagamentos` (
  `tpg_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tpg_nome` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `tpg_mobile` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`tpg_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 AVG_ROW_LENGTH=4096 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `tipospagamentos`
--

INSERT INTO `tipospagamentos` (`tpg_id`, `tpg_nome`, `tpg_mobile`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Dinheiro', 1, 1, NULL, NULL),
(2, 'Duplicata', 1, 1, NULL, NULL),
(3, 'Cheque', 1, 1, NULL, NULL),
(4, 'Cartão', 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tipo` enum('super','admin','usuario') COLLATE utf8_unicode_ci NOT NULL,
  `emp_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=10 AVG_ROW_LENGTH=8192 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `tipo`, `emp_id`) VALUES
(1, 'Paulo Henrique Andrade', 'phdorocha@gmail.com', '$2y$10$zuWghLNomIOoWw0.SQqlVueV28oOIho0m1sTC1/HXj65mArsRdFJa', 'AIAehJu7wn5cYB25xJfwxm1qiM6pGlUuCdPmpkVL1R2IGqlYdXDb5yBgx8LJ', '2017-10-13 04:57:35', '2017-10-13 04:57:35', 'super', 1),
(2, 'Usuário Teste', 'demo@demo', '$2y$10$SPMyBHyrl3yJUnW2ANBV5OsR4a2UyD0nwAUOtR4j.LTuEKGLJuaBO', '9JrZtcfmnfXhTKaQ4xuOAqeEebMdFyhnLbECS5BIxukQkCZocPU59ka5A5Ft', '2017-10-21 05:49:40', '2017-10-21 05:49:40', 'admin', 2),
(4, 'Edvaldo Emiliano de Lima', '', '$2y$10$E3oIKp6aT9CIj816gl6vUOJkuekgMuAQdh7wiUmrL37nFRTg1SIqq123', 'ulSThrkgAUzKtXeK97eB6pAt8YMm8dz76i03SsGe10mlIHCyKyULbPkFfUpn', '2017-10-21 05:50:52', '2017-10-21 05:50:52', 'admin', 2),
(5, 'Debora Ruth', 'deborarute_@hotmail.com', '$2y$10$89cLckozp7s98S7D0aHpY.uDM8/0WXicDtC9T1jvQAoBdgvS6AJmi', 'hY3mHsRWtmzP2EG1BQtyUIoDA8Iu5RT4zXIcv2McuLcZ8jhIRITY5p7CMfoC', '2017-10-21 21:49:12', '2017-10-21 21:49:12', 'admin', 3),
(6, 'Cristiano', 'sdxadev@gmail.com', '$2y$10$abkC10yGKtta5rq3U2gQvOs4VvTXrzH5aOdfOFc9Ug1HBDOkhfp5C', '6VFxef5ZpWFCrtmWtTLKjLAufMYnTNhCwuzNWoLbMGS2wPPNFxHxCLrAVCcT', '2017-10-26 03:21:53', '2017-10-26 03:21:53', 'super', 2),
(7, 'Cecília', 'teste@sid.com.br', '$2y$10$2x69fbQTHLWem2WJS1I2uOLXZg3ks1el0/JdM9qtRd3V3bRg7hJq.123', 'hZlmF5IzE9jNy8xYIPBIhUkAQC07P76nevMyAVCZz1WweFYRwtpZlObFjEcG', '2017-11-14 22:52:08', '2017-11-14 22:52:08', 'super', 1),
(8, 'Josane Sampaio', 'jo.sampaio12@hotmail.com', '$2y$10$zuWghLNomIOoWw0.SQqlVueV28oOIho0m1sTC1/HXj65mArsRdFJa', 'xHxyr2JgjlISVfWaJS2cjWZl1STAisZ2FFXXc9t0rvoWrWFkUVzCEMkjkx6A', '2017-10-13 04:57:35', '2017-10-13 04:57:35', 'admin', 4),
(9, 'Walter Gomes', 'waltergomes.keyboards@gmail.com ', '$2y$10$abkC10yGKtta5rq3U2gQvOs4VvTXrzH5aOdfOFc9Ug1HBDOkhfp5C', 'KlwkogUCObnZq2GILg9BIGeaZHfnuhjatHBwToyyt8RL3RscgOCw3P5gkJbr', '2017-10-26 03:21:53', '2017-10-26 03:21:53', 'super', 1);

--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `agendas`
--
ALTER TABLE `agendas`
  ADD CONSTRAINT `agendas_cli_id_foreign` FOREIGN KEY (`cli_id`) REFERENCES `clientes` (`cli_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `agendas_emp_id_foreign` FOREIGN KEY (`emp_id`) REFERENCES `empresas` (`emp_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `agendas_prof_id_foreign` FOREIGN KEY (`prof_id`) REFERENCES `profissionais` (`prof_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `agendas_prosrv_id_foreign` FOREIGN KEY (`prosrv_id`) REFERENCES `produtosservicos` (`prosrv_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `agendas_usu_id_foreign` FOREIGN KEY (`usu_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para tabelas `caixas`
--
ALTER TABLE `caixas`
  ADD CONSTRAINT `caixas_usu_id_foreign` FOREIGN KEY (`usu_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Restrições para tabelas `comandas`
--
ALTER TABLE `comandas`
  ADD CONSTRAINT `comandas_cli_id_foreign` FOREIGN KEY (`cli_id`) REFERENCES `clientes` (`cli_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `comandas_usu_id_foreign` FOREIGN KEY (`usu_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para tabelas `comissoes`
--
ALTER TABLE `comissoes`
  ADD CONSTRAINT `comissoes_cli_id_foreign` FOREIGN KEY (`cli_id`) REFERENCES `clientes` (`cli_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `comissoes_prof_id_foreign` FOREIGN KEY (`prof_id`) REFERENCES `profissionais` (`prof_id`),
  ADD CONSTRAINT `comissoes_prosrv_id_foreign` FOREIGN KEY (`prosrv_id`) REFERENCES `produtosservicos` (`prosrv_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para tabelas `itens`
--
ALTER TABLE `itens`
  ADD CONSTRAINT `itens_cmd_id_foreign` FOREIGN KEY (`cmd_id`) REFERENCES `comandas` (`cmd_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `itens_prof_id_foreign` FOREIGN KEY (`prof_id`) REFERENCES `profissionais` (`prof_id`),
  ADD CONSTRAINT `itens_prosrv_id_foreign` FOREIGN KEY (`prosrv_id`) REFERENCES `produtosservicos` (`prosrv_id`),
  ADD CONSTRAINT `itens_usu_id_foreign` FOREIGN KEY (`usu_id`) REFERENCES `users` (`id`);

--
-- Restrições para tabelas `movcaixas`
--
ALTER TABLE `movcaixas`
  ADD CONSTRAINT `movcaixas_cx_id_foreign` FOREIGN KEY (`cx_id`) REFERENCES `caixas` (`cx_id`),
  ADD CONSTRAINT `movcaixas_usu_id_foreign` FOREIGN KEY (`usu_id`) REFERENCES `users` (`id`);

--
-- Restrições para tabelas `pagamentos`
--
ALTER TABLE `pagamentos`
  ADD CONSTRAINT `pagamentos_cmd_id_foreign` FOREIGN KEY (`cmd_id`) REFERENCES `comandas` (`cmd_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `pagamentos_tpg_id_foreign` FOREIGN KEY (`tpg_id`) REFERENCES `tipospagamentos` (`tpg_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Restrições para tabelas `produtosprofissionais`
--
ALTER TABLE `produtosprofissionais`
  ADD CONSTRAINT `produtosprofissionais_prof_id_foreign` FOREIGN KEY (`prof_id`) REFERENCES `profissionais` (`prof_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `produtosprofissionais_prosrv_id_foreign` FOREIGN KEY (`prosrv_id`) REFERENCES `produtosservicos` (`prosrv_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para tabelas `profissionais`
--
ALTER TABLE `profissionais`
  ADD CONSTRAINT `profissionais_usu_id_foreign` FOREIGN KEY (`usu_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para tabelas `retiradas`
--
ALTER TABLE `retiradas`
  ADD CONSTRAINT `retiradas_movcx_id_foreign` FOREIGN KEY (`movcx_id`) REFERENCES `movcaixas` (`movcx_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `retiradas_usu_id_foreign` FOREIGN KEY (`usu_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Restrições para tabelas `suplementos`
--
ALTER TABLE `suplementos`
  ADD CONSTRAINT `suplementos_movcx_id_foreign` FOREIGN KEY (`movcx_id`) REFERENCES `movcaixas` (`movcx_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `suplementos_usu_id_foreign` FOREIGN KEY (`usu_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
