<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Linhas de autenticação
    |--------------------------------------------------------------------------
    |
    | As seguintes linhas de idioma são usadas durante a autenticação para vários
    | mensagens que precisamos exibir para o usuário. Você pode modificar
    | estas linhas de idioma de acordo com os requisitos da sua aplicação.
    |
    */

    'failed' => 'Essas credenciais não correspondem aos nossos registros.',
    'throttle' => 'Muitas tentativas de login. Tente novamente em:seconds seconds.',

];
