
<script>
$(document).ready(function() {
    $('#calendar').fullCalendar({
            locale: 'pt-br',
            // put your options and callbacks here
            events : [
                @foreach($agendas as $agenda)
                {
                    title : '{{ $agenda->cliente->cli_nome }}',
                    start : '{{ $agenda->agd_data }}',
                    data:   '{{ $agenda->agd_data }}',
                    url : '{{ route('agenda.show', $agenda->agd_id) }}',
                    profissional: '{{ $agenda->profissional->prof_nome }}',
                },
                @endforeach
            ],

            eventRender: function(event, element) {
                    element.find('span.fc-title').append("<br/>" + event.profissional);
                    element.find('span.fc-title').append("<br/>" + event.data);
                },
        })
})
console.log(123);
</script>
