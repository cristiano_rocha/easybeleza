
<script>
$(document).ready(function() {
  $(function () {
      $("#cli_nome").autocomplete({
    delay: 100,
    source: function(request, response) {
      searchCLIURL = "/search";
      $.ajax({
          method: 'GET',
          dataType: 'json',
          url: searchCLIURL,
          data: {
              term: request.term
          }
        })
        .done(function(data) {
          response(data);
        });
    },
    select: function(event, ui) {
      console.log(ui.item.id)
      $("#cli_id").val(ui.item.id);
    },
    messages: {
      noResults: "Nenhum cliente foi encontrado",
    }
  });
  });

  $(function () {
      $("#prof_nome").autocomplete({
    delay: 100,
    source: function(request, response) {
      searchCLIURL = "/searchProf";
      $.ajax({
          method: 'GET',
          dataType: 'json',
          url: searchCLIURL,
          data: {
              term: request.term
          }
        })
        .done(function(data) {
          response(data);
        });
    },
    select: function(event, ui) {
      console.log(ui.item.id)
      $("#prof_id").val(ui.item.id);
    },
    messages: {
      noResults: "Nenhum profissional foi encontrado",
    }
  });
  });


});

</script>


