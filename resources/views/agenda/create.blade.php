@extends('agenda.layouts.app')
@section('content')
<style>
    @font-face {
        font-family: fontello;
        src: url('{{ public_path('fonts/fontello.woff') }}')
    }
</style>
    <div class="container">
        <div class="card">
        <div class="card-body">
            <nav class="navbar navbar-inverse" style="width:30%;">
                <div class="navbar-header">
                    <i class="fa fa-calendar"></i> <a class="navbar-brand" href="{{ URL::to('./agenda') }}">Agenda</a>
                    <i class="fa fa-list-alt"></i> <a class="navbar-brand" href="{{ URL::to('lista') }}">Lista</a>
                    <i class="fa fa-newspaper-o"></i> <a class="navbar-brand" href="{{ URL::to('agenda/create') }}">Novo</a>
                </div>
            </nav>

            <div class="card-title">Agendamento</div>

            <!-- se houver erros, serão mostrados -->
            {{ Html::ul($errors->all()) }}
            {{ Form::open(['route' => 'agendas.store', 'method' => 'post']) }}    
                <div class="row">
                   <div class="col-sm-4">
                    <div class="form-group">
                        {{Form::label('cli_nome', 'Cliente')}}
                        {{Form::text('cli_nome',null, array('class' => 'form-control', 'id' => 'cli_nome'))}}
                    </div>

                    <div class="form-group">
                        {{Form::label('prof_nome', 'Profissional')}}
                        {{Form::text('prof_nome',null, array('class' => 'form-control', 'id' => 'prof_nome'))}}
                    </div>

                    <div class="form-group">
                        {{Form::label('prosrv_id', 'Produto/Serviço')}}
                        <select name="prosrv_id" id="prosrv_id" class="form-control">
                        @if($prosrvs->count() > 0)
                            @foreach($prosrvs as $prosrv)
                                <option selected="selected">Selecione...</option>
                                <option value="{{$prosrv->prosrv_id}}">{{ $prosrv->prosrv_nome }}</option>}}
                            @endforeach
                        @else
                            Nenhum serviço/produto encontrado
                        @endif
                        </select>
                    </div>

                    <div class="form-group">
                        {{Form::label('agd_data', 'Data')}}
                        {{Form::text('agd_data',null, array('class' => 'form-control date', 'id' => 'agd_data', 'type' => 'date'))}}
                    </div>

                    <div class="form-group">
                        {{Form::label('agd_hora', 'Hora')}}
                        {{Form::text('agd_hora',null, array('class' => 'form-control timepicker', 'id' => 'agd_hora'))}}
                    </div>


                   </div>

                </div>
            <input type="hidden" id="cli_id" name="cli_id"  value="">
            <input type="hidden" id="prof_id" name="prof_id"  value="">
            <input type="hidden" id="usu_id" name="usu_id" value="{{Auth::user()->id}}">
            <input type="hidden" id="emp_id" name="emp_id" value="{{Auth::user()->emp_id}}">
            {{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}
            {{ Form::close() }}
        </div>
        </div>
    </div>
    @include('agenda.scripts.libraries')
    @include('agenda.scripts.autocomplete')
    <script>
        $('#agd_data').datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            ChangeYear: true
        });

        var options = {
            twentyFour: true,
            upArrow: 'wickedpicker__controls__control-up',
            downArrow: 'wickedpicker__controls__control-down',
            close: 'wickedpicker__close',
            title: 'Selecione o horário',
            now: '00:00'
        };
        $('#agd_hora').wickedpicker(options);
    </script>


@endsection


