@extends('agenda.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-calendar"></i> <a class="navbar-brand" href="{{ URL::to('./agenda') }}">Agenda</a>
                        <i class="fa fa-list-alt"></i> <a class="navbar-brand" href="{{ URL::to('lista') }}">Lista</a>
                        <i class="fa fa-newspaper-o"></i> <a class="navbar-brand" href="{{ URL::to('agenda/create') }}">Novo</a>
                    </div>                    
                    <div class="panel-body">
                        <div id="calendar"></div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    
   @include('agenda.scripts.libraries')
   @include('agenda.scripts.fullcalendar')


@endsection