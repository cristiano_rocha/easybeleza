<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Programa para salão de beleza, sistema de gestão e marketing, seu salão de beleza na Web. Agendamento online e muito mais.">
	<meta name="keywords" content= "programa para salao, programa para salão, controle de estoque, salão de beleza online, como abrir um salão, programa para salao, programa para salão, salao de beleza online, sistema de salao de beleza, sistema para salão, software para salao, software para salão, sistema salao, sistema de salao, sistema salão, salao de beleza na web, software livre, sistema de gestão" />
	<meta name="robots" content="index,follow">
	<meta name="author" content="RochaNet Informática - SiD Soluções">
	<meta http-equiv="content-language" content="pt-BR">
	<meta name="googlebot" content="index,follow">
	<meta http-equiv="cache-control" content="max-age">

	<meta property="og:title" content="Programa para Salão de Beleza Completo - EasyBeleza">
    <meta property="og:description" content="Programa para salão de beleza com agendamento on-line, gestão de agenda, cadastro de clientes, controle financeiro, controle de estoque">
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="EasyBeleza">
    <meta property="og:url" content="https://app.easybeleza.net.br">
    <meta property="og:image" content="https://app.easybeleza.net.br/img/logo-easybeleza.png">


	<link href="https://app.easybeleza.net.br/img/logo-x-easybeleza.png" rel="icon" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }} - {{ config('app.subtitle') }}</title>
    <!-- Styles -->
    <link rel="stylesheet" href="{{ URL::to('css/app.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ URL::to('css/draggable.css')}}" type="text/css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">

    <!-- Scripts JS -->
    <script type="text/javascript" async="async" defer="defer" data-cfasync="false" src="{{ URL::to('js/draggable.js')}}"></script>

    <!-- Configurações Push Notification -->
    <link rel="manifest" href="/manifest.json">
    
    <!-- Bootstrap-Toggle -->
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css' />
    <link rel="stylesheet" href="{{ URL::to('css/wickedpicker.min.css')}}">
    
    

</head>
<body>    
<div class="pagina">
    <!-- Authentication Links -->
    @guest
</head>
<body>
    <nav class="navbar bg-pink" id="navbar">
        <a class="navbar-brand text-white" href="{{ config('app.URL') }}/home">{{ config('app.name') }}</a>
        <!-- Right Side Of Navbar -->
        <ul class="nav navbar-nav navbar-right">
            <!-- Authentication Links -->
            @guest
                <li><a href="{{ route('login') }}">Login</a></li>
                <!--<li><a href="{{ route('register') }}">Register</a></li>-->
            @else
                    <a href="#" class="dropdown-toggle text-white" data-toggle="dropdown" role="button" aria-expanded="false">
                        {{ Auth::user()->tipo }}:{{ Auth::user()->name }} - {{ Auth::user()->emp_id }}
                    </a>
                <li class="dropdown">                                
                    <a href="{{ route('logout') }}" class="text-white"
                        onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                        Logout
                    </a>
                </li>
            @endguest
        </ul>
    </nav>

    <div class="sidebar">
        <div class="panel sidebar-categories">
            <div class="inner">
                <span class="h1" style="color:#ee5190">Dashboard</span>
                <ul>
                    <li class="current-sidebar-link"><a href="{{ config('app.URL') }}/clientes"><i class="fa fa-calendar"></i>Agenda</a></li>
                    <li><a href="#"><i class="fa fa-user"></i>Clientes</a></li>
                    <li><a href="#"><i class="fa fa-graduation-cap"></i>Profissionais</a></li>
                    <li><a href="#"><i class="fa fa-trophy"></i>Prod/Serviços</a></li>
                    <li><a href="#"><i class="fa fa-shopping-cart"></i>Comandas</a></li>
                    <li><a href="#"><i class="fa fa-money"></i>Comissões</a></li>
                </ul>
            </div>
        </div>
    </div>

    @else
    <div class="col-12 col-sm-4 col-lg-2 no-padding float-left">
        <nav tabindex="0" id="sidebar">
            <div class="sidebar-header">
                <h3 class="py-3 px-4">Dashboard</h3>
            </div>
            <ul class="list-unstyled">
                <li>
                    <a href="{{ URL::to('/clientes') }}" class="nav-link"><i class="fa fa-user"></i><span class="title">Clientes</span></a>
                    <a href="{{ URL::to('/profissionais') }}" class="nav-link"><i class="fa fa-graduation-cap"></i>Profissionais</a>
                    <a href="{{ URL::to('/produtosservicos') }}" class="nav-link"><i class="fa fa-trophy"></i>Produtos/Serviços</a>
                    <a href="{{ URL::to('/gcalendar') }}" class="nav-link"><i class="fa fa-calendar"></i>Agenda</a>
                    <a href="{{ URL::to('/comandas') }}" class="nav-link"><i class="fa fa-shopping-cart"></i>Comandas</a>
                    <a href="{{ URL::to('/comissoes') }}" class="nav-link"><i class="fa fa-money"></i>Comissões</a>
                    <a href="{{ URL::to('/financeiro') }}" class="nav-link"><i class="fa fa-shopping-bag"></i>Financeiro</a>
                </li>
            </ul>
        </nav>        
    </div>
    @endguest
    <div class="col-12 col-sm-8 col-lg-10 no-padding float-left">
        <div class="barra-status">
          <nav class="navbar bg-primary" id="navbar">
            <a class="navbar-brand text-white" href="{{ URL::to('/home') }}">{{ config('app.name') }}</a>
            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @guest
                    <li><a href="{{ route('login') }}">Login</a></li>
                    <!--<li><a href="{{ route('register') }}">Register</a></li>-->
                @else
                        <a href="#" class="dropdown-toggle text-white" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->name }} - {{ ((!isset(Auth::user()->emp_id))) ? Auth::user()->emp_id : '' }}
                        </a>
                    <li class="dropdown">                                
                        <a href="{{ route('logout') }}" class="text-white"
                            onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                            Logout
                        </a>
                    </li>
                @endguest
            </ul>
            <!-- Form Session Exit -->
            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
          </nav>
        </div>
        <div class="content-main">
            @yield('content')
        </div>
        
    </div>
</div>


</body>
</html>
