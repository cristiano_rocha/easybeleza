@extends('layouts.app')

@section('content')
<div class="container">

<nav class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('agenda') }}">Lista</a>
        <a href="{{ URL::to('agenda/create') }}">Novo</a>
    </div>
</nav>      

      <h3>{{$Agendas->agd_id}} - {{$Agendas->profissional->prof_nome}}</h3>

    <div class="jumbotron text-center">
        <p>
            <strong>ID:</strong> {{$Agendas->agd_id}}<br>
            <strong>Profissional:</strong> {{$Agendas->profissional->prof_nome}}<br>
            <strong>Cliente:</strong> {{$Agendas->cliente->cli_nome}}<br>
            <strong>Produto/Serviço:</strong> {{$Agendas->produtoservico->prosrv_nome}}<br>
            <strong>Data:</strong> {{$Agendas->agd_data}}
            <strong>Horário:</strong> {{$Agendas->agd_hora}}
        </p>
    </div>
</div>
@endsection