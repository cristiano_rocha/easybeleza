@extends('layouts.app')


    <script type="text/javascript" src="//code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script>
        $(document).ready( function() {
            $('#item-preco, #item-qtde, #item-desconto').blur(function(){
                var preco = $('#item-preco').val().replace(',','.');
                var qtde = $('#item-qtde').val().replace(',','.');
                var desconto = $('#item-desconto').val().replace(',','.');
                var subtotal = 0;

                if(preco == "") preco = 0;
                if(qtde == "") qtde = 1;
                if(desconto == "") desconto = 0;

                var subtotal   = parseFloat(preco-desconto) * parseInt(qtde);                
                valor = subtotal.toFixed(2);
                $("#item-subtotal").val(mascaraValor(valor));
            })
        });
    </script>

@section('content')

<?php $ldate = date('d/m/Y')?>

    <div class="container">    	
        <div class="card">
        	<div class="card-body">
	            <nav class="navbar navbar-inverse">
	                <div class="navbar-header">
	                    <a class="navbar-brand" href="{{ redirect()->getUrlGenerator()->previous() }}">Voltar</a>
	                </div>
	            </nav>

	            <div class="card-title">Novo Item do Pedido de Venda {{ $cmd_id }}</div>


	            <!-- se houver erros, serão mostrados -->
	            {{ Html::ul($errors->all()) }}                
	            {{ Form::open(['route' => ['comandas.itens.store', $cmd_id], 'method' => 'post']) }}  
                <div class="form-group">
                	<div class="row">
                        <div class="col-sm-12">
                            {{ Form::label('prof_id', 'Profissional') }} *
                            <select class="form-control" name="prof_id" id="prof_id">
                                @foreach ($jprof as $row)
                                <option value="{{ $row->prof_id }}">{{ $row->prof_nome }}</option>
                                @endforeach
                            </select>     
                        </div>
                        <div class="col-sm-12">
                            {{ Form::label('prosrv_id', 'Produto/Serviço') }} *
                            <select class="form-control" name="prosrv_id" id="sprosrv_id" onchange="check();">
                                <option value="0" selected="select">Selecione um produto</option>
                                @foreach ($jprod as $row)
                                <option value="{{ $row->prosrv_id }}">{{ $row->prosrv_id }} - {{ $row->prosrv_nome }}</option>
                                @endforeach
                            </select>                         
                        </div>
                        <div class="col-sm-2">
                            {{ Form::label('itn_preco', 'Preço R$') }} *
                            {{ Form::text('itn_preco', '0,00', array('id' => 'item-preco', 'class' => 'form-control', 'placeholder'=>'0,00', 'style'=>'display: inline-block; text-align: right;')) }}
                        </div>
                        <div class="col-sm-2">
                            {{ Form::label('itn_qtde', 'Qtde') }} *
                            {{ Form::text('itn_qtde', '1,00', array('id' => 'item-qtde', 'class' => 'form-control', 'placeholder'=>'1,00', 'style'=>'display: inline-block; text-align: right;')) }}
                        </div>
                        <div class="col-sm-2">
                            {{ Form::label('itn_ucom', 'Uidade') }} *
                            {{ Form::text('itn_ucom', 'UN', array('id' => 'item-un', 'class' => 'form-control', 'placeholder'=>'UN, CX, PC, LT, KG', 'style'=>'display: inline-block; text-align: right;')) }}
                        </div>
                        <div class="col-sm-2">
                            {{ Form::label('itn_desconto', 'Desconto R$') }}
                            {{ Form::text('itn_desconto', '0,00', array('id' => 'item-desconto', 'class' => 'form-control', 'placeholder'=>'0,00', 'style'=>'display: inline-block; text-align: right;')) }}
                        </div>
                        <div class="col-sm-2">
                            {{ Form::label('itn_subtotal', 'SubTotal R$') }}
                            {{ Form::text('itn_subtotal', '0,00', array('id' => 'item-subtotal', 'class' => 'form-control', 'placeholder'=>'0,00', 'style'=>'display: inline-block; text-align: right;')) }}
                        </div>
                        <div class="col-sm-2">
                            {{ Form::label('itn_data', 'Data') }} *
                            {{ Form::text('itn_data', $ldate, array('id' => 'datepicker', 'class' => 'form-control', 'placeholder'=>'dd/mm/aaaa', 'style'=>'display: inline-block; text-align: right;')) }}
                        </div>
                    </div>
                </div>
                <input type="hidden" id="cmd_id" name="cmd_id" value="{{$cmd_id}}">
                <input type="hidden" id="usu_id" name="usu_id" value="{{Auth::user()->id}}">
                {{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}
                {{ Form::close() }}
        	</div>
        </div>
    </div>


<!-- Script Produtos -->
<script>
	$('#sprosrv_id').on('change', function() {
    	$.ajax({
	        type: "get",
	        url: "//"+location.host+'/jsonprod',
	        dataType: "json",
	        data: 'prosrv_id='+$("#sprosrv_id option:selected").val(), 	        
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        },
	        success : function(row) {	
	        	preco = parseFloat(row);        			                
	            $('#item-preco').val(mascaraValor(row));	 
	            check();           
	        },
	        error: function (xhr, ajaxOptions, thrownError) {

	            alert(xhr.responseText);
	        }
    	});
	});

    $('#sprosrv_id').on('change', function() {
        $.ajax({
            type: "get",
            url: "//"+location.host+'/prduncomplete',
            dataType: "json",
            data: 'prosrv_id='+$("#sprosrv_id option:selected").val(),          
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success : function(row) {   
                ucom = row;                                    
                $('#item-un').val(ucom);               
            },
            error: function (xhr, ajaxOptions, thrownError) {

                alert(xhr.responseText);
            }
        });
    });
</script>
<!-- Script Produtos -->

<script>
	function check() { 		
          var preco = $('#item-preco').val().replace(',','.');
          var qtde = $('#item-qtde').val().replace(',','.');
          var desconto = $('#item-desconto').val().replace(',','.');
          var subtotal = 0;

          if(preco == "") preco = 0;
          if(qtde == "") qtde = 1;
          if(desconto == "") desconto = 0;

          subtotal = (preco-desconto) * qtde;

          var resultado   = parseFloat(preco-desconto) * parseInt(qtde);                

          $("#item-subtotal").val(mascaraValor(resultado));
    };

    function mascaraValor(valor) {
        valor = valor.toString().replace(/\D/g,"");
        valor = valor.toString().replace(/(\d)(\d{8})$/,"$1.$2");
        valor = valor.toString().replace(/(\d)(\d{5})$/,"$1.$2");
        valor = valor.toString().replace(/(\d)(\d{2})$/,"$1,$2");
        return valor                    
    }

    $('#prof_id').on('change',function(e){
        console.log(e);

        var prof_id = e.target.value;

        //ajax
        $.get("//"+location.host+'/ajaxprodutosservicos?prof_id=' + prof_id, function(data){
            //sucesso data
            $('#sprosrv_id').empty();
            $('#sprosrv_id').append('<option value="" selected>Selecione</option>');
            $.each(data, function(create, prosrvObj){
                $('#sprosrv_id').append('<option value="'+prosrvObj.prosrv_id+'">'+prosrvObj.produtoservico.prosrv_nome+'</option>');
            });

        });
    });


</script>


@endsection