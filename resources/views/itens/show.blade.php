@extends('layouts.app')

@section('content')
<div class="container">

<nav class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('comandas') }}">Lista</a>
        <a href="{{ URL::to('itens/create') }}">Novo</a>
        | <a href="{{ URL::to('comandas/' . $Itens->cmd_id) }}">Comanda {{$Itens->cmd_id}}</a>
    </div>
</nav>      

      <h1>Item: {{$Itens->itn_id}}</h1>

    <div class="jumbotron text-center">
        <p>
            <strong>ID:</strong> {{$Itens->itn_id}}<br>
            <strong>Profissional:</strong> {{$Itens->Profissional->prof_nome}}<br>
            <strong>Produto:</strong> {{$Itens->ProdutoServico->prosrv_nome}}<br>
            <strong>Preço:</strong> {{number_format($Itens->itn_preco,2, ',', '.')}}<br>
            <strong>Qtde:</strong> {{number_format($Itens->itn_qtde, 2, ',', '.')}}<br>
            <strong>SubTotal:</strong> {{number_format($Itens->itn_subtotal, 2)}}
        </p>
    </div>

</div>
@endsection