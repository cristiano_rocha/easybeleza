@extends('layouts.app')

@section('content')
<div class="container">

    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('produtosservicos') }}">Lista</a>
            <a href="{{ URL::to('produtosservicos/create') }}">Novo</a>
        </div>
    </nav> 

    <div class="card">
        <div class="card-body">
            <div class="card-title">PRODUTOS/SERVIÇOS</div>
            <!-- will be used to show any messages -->
            @if (Session::has('message'))
                <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif
            <div class="col-md-12">{{ $ProdutosServicos->links() }}</div>
            <div class="card-body produtosservicos-lista">
                <table class="table">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Cod. Barras</th>
                        <th>Nome</th>
                        <th>Unidade</th>
                        <th>Preço</th>
                        <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                      @foreach($ProdutosServicos as $row)  
                      @if ($row->emp_id == Auth::user()->emp_id)
                      <tr>
                        <td>{{$row->prosrv_id}}</td>
                        <td>{{$row->prosrv_nome}}</td>
                        <td>{{$row->prosrv_ucom}}</td>
                        <td>{{number_format($row->prosrv_preco,2, ',', '.')}}</td>
                        <td>{{number_format($row->prosrv_pcomissao,2, ',', '.')}}</td>
                        <!-- Botões Exibir, Alterar e Excluir -->
                        <td>
                            <a class="btn btn-small btn-success" href="{{ URL::to('produtosservicos/' . $row->prosrv_id) }}">Exibir</a>
                            <a class="btn btn-small btn-info" href="{{ URL::to('produtosservicos/' . $row->prosrv_id . '/edit') }}">Alterar</a>
                            <div class="btn">
                            {{ Form::open(array('url' => 'produtosservicos/' . $row->prosrv_id, 'class' => 'pull-right')) }}
                            {{ Form::hidden('_method', 'DELETE') }}
                            {{ Form::submit('Excluir', array('class' => 'btn btn-warning')) }}
                            {{ Form::close() }}
                            </div>
                        </td>
                      </tr>
                      @endif
                      @endforeach 
                    </tbody>
                </table>
            </div>
            <div class="col-md-12">{{ $ProdutosServicos->links() }}</div>
        </div>
    </div>  
</div>
@endsection