@extends('layouts.app')

@section('content')
<div class="container">

    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('produtosservicos') }}">Lista</a>
            <a href="{{ URL::to('produtosservicos/create') }}">Novo</a>
            <a class="btn btn-small btn-info" href="{{ URL::to('produtosservicos/' . $ProdutosServicos->prosrv_id . '/edit') }}">Alterar</a>
        </div>
    </nav>      

      <h1>{{$ProdutosServicos->prosrv_id}} - {{$ProdutosServicos->prosrv_nome}}</h1>

    <div class="jumbotron text-center">
        <p>
            <strong>ID:</strong> {{$ProdutosServicos->prosrv_id}}<br>
            <strong>Nome:</strong> {{$ProdutosServicos->prosrv_nome}}<br>
            <strong>Unidade:</strong> {{$ProdutosServicos->prosrv_ucom}}<br>
            <strong>Preço:</strong> {{number_format($ProdutosServicos->prosrv_preco,2, ',', '.')}}<br>
            <strong>% Comissão:</strong> {{number_format($ProdutosServicos->prosrv_pcomissao,2, ',', '.')}}
        </p>
    </div>
</div>
@endsection