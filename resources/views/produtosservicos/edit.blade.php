@extends('layouts.app')

@section('content')
    <script>
      $(document).ready(function() {
        $(function() {jQuery("#tab a:first").tab("show"); });

      });

      $( function() {
            $( "#tabs" ).tabs();
      } );
                            
                                    
    </script>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 
    <div class="container">
        <div class="card">
        <div class="card-body">
            <nav class="navbar navbar-inverse">
                <div class="navbar-header">
                    <a class="navbar-brand" href="{{ URL::to('produtosservicos') }}">Lista</a>
                    <a href="{{ URL::to('produtosservicos/create') }}">Novo</a>
                </div>
            </nav>

            <div class="card-title">CADASTRO DE PRODUTOS/SERVIÇOS</div>

            <!-- se houver erros, serão mostrados -->
            {{ Html::ul($errors->all()) }}
            {{ Form::model($ProdutosServicos, array('route' => array('produtosservicos.update', $ProdutosServicos->prosrv_id), 'method' => 'PUT')) }} 
                <div class="form-group">
                    <div class="tab-custom">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                            	<a class="nav-link active" href="#Produtos" aria-expanded="true" data-toggle="tab">
                                    <span class="hidden-xs">Produtos/Serviços</span>
                                </a>
                            </li>
                            <li class="nav-item">
                            	<a class="nav-link" href="#Comissoes" aria-expanded="false" data-toggle="tab">
                                    <span class="hidden-xs">Comissões</span>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content" id="tabContent">
                            <div class="tab-pane fade show active" id="Produtos">
                                <div class="row">
                                    <div class="col-sm-6 col-xs-6">
                                        {{ Form::label('prosrv_nome','Produto')}} *
                                        {{ Form::text('prosrv_nome', $ProdutosServicos->prosrv_nome, array(
                                            'class' => 'form-control',
                                            'id' => 'prosrv_nome'
                                        ))}}
                                    </div>

                                    <div class="col sm-6 col-xs-6">
                                        {{ Form::label('prosrv_descricao', 'Descrição')}}
                                        {{ Form::text('prosrv_descricao', $ProdutosServicos->prosrv_descricao, array(
                                            'class' => 'form-control',
                                            'id' => 'prosrv_descricao'
                                        ))}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3 col-xs-6">
                                        {{ Form::label('prosrv_codbarras','Código de Barras') }}
                                        {{ Form::text('prosrv_codbarras',$ProdutosServicos->prosrv_codbarras, array(
                                            'class' => 'form-control',
                                            'id'    => 'prosrv_codbarras'
                                        ))}}
                                    </div>

                                    <div class="col-sm-2 col-xs-3">
                                        {{ Form::label('prosrv_ucom','UN')}}
                                        {{ Form::text('prosrv_ucom', $ProdutosServicos->prosrv_ucom, array(
                                            'class' => 'form-control',
                                            'id' => 'prosrv_ucom'    
                                        ))}}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-2 col-xs-6">
                                        {{ Form::label('prosrv_custo','Custo') }}
                                        <div class="input-group">
                                            <input type="text" id="prosrv_custo" name="prosrv_custo" class="form-control" value="{{number_format($ProdutosServicos->prosrv_custo,2, ',', '.')}}">
                                            <span class="input-group-addon">
                                                <i class="fa fa-money"></i>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="col-sm-2 col-xs-6">
                                        {{ Form::label('prosrv_preco','Preço') }}
                                        <div class="input-group">
                                            <input type="text" name="prosrv_preco" id="prosrv_preco" class="form-control" value="{{number_format($ProdutosServicos->prosrv_preco,2, ',', '.')}}">
                                            <div class="input-group-addon">
                                                <span class="fa fa-money"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-2 col-xs-6">
                                        {{Form::label('prosrv_lucro','Lucro')}}
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <span>R$ </span>
                                            </div>
                                            <input type="text" id="prosrv_lucro" class="form-control" readonly value="{{number_format(($ProdutosServicos->prosrv_preco-$ProdutosServicos->prosrv_custo), 2, ',', '.')}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="Comissoes" class="tab-pane">
                                <div class="row">
                                    <div class="col-sm-12" id="divComissao">
                                        <table class="table" id="tableComissao">
                                            <tbody id="tbodyComissao">
                                                <tr>             
                                                    <td><a class="btn btn-success" href="{{ URL::to('produtosservicos/'. $ProdutosServicos->prosrv_id .'/produtosprofissionais/create') }}">Adicionar Comissão</a></td>
                                                </tr>
                                                @foreach($ProdProfissionais as $rowprof)
                                                    <tr class="colComissao">
                                                        <td>{{ $rowprof->Profissional->prof_id }}</td>
                                                        <td>{{ $rowprof->Profissional->prof_nome }}</td>
                                                        <td>{{ number_format($rowprof->proprof_pcomissao,2, ',', '.') }}%</td>
                                                    </tr>
                                                @endforeach
                                                
                                            </tbody>                                    
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- tab-custom -->
                </div><!-- form-group começa -->
              </div>
            </div>
            <input type="hidden" id="usu_id" name="usu_id" value="{{Auth::user()->id}}">
            <input type="hidden" id="emp_id" name="emp_id" value="{{ Auth::user()->emp_id }}">
            {{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}

            {{ Form::close() }}
        </div>
        </div>
    </div>
@endsection