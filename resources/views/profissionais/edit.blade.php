@extends('layouts.app')

@section('content')
    <script>
      $(document).ready(function() {
        $(function() {jQuery("#tab a:first").tab("show"); });

        // POST JSON

        $(document).on('submit','#form_cli', function(e) {
            var data = $("#reg-form").serialize();
            e.preventDefault();
                $.ajax({
                    type: 'POST',
                    url: '{{ url("api/clientes/salvarjson") }}',
                    data: data,
                    success: function(data) {
                        alert("Post bem sucedido");
                        console.log(data);
                    },

                    error: function(data) {
                        alert("error");
                    }
                });
                return false;
      });

      });
                            
                                    
    </script>
<div class="container">
    <div class="card">
        <div class="card-body">

            <nav class="navbar navbar-inverse">
                <div class="navbar-header">
                    <a class="navbar-brand" href="{{ URL::to('profissionais') }}">Lista</a>
                </div>
            </nav>

            <div class="card-title"><h1>Alterar {{$Profissionais->prof_nome}}</h1></div>

            <!-- se houver erros, serão mostrados -->
            {{ Html::ul($errors->all()) }}
            {{ Form::model($Profissionais, array('route' => array('profissionais.update', $Profissionais->prof_id), 'method' => 'PUT')) }} 
                <div class="form-group">
                    <div class="tab-custom">
                        <ul class="nav nav-tabs" id="tab">

                            <li class="nav-item active">
                                <a  class="nav-link" href="#tabGeral"  aria-expanded="true" data-toggle="tab">
                                    <span class="hidden-xs">Geral</span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="#tabContato"  aria-expanded="false" data-toggle="tab">
                                    <span class="hidden-xs">Contato</span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="#tabLocalizacao"  aria-expanded="false" data-toggle="tab">
                                    <span class="hidden-xs">Localização</span>
                                </a>
                            </li>
                        </ul>

                        <div class="tab-content" id="tabContent">
                            <div class="tab-pane fade show active" id="tabGeral">
                                <div class="row">
                                    <div class="col-sm-12">
                                        {{ Form::label('prof_nome', 'Nome') }} *
                                        {{ Form::text('prof_nome', null, array('class' => 'form-control')) }}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-3 col-xs-6">
                                            {{ Form::label('prof_cpf', 'CPF') }}
                                            {{ Form::text('prof_cpf', null, array('class' => 'form-control')) }}
                                        </div>
                                        <div class="col-sm-3 col-xs-6">
                                            {{ Form::label('prof_rg', 'RG') }}
                                            {{ Form::text('prof_rg', null, array('class' => 'form-control')) }}

                                        </div>
                                        <div class="col-sm-3 col-xs-6">
                                            <label for="prof_nascimento" class="control-label">Data de nascimento</label>
                                            <input type="text" class="form-control" id="prof_nascimento" maxlength="14" placeholder="dd/mm/aaaa" name="prof_nascimento" value="{{date('d/m/Y', strtotime($Profissionais->prof_nascimento))}}">
                                        </div>
                                    </div>
                                </div>
                              </div>
                                <div class="tab-pane fade show" id="tabContato">
                                    <div class="row">
                                        <div class="col-sm-12" id="divContato">
                                            <table class="table" id="tableContatos">
                                                <tbody id="tbodyTelefones">
                                                    <tr class="colTelefone">
                                                        <td class="form-group">
                                                            {{ Form::label('prof_celular', 'Celuar') }}
                                                        </td>

                                                        <td class="tdContato">                                                    
                                                            {{ Form::text('prof_celular', null, array('class' => 'form-control')) }}
                                                        </td>

                                                        <td class="tdOperadora">

                                                        </td>

                                                        <td class="tdObservacoes">

                                                        </td>

                                                    </tr>

                                                    <tr class="colTelefone">
                                                        <td class="form-group">
                                                            {{ Form::label('prof_email', 'E-mail') }}
                                                        </td>

                                                        <td class="tdContato">
                                                            {{ Form::text('prof_email', $Profissionais->prof_email, array('class' => 'form-control')) }}
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                  </div>
                                    <div class="tab-pane" id="tabLocalizacao">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    {{ Form::label('prof_endereco', 'Endereço') }}
                                                    {{ Form::text('prof_endereco', null, array('class' => 'form-control')) }}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-3 col-xs-6">
                                                    {{ Form::label('prof_bairro', 'Bairro') }}
                                                    {{ Form::text('prof_bairro', null, array('class' => 'form-control')) }}
                                                </div>

                                                <div class="col-sm-3 col-xs-6">
                                                    {{ Form::label('prof_cep', 'CEP') }}
                                                    {{ Form::text('prof_cep', null, array('class' => 'form-control')) }}
                                                </div>

                                                <div class="col-sm-3 col-xs-6">
                                                    {{ Form::label('prof_cidade', 'Cidade') }}
                                                    {{ Form::text('prof_cidade', null, array('class' => 'form-control')) }}
                                                </div>

                                                <div class="col-sm-3 col-xs-6">
                                                    {{ Form::label('prof_estado', 'Estado') }}
                                                    {{ Form::text('prof_estado', null, array('class' => 'form-control')) }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}  
                    </div>
                </div>
                        
        </div>
    </div>
    {{ Form::close() }}
</div>
@endsection