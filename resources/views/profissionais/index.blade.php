@extends('layouts.app')

@section('content')
<div class="container">
    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('profissionais') }}">Lista</a>
            <a href="{{ URL::to('profissionais/create') }}">Novo</a>
        </div>
    </nav> 

    <div class="card">
        <div class="card-body">
            <div class="card-title">PROFISSIONAIS - {{ Auth::user()->emp_id }}</div>
            <!-- will be used to show any messages -->
            @if (Session::has('message'))
                <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif
            <div class="col-md-6">{{ $Profissionais->links() }}</div>
            <div class="card-body profissionais-lista">
                <table class="table">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>Nascimento</th>
                        <th>Telefone</th>
                        <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>           
                      @foreach($Profissionais as $row)  
                      @if ($row->emp_id == Auth::user()->emp_id)
                      <tr>
                        <td>{{$row->prof_id}}</td>
                        <td>{{$row->prof_nome}}</td>
                        <td>{{ databr($row->prof_nascimento)}}</td>
                        <td>{{fone($row->prof_celular)}}</td>
                        <!-- Botões Exibir, Alterar e Excluir -->
                        <td>
                            <div class="btn btn-blue"><a href="{{ URL::to('profissionais/' . $row->prof_id) }}"><span><i class="fa fa-eye"></i > Exibir</span></a></div>
                            <div class="btn btn-green"><a href="{{ URL::to('profissionais/' . $row->prof_id . '/edit') }}"><span><i class="fa fa-pencil" aria-hidden="true"></i> Editar</span></a></div>
                            <div class="btn">
                            {{ Form::open(array('url' => 'profissionais/' . $row->prof_id, 'class' => 'pull-right')) }}
                            {{ Form::hidden('_method', 'DELETE') }}
                            {{Form::submit('Excluir',['type' => 'submit'])}}
                            {{ Form::close() }}
                            </div>
                        </td>
                      </tr>
                      @endif
                      @endforeach 
                    </tbody>
                </table>
            </div>
            <div class="col-md-6">{{ $Profissionais->links() }}</div>
        </div>
    </div>
</div>
@endsection