@extends('layouts.app')


<style>
#div1, #div2 {
    float: left;
    min-width: 250px;
    min-height: 75px;*/
    margin: 10px;
    padding: 10px;
    border: 4px dotted #647aab;
}
.item-drag {
  cursor: move;
}


.item-drag.over {
  border: 2px dashed #000;
}
</style>

@section('content')
    <script>
      $(document).ready(function() {
        $(function() {jQuery("#tab a:first").tab("show"); });

        // POST JSON

        $(document).on('submit','#form_cli', function(e) {
            var data = $("#reg-form").serialize();
            e.preventDefault();
                $.ajax({
                    type: 'POST',
                    url: '{{ url("api/clientes/salvarjson") }}',
                    data: data,
                    success: function(data) {
                        alert("Post bem sucedido");
                        console.log(data);
                    },

                    error: function(data) {
                        alert("error");
                    }
                });
                return false;
      });

      });
                            
                                    
    </script>
    <div class="container">
        <div class="card">
        <div class="card-body">
            <nav class="navbar navbar-inverse">
                <div class="navbar-header">
                    <a class="navbar-brand" href="{{ URL::to('profissionais') }}">Lista</a>
                    <a href="{{ URL::to('profissionais/create') }}">Novo</a>
                </div>
            </nav>

            <div class="card-title">CADASTRO DE PROFISSIONAIS</div>

            <!-- se houver erros, serão mostrados -->
            {{ Html::ul($errors->all()) }}
            {{ Form::open(['route' => 'profissionais.store', 'method' => 'post']) }}    
                <div class="form-group">
                    <div class="tab-custom">
                        <ul class="nav nav-tabs" id="tab">

                            <li class="nav-item active">
                                <a  class="nav-link" href="#tabGeral"  aria-expanded="true" data-toggle="tab">
                                    <span class="hidden-xs">Geral</span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="#tabContato"  aria-expanded="false" data-toggle="tab">
                                    <span class="hidden-xs">Contato</span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="#tabLocalizacao"  aria-expanded="false" data-toggle="tab">
                                    <span class="hidden-xs">Localização</span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="#tabServicos"  aria-expanded="false" data-toggle="tab">
                                    <span class="hidden-xs">Serviços</span>
                                </a>
                            </li>
                        </ul>

                        <div class="tab-content" id="tabContent">
                            <div class="tab-pane fade show active" id="tabGeral">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label class="control-label">Nome*</label>
                                        <input type="text" class="form-control" id="prof_nome" name="prof_nome">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-3 col-xs-6">
                                            <label for="prof_cpf">CPF</label>
                                            <input type="text" class="form-control" id="cpf" maxlength="14" autocomplete="off" name="prof_cpf">
                                        </div>
                                        <div class="col-sm-3 col-xs-6">
                                            <label for="" class="control-label">RG</label>
                                            <input type="text" class="form-control" id="prof_rg" name="prof_rg" maxlength="15" name="prof_rg">
                                        </div>
                                        <div class="col-sm-3 col-xs-6">
                                            <label for="prof_nascimento" class="control-label">Data de nascimento</label>
                                            <input type="text" class="form-control datepicker" id="prof_nascimento" data-provide="datepicker" maxlength="10" placeholder="dd/mm/aaaa" name="prof_nascimento">                                          
                                        </div>
                                    </div>
                                </div>
                              </div>
                                <div class="tab-pane fade show" id="tabContato">
                                    <div class="row">
                                        <div class="col-sm-12" id="divContato">
                                            <table class="table" id="tableContatos">
                                                <tbody id="tbodyTelefones">
                                                    <tr class="colTelefone">
                                                        <td class="form-group">
                                                            Celular
                                                        </td>

                                                        <td class="tdContato">
                                                            <input type="text" class="col-sm-12 form-control" id="contato" maxlength="15" placeholder="Número" aria-required="true" name="prof_celular">
                                                        </td>

                                                        <td class="tdOperadora">

                                                        </td>

                                                        <td class="tdObservacoes">

                                                        </td>

                                                    </tr>

                                                    <tr class="colTelefone">
                                                        <td class="form-group">
                                                            E-mail
                                                        </td>

                                                        <td class="tdContato">
                                                            <input type="text" class="col-sm-12 form-control" id="prof_email" maxlength="50" placeholder="E-mail" aria-required="true" name="prof_email">
                                                        </td>

                                                        <td class="tdObservacoes">

                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                  </div>
                                    <div class="tab-pane" id="tabLocalizacao">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <label for="prof_endereco" class="control-label">Endereço</label>
                                                    <input type="text" class="form-control" id="prof_endereco" name="prof_endereco" maxlength="150">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-3 col-xs-6">
                                                    <label for="prof_bairro" class="control-label">Bairro</label>
                                                    <input type="text" class="form-control" id="prof_bairro" name="prof_bairro" maxlength="150">
                                                </div>

                                                <div class="col-sm-3 col-xs-6">
                                                    <label for="prof_cep" class="control-label" id="prof_cep">CEP</label>
                                                    <input type="text" class="form-control" id="prof_cep" name="prof_cep" maxlength="10">
                                                </div>

                                                <div class="col-sm-3 col-xs-6">
                                                    <label for="prof_cidade" class="control-label" id="prof_cidade">Cidade</label>
                                                    <input type="text" class="form-control" id="prof_cidade" name="prof_cidade" maxlength="150">
                                                </div>

                                                <div class="col-sm-3 col-xs-6">
                                                    <label for="prof_estado" class="control-label">Estado</label>
                                                    <input type="text" class="form-control" id="prof_estado" name="prof_estado" maxlength="150">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tabServicos">
                                        <div class="row">
                                            <div class="clear"><BR /></div>
                                            <div class="clear col-sm-12 col-xs-12"> </div>
                                            <div class="col-sm-3 col-xs-5">
                                                <div id="div1" class="list-group" ondrop="drop(event)" ondragover="allowDrop(event)"">
                                                    <a href="#" class="list-group-item item-drag" draggable="true" ondragstart="drag(event)" id="drag1">Corte de cabelo</a>
                                                    <a href="#" class="list-group-item item-drag" draggable="true" ondragstart="drag(event)" id="drag2">Escova</a>
                                                    <a href="#" class="list-group-item item-drag" draggable="true" ondragstart="drag(event)" id="drag3">Drenagem</a>
                                                </div>
                                            </div>                                                
                                            <div class="col-sm-1 col-xs-2">
                                                --><BR />
                                                <--
                                            </div>
                                            <div class="col-sm-3 col-xs-5">
                                                <div id="div2" class="list-group" ondrop="drop(event)" ondragover="allowDrop(event)">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" id="usu_id" name="usu_id" value="{{Auth::user()->id}}">
                        <input type="hidden" id="emp_id" name="emp_id" value="{{ Auth::user()->emp_id }}">
                        {{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}
                        {{ Form::close() }}
                    </div>
                </div>
                <input type="hidden" id="usu_id" name="usu_id" value="{{Auth::user()->id}}">
                <input type="hidden" id="emp_id" name="emp_id" value="{{ Auth::user()->emp_id }}">
                {{ Form::submit('Salvar', array('class' => 'btn btn-blue')) }}

            {{ Form::close() }}
        </div>
        </div>
    </div>
    <script>
        function handleDragStart(ev) {
  this.style.opacity = '0.4';  // this / e.target is the source node.
}

function handleDragOver(ev) {
  if (ev.preventDefault) {
    ev.preventDefault(); // Necessary. Allows us to drop.
  }

  ev.dataTransfer.dropEffect = 'move';  // See the section on the DataTransfer object.

  return false;
}

function handleDragEnter(ev) {
  // this / e.target is the current hover target.
  this.classList.add('over');
}

function handleDragLeave(ev) {
  this.classList.remove('over');  // this / e.target is previous target element.
}

function handleDrop(ev) {
  // this / e.target is current target element.

  if (ev.stopPropagation) {
    ev.stopPropagation(); // stops the browser from redirecting.
  }

  // See the section on the DataTransfer object.

  return false;
}

function handleDragEnd(ev) {
  // this/e.target is the source node.

  [].forEach.call(cols, function (col) {
    col.classList.remove('over');
  });
}

var cols = document.querySelectorAll('#div1 .item-drag');
[].forEach.call(cols, function(col) {
  col.addEventListener('dragstart', handleDragStart, false);
  col.addEventListener('dragenter', handleDragEnter, false);
  col.addEventListener('dragover', handleDragOver, false);
  col.addEventListener('dragleave', handleDragLeave, false);
  col.addEventListener('drop', handleDrop, false);
  col.addEventListener('dragend', handleDragEnd, false);
});       

        function allowDrop(ev) {
            ev.preventDefault();
        }

        function drag(ev) {
            ev.dataTransfer.setData("text", ev.target.id);
        }

        function drop(ev) {
            ev.preventDefault();
            var data = ev.dataTransfer.getData("text");
            ev.target.appendChild(document.getElementById(data));
        }
    </script>
@endsection