@extends('layouts.app')

@section('content')
    <div class="container">
        <nav class="navbar navbar-inverse">
            <div class="navbar-header">
                <a class="navbar-brand" href="{{ URL::to('profissionais') }}">Lista</a>
                <a href="{{ URL::to('profissionais/create') }}">Novo</a>
            </div>
        </nav>      

        <h3>{{$Profissionais->prof_id}} - {{$Profissionais->prof_nome}}</h3>

        <div class="jumbotron text-center">
            <p>
                <strong>ID:</strong> {{$Profissionais->prof_id}}<br>
                <strong>Nome:</strong> {{$Profissionais->prof_nome}}<br>
                <strong>Nascimento:</strong> {{databr($Profissionais->prof_nascimento)}}<br>
                <strong>Celular:</strong> {{fone($Profissionais->prof_celular)}}
            </p>
        </div>

    </div>
@endsection