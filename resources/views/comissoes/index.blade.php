@extends('layouts.app')

@section('content')
<div class="container">

    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('comissoes') }}">Lista</a>
            <a href="{{ URL::to('comissoes/create') }}">Novo</a>
        </div>
    </nav> 

    <div class="card">
        <div class="card-body">
            <div class="card-title">COMISSÕES</div>
            <div class="card-body comissoes-lista">
                <table class="table">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Profissional ID</th>
                        <th>ID Cliente</th>
                        <th>Prod/Serv ID</th>
                        <th>Comissão %</th>
                        </tr>
                    </thead>
                    <tbody>
                      @foreach($Comissoes as $row)                        
                      @if ($row->emp_id == Auth::user()->emp_id)
                      <tr>
                        <td>{{$row->cms_id}}</td>
                        <td>{{$row->prof_id}}</td>
                        <td>{{$row->cli_id}}</td>
                        <td>{{$row->prosrv_id}}</td>
                        <td>{{$row->cms_pcomissao}}</td>
                      </tr>
                      @endif
                      @endforeach 
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    

@endsection
</div>