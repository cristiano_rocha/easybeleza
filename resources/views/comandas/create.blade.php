@extends('layouts.app')

@section('content')


    {{--  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/3/css/bootstrap.css">  --}}
    <link rel="stylesheet" href="{{ URL::to('css/select2.min.css') }}"/>


    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    {{--  <script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"></script>  --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
    <!-- Include Required Prerequisites -->
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <!-- Include Date Range Picker -->
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
    <script type="text/javascript" src="{{ URL::to('js/pt-br.js') }}"></script>
    <script type="text/javascript" src="{{ URL::to('js/jquery.mask.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::to('js/select2.min.js')}}"></script>


</head>

<body>
<div class="container">

    <div class="container">
        <!-- NOVA COMAND -->
        {{ Html::ul($errors->all()) }}

        <div class="row">
            <div class="col-sm-12">
            {{Form::open(['route' => 'comandas.store','method' => 'post'])}}
                <div class="form-group mt-5">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="input-group">
                                <select name="cli_id" id="cli_id" class="form-control">
                                    <option value="0" selected="selected">Selecione o cliente</option>
                                    @foreach($jCli as $row)
                                        <option value="{{ $row->cli_id }}">{{$row->cli_nome}}</option>
                                    @endforeach
                                </select>
                                <span class="input-group-addon">
                                    <i class="fa fa-user pr-1"></i>Cliente
                                </span>
                            </div>
                        </div>

                        <div class="col-sm-3 pt-1">
                            <div class="input-group">
                                <input type="text" id="date" class="form-control datepicker" data-provide="datepicker" name="cmd_data">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar pr-1"></i> Data
                                </span>
                            </div>
                        </div>

                        <div class="col-sm-3 pt-1">
                            <div class="input-group">
                                <input type="text" id="hora" name="cmd_hora" class="form-control">
                                <span class="input-group-addon">
                                    <i class="fa fa-clock-o pr-1"></i> Hora
                                </span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <table class="table table-striped table-condensed" style="margin-top: 65px;">
                            <thead>
                                <tr>
                                    <th width="150" class="text-left">Comanda</th>
                                    <th class="text-left">Data</th>
                                    <th class="text-left">Hora</th>
                                    <th class="text-left">Cliente</th>
                                </tr>
                            </thead>

                            <tbody>
                                <tr>
                                    <td colspan="5"><div class="alert alert-default text-center">Nenhuma comanda encontrada</div></td>
                                </tr>
                            </tbody>
                        </table>

                        <input type="hidden" id="cmd_subtotal" name="cmd_subtotal" value="0">
                        <input type="hidden" id="cmd_total" name="cmd_total" value="0">
                        <input type="hidden" id="usu_id" name="usu_id" value="{{Auth::user()->id}}">
                        <input type="hidden" id="emp_id" name="emp_id" value="{{ Auth::user()->emp_id }}">
                        <button class="btn btn-primary float-right">Abrir comanda</button>
                        {{ Form::close() }}
                    </div>
                </div>
            
            </div>
        </div>
    </div>
</div>
@endsection