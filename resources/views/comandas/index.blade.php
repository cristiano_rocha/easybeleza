@extends('layouts.app')

@section('content')
<div class="container">

    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('comandas') }}">Lista</a>
            <a href="{{ URL::to('comandas/create') }}">Novo</a>
        </div>
    </nav>   

    <div class="card">
        <div class="card-body">
            <div class="card-title">COMANDAS</div>
            <!-- will be used to show any messages -->
            @if (Session::has('message'))
                <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif
            <div class="card-body comandas-lista">
                <table class="table">
                    <thead>
                      <tr>
                        <th>Status</th>
                        <th>ID</th>
                        <th>Cliente</th>
                        <th>Data</th>
                        <th>SubTotal</th>
                        <th>Total</th>
                        <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                      @foreach($Comandas as $row)
                      @if ($row->emp_id == Auth::user()->emp_id)
                      <tr>
                        <td>{{($row->cmd_status == 2) ? 'Fechado' : 'Aberto'}}</td>
                        <td>{{$row->cmd_id}}</td>
                        <td>{{$row->cliente->cli_nome}}</td>
                        <td>{{date( 'd/m/Y' , strtotime($row->cmd_data))}}</td>
                        <td>{{number_format($row->cmd_subtotal, 2, ',', '.')}}</td>
                        <td>{{number_format($row->cmd_total, 2, ',', '.')}}</td>
                        <!-- Botões Exibir, Alterar e Excluir -->
                        <td>
                            <a class="btn btn-small btn-success" href="{{ URL::to('comandas/' . $row->cmd_id) }}">V</a>
                            @if($row->cmd_status != 2)
                                <a class="btn btn-small btn-info" href="{{ URL::to('comandas/' . $row->cmd_id . '/edit') }}">A</a>
                                <div class="btn">
                                    {{ Form::open(array('url' => 'comandas/' . $row->cmd_id, 'class' => 'pull-right')) }}
                                    {{ Form::hidden('_method', 'DELETE') }}
                                    {{ Form::submit('E', array('class' => 'btn btn-warning')) }}
                                    {{ Form::close() }}
                                </div>
                            @endif
                        </td>
                      </tr>
                      @endif
                      @endforeach 
                    </tbody>
                </table>
                {{ $Comandas->links() }}
            </div>
        </div>
    </div>    
</div>
@endsection