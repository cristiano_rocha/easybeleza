@extends('layouts.app')

@section('content')
<?php
$SomaPag = 0;
$TotalPag = 0;

foreach ($Comanda->Pagamentos as $row){
    $SomaPag = $SomaPag + $row->pag_valor;
}
$TotalPag = $Comanda->cmd_subtotal-$SomaPag;
?>
<div class="container">

<nav class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('/comandas/'.$Comanda->cmd_id) }}">Voltar</a>
    </div>
</nav>      
        <!-- MODAL PAGAMENTOS -->
        <div id="modalPagamentos" class="modal fade" role="dialog">
            <div class="modal-dialog">    
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title col-md-8">Pagamentos</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  {{ Form::open(['url' => 'comandas/'. $Comanda->cmd_id.'/pagar','method' => 'post']) }}                                                                
                  <div class="modal-body">
                    <p>Informe o valor e clique no tipo de pagamento.</p>
                    <div class="col-sm-5">
                        <select name="tpg_id" id="tpg_id" class="form-control">
                            @foreach ($TiposPagamentos as $rowtp)   
                                <option value="{{ $rowtp->tpg_id }}" {{($rowtp->tpg_id == 1) ? 'selected' : ''}}>{{$rowtp->tpg_nome}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-3">
                        {{ Form::text('pag_valor', number_format($TotalPag, 2, ',', '.'), array('id' => 'pag_valor', 'class' => 'form-control', 'placeholder'=>'0,00', 'style'=>'display: inline-block; text-align: right;')) }}
                    </div>
                  </div>
                  <div class="modal-footer">
                    <div class="total">
                        <span class="pag-total">TOTAL ---> R$ {{number_format($Comanda->cmd_subtotal, 2, ',', '.')}}</span>
                    </div>
                    <input type="hidden" id="cmd_id" name="cmd_id" value="{{$Comanda->cmd_id}}">
                    {{ Form::submit('Salvar', array('class' => 'btn btn-small btn-success')) }}
                  </div>                  
                  {{ Form::close() }}
                </div>
            </div>
        </div>

        <!-- FATURAR -->
        {{ Html::ul($errors->all()) }}
        <div class="row">          
                <div class="col-sm-4 col-xs-4">
                    <span class="col-md-6 pag-subtotal">SUBTOLTAL</span>
                    <span class="col-md-6 pag-subtotal">R$ {{number_format($Comanda->cmd_subtotal, 2, ',', '.')}}</span>
                </div>       
                <div class="col-sm-4 col-xs-4">                    
                    <span class="col-md-6 pag-desconto">DESCONTO</span>
                    <span class="col-md-6 pag-desconto">R$ 0,00</span>
                </div>       
                <div class="col-sm-4 col-xs-4">
                    <span class="col-md-6 pag-acrescimo">ACRÉSCIMO</span>
                    <span class="col-md-6 pag-acrescimo">R$ 0,00</span>
                </div>                       
                <div class="clear col-sm-12"><BR /></div>
                <div class="col-md-9">
                    <span class="pag-total">TOTAL</span>
                </div>       
                <div class="col-md-3">
                    <span class="pag-total">R$ {{number_format($Comanda->cmd_subtotal, 2, ',', '.')}}</span>
                </div>
        </div>
        <div class="clear col-sm-12"><BR /></div>
        <div class="clear col-sm-12"><BR /></div>
        <table class="table">
              <thead>       
                <tr>      
                  <th>Tipo</th>
                  <th>Valor R$</th>
                  <th> </th>  
                </tr>
              </thead>              
              <tbody>       
              @foreach ($Comanda->Pagamentos as $row)
                <tr>
                    <td>{{ $row->TipoPagamento->tpg_nome }}</td>
                    <td>{{ number_format($row->pag_valor, 2, ',', '.') }}</td>
                    <!-- Botões Exibir, Alterar e Excluir -->
                    <td>
                        <div class="btn">
                        {{ Form::open(array('url' => 'comandas/'. $row->cmd_id .'/fatura/' . $row->pag_id, 'class' => 'pull-right')) }}
                        {{ Form::hidden('_method', 'DELETE') }}
                        {{ Form::submit('E', array('class' => 'btn btn-warning')) }}
                        {{ Form::close() }}
                        </div>
                    </td>
                </tr>            
              @endforeach
              </tbody>
            </table>
        <!-- Abrir modal Pagamentos -->
        <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#modalPagamentos">Pagamentos</button>
</div>

<!-- Script Pagamentos -->
<script>
	function check() { 		
          var valor = $('#pag_valor').val().replace(',','.');

          if(valor == "") valor = 0;

          var resultado   = parseFloat(valor);                

          $("#pag_valor").val(mascaraValor(valor));
    };

    function mascaraValor(valor) {
        valor = valor.toString().replace(/\D/g,"");
        valor = valor.toString().replace(/(\d)(\d{8})$/,"$1.$2");
        valor = valor.toString().replace(/(\d)(\d{5})$/,"$1.$2");
        valor = valor.toString().replace(/(\d)(\d{2})$/,"$1,$2");
        return valor                    
    }


</script>

@endsection