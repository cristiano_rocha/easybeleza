@extends('layouts.app')

@section('content')


    <link rel="stylesheet" href="{{ URL::to('css/fullcalendar.min.css') }}" type="text/css">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/3/css/bootstrap.css">
    <link rel="stylesheet" href="{{ URL::to('css/select2.min.css') }}"/>


    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    {{--  <script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"></script>  --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
    <!-- Include Required Prerequisites -->
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <!-- Include Date Range Picker -->
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
    <script type="text/javascript" src="{{ URL::to('js/fullcalendar.js') }}"></script>
    <script type="text/javascript" src="{{ URL::to('js/pt-br.js') }}"></script>
    <script type="text/javascript" src="{{ URL::to('js/jquery.mask.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::to('js/select2.min.js')}}"></script>


    <script>
        $(document).ready(function(){
            $('#calendario').fullCalendar({
                lang: 'pt-br',
                header: {
                    center: 'title',
                    left: 'prev,next,today',
                    right: 'month,agendaWeek, agendaDay'
                },

                events: [
                    {
                        title: 'Manicure - Andressa',
                        start: '2017-10-12'
                    }
                ],
                color: 'blue',
                textColor: 'white'


            });

            $('#tel').mask('00 0000-00000');
            $('#hora').mask('00:00');
            $('#date').mask('00/00/0000');
            
            $('.servprod').select2();
        });
    </script>

</head>

<body>
<div class="container">

    <div class="container">
        <!-- NOVA COMAND -->


        <!-- se houver erros, serão mostrados -->
        {{ Html::ul($errors->all()) }}        
        <div class="col-sm-12">
            <div class="form-row">
                <div class="card">
                    <div class="card-title"><h3>Nova comanda</h3></div>
                    <div class="card-body">
                        {{ Form::model($Comanda, array('route' => array('comandas.update', $Comanda->cmd_id), 'method' => 'PUT')) }}
                        <div class="col-sm-3">
                            <div class="input-group">
                                <select class="form-control" name="cli_id" id="cli_id" onchange="check();">
                                    <option value="0" selected="select">Selecione cliente</option>
                                    @foreach ($jCli as $row)                                        
                                        <option value="{{ $row->cli_id }}" {{($Comanda->cli_id = $row->cli_id) ? 'selected' : ''}}>{{ $row->cli_nome }}</option>
                                    @endforeach
                                </select>   
                                <span class="input-group-addon">
                                    <i class="fa fa-user"></i> Cliente
                                </span>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="input-group">
                                <select class="form-control" name="prof_id" id="prof_id" onchange="check();">
                                    <option value="0" selected="select">Selecione profissional</option>
                                    @foreach ($jProf as $row)
                                        <option value="{{ $row->prof_id }}" {{($Comanda->prof_id = $row->prof_id) ? 'selected' : ''}}>{{ $row->prof_nome }}</option>
                                    @endforeach
                                </select>   
                                <span class="input-group-addon">
                                    <i class="fa fa-user"></i> Profissional
                                </span>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="input-group">                                
                                {{ Form::text('cmd_data', date('d/m/Y', strtotime($Comanda->cmd_data)), array('class' => 'form-control datepicker', 'data-provide' => 'datepicker')) }}
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i> Data
                                </span>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="input-group">
                                <input type="text" type="text" id="hora" class="form-control">
                                <span class="input-group-addon">
                                    <i class="fa fa-clock-o"></i> Horário
                                </span>
                            </div>
                        </div>                        

                        <input type="hidden" id="usu_id" name="cmd_subtotal" value="0">
                        <input type="hidden" id="usu_id" name="cmd_total" value="0">
                        <input type="hidden" id="usu_id" name="usu_id" value="{{Auth::user()->id}}">
                        <input type="hidden" id="emp_id" name="emp_id" value="{{ Auth::user()->emp_id }}">
                        <div class="botoes">
                            <a class="btn btn-warning" href="{{ redirect()->getUrlGenerator()->previous() }}">Voltar</a>
                            <button class="btn btn-primary float-right">Salvar</button>
                        </div>
                        {{ Form::close() }}

                    </div>
                </div>

                <div class="card">
                    <div class="card-title">Novo Lançamento</div>
                    <div class="card-body">
                        <div class="col-sm-2">
                            <div class="input-group">
                                <input type="text" class="form-control">
                                <span class="input-group-addon">
                                    <i class="fa fa-user"></i>
                                </span>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-group">
                                <select class="form-control servprod" id="servprod">
                                    <option selected>Selecione...</option>
                                    <option value="produto">Produto</option>
                                    <option value="servico">Serviço</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="input-group">
                                <input type="text" type="number" class="form-control">
                                <span class="input-group-addon">
                                    Qtd
                                </span>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="input-group">
                                <input type="text" class="form-control">
                                <span class="input-group-addon">
                                    Descrição
                                </span>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="input-group">
                                <input type="text" class="form-control">
                                <span class="input-group-addon">
                                    <i class="fa fa-money"></i>
                                </span>
                            </div>
                        </div>

                        <table class="table table-striped table-condensed" style="margin-top: 65px">
                            <thead>
                                <tr>
                                    <th width="150" class="text-left">Data</th>
                                    <th class="text-left">Hora</th>
                                    <th class="text-left">Serviço/Produto</th>
                                    <th class="text-left">Profissional</th>
                                    <th class="text-left">Valor</th>
                                    <th class="text-left">Quantidade</th>
                                    <th class="text-left">Total</th>
                                </tr>
                            </thead>

                            <tbody>
                                <tr>
                                    <td colspan="7">
                                        <div class="alert alert-default text-center">Nenhum lançamento encontrado</div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <input type="hidden" id="usu_id" name="usu_id" value="{{Auth::user()->id}}">
                        <input type="hidden" id="usu_id" name="usu_id" value="{{Auth::user()->emp_id}}">
                        {{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}          
                    </div>
                </div>
            </div>
        </div>        
    </div>
</div>
@endsection