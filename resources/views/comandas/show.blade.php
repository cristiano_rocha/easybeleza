@extends('layouts.app')

@section('content')
<div class="container">

<nav class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('comandas') }}">Lista</a>
        <a href="{{ URL::to('comandas/create') }}">Novo</a>
    </div>
</nav>      

    <h1>Comanda: {{$Comandas->cmd_id}}</h1>
    <!-- Mensagens de erro -->
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif

    <div class="jumbotron">        
            <div class="col-md-2 col-xs-2"><strong>Comanda ID:</strong> {{$Comandas->cmd_id}}</div>
            <div class="col-md-10 col-xs-10"><strong>Cliente:</strong> {{$Comandas->cli_id}} - {{$Comandas->Cliente->cli_nome}}</div>
            <div class="col-md-3 col-xs-3"><strong>Data:</strong> {{date( 'd/m/Y' , strtotime($Comandas->cmd_data))}}</div>
            <div class="col-md-3 col-xs-3"><strong>SubTotal:</strong> {{number_format($Comandas->cmd_subtotal, 2, ',', '.')}}</div>
            <div class="col-md-3 col-xs-3"><strong>Total:</strong> {{number_format($Comandas->cmd_total, 2, ',', '.')}}</div>
            <table class="table">
              <thead>
                <tr>
                  <td colspan="6">
                  @if($Comandas->cmd_status == 2)
                    <h5>Comanda fechada</h5>
                  @else
                    <a class="btn btn-small btn-success" href="{{ URL::to('comandas/'. $Comandas->cmd_id .'/itens/create') }}">+ Item</a>
                    <a class="btn btn-small btn-success" href="{{ URL::to('comandas/'.$Comandas->cmd_id.'/fatura') }}"> Pagamentos</a>
                    <a class="btn btn-small btn-success" href="{{ url('comandas/'.$Comandas->cmd_id.'/faturar') }}"> Faturar</a>
                  @endif
                  </td>
                </tr>        
                <tr>
                  <th>ID</th>      
                  <th>Profissional</th>   
                  <th>Item</th>        
                  <th>Preço</th>          
                  <th>Qtde</th>       
                  <th>SubTotal</th>
                  <th>Ações</th> 
                </tr>
              </thead>              
              <tbody>       
              @foreach ($Comandas->Itens as $row)
                <tr>
                    <td>{{ $row->itn_id }}</td>
                    <td>{{ $row->Profissional->prof_nome }}</td>
                    <td>{{ $row->ProdutoServico->prosrv_nome }}</td>
                    <td>{{ number_format($row->itn_preco, 2, ',', '.') }}</td>
                    <td>{{ number_format($row->itn_qtde, 2, ',', '.') }}</td>
                    <td>{{ number_format($row->itn_subtotal, 2, ',', '.') }}</td>
                    
                    <!-- Botões Exibir, Alterar e Excluir -->
                    <td>
                        <a class="btn btn-small btn-success" href="{{ URL::to('comandas/'. $Comandas->cmd_id .'/itens/' . $row->itn_id) }}">V</a>
                        @if($Comandas->cmd_status != 2)
                        <a class="btn btn-small btn-info" href="{{ URL::to('comandas/'. $Comandas->cmd_id .'/itens/' . $row->itn_id . '/edit') }}">A</a>
                        <div class="btn">
                        {{ Form::open(array('url' => 'comandas/'. $Comandas->cmd_id .'/itens/' . $row->itn_id, 'class' => 'pull-right')) }}
                        {{ Form::hidden('_method', 'DELETE') }}
                        {{ Form::submit('E', array('class' => 'btn btn-warning')) }}
                        {{ Form::close() }}
                        </div>
                        @endif
                    </td>                    
                </tr>            
              @endforeach
              </tbody>
            </table>
    </div>

</div>
@endsection