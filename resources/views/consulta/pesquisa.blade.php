@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row mt-5">
        <div class="col-sm-12">
            <form class="typeahead" role="search">
                <div class="input-group">
                <input type="search" class="form-control" id="search-input" name="q" autocomplete="off">
                    <div class="input-group-btn">
                        <span class="input-group-btn">
                            <button class="btn btn-primary">Consultar</button>
                            {{ csrf_field() }}
                        </span>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

   


@endsection













