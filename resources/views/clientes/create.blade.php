@extends('layouts.app')

@section('content')

    <script>
      $(document).ready(function() {
        $(function() {jQuery("#tab a:first").tab("show"); });
      });
    </script>

    <div class="container">
        <div class="card">
        <div class="card-body">
            <nav class="navbar navbar-inverse">
            <!-- Menu cabeçalho -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="{{ URL::to('clientes') }}">Lista</a>
                    <a href="{{ URL::to('clientes/create') }}">Novo</a>
                </div>
            </nav>

            <div class="card-title">CADASTRO DE CLIENTES</div>

            <!-- se houver erros, serão mostrados -->
            {{ Html::ul($errors->all()) }}
            {{ Form::open(['route' => 'clientes.store', 'method' => 'post']) }}    
                <div class="form-group">
                    <div class="tab-custom">
                        <ul class="nav nav-tabs" id="tab">
                            <li class="nav-item">
                                <a  class="nav-link active" href="#tabGeral"  aria-expanded="true" data-toggle="tab">
                                    <span class="hidden-xs">Geral</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#tabContato"  aria-expanded="false" data-toggle="tab">
                                    <span class="hidden-xs">Contato</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#tabLocalizacao"  aria-expanded="false" data-toggle="tab">
                                    <span class="hidden-xs">Localização</span>
                                </a>
                            </li>
                        </ul>

                        <div class="tab-content" id="tabContent">
                            <div id="tabGeral" class="tab-pane fade show active" aria-expanded="true">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label class="control-label">Nome*</label>
                                        <input type="text" class="form-control" id="cli_nome" name="cli_nome">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-3 col-xs-6">
                                            <label for="cli_cpfcnpj">CPF</label>
                                            <input type="text" class="form-control" id="cli_cpfcnpj" maxlength="14" autocomplete="off" name="cli_cpfcnpj">
                                        </div>
                                        <div class="col-sm-3 col-xs-6">
                                            <label for="" class="control-label">RG</label>
                                            <input type="text" class="form-control" id="cli_rg" name="cli_rg" maxlength="15" name="cli_rg">
                                        </div>
                                        <div class="col-sm-3 col-xs-6">
                                            <label for="cli_nascimento" class="control-label">Data de nascimento</label>
                                            <input type="text" class="form-control datepicker data" id="cli_nascimento" data-provide="datepicker" maxlength="10" placeholder="dd/mm/aaaa" name="cli_nascimento"> 
                                        </div>
                                    </div>
                                </div>
                              </div>
                        <div class="tab-pane fade show" id="tabContato">
                            <div class="row">
                                <div class="col-sm-12" id="divContato">
                                    <table class="table" id="tableContatos">
                                        <tbody id="tbodyTelefones">
                                            <tr class="colTelefone">
                                                <td class="form-group">
                                                    Celular
                                                </td>
                                                <td class="tdContato">
                                                    <input type="text" class="col-sm-12 form-control" id="cli_telefone" maxlength="15" placeholder="Número" aria-required="true" name="cli_telefone">
                                                </td>
                                                <td class="tdOperadora">

                                                </td>
                                                <td class="tdObservacoes">

                                                </td>
                                            </tr>
                                            <tr class="colTelefone">
                                                <td class="form-group">
                                                    E-mail
                                                </td>
                                                <td class="tdContato">
                                                    <input type="text" class="col-sm-12 form-control" id="cli_email" maxlength="50" placeholder="E-mail" aria-required="true" name="cli_email">
                                                </td>
                                                <td class="tdObservacoes">

                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tabLocalizacao">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label for="cli_endereco" class="control-label">Endereço</label>
                                        <input type="text" class="form-control" id="cli_endereco" name="cli_endereco" maxlength="150">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-3 col-xs-6">
                                        <label for="cli_bairro" class="control-label">Bairro</label>
                                        <input type="text" class="form-control" id="cli_bairro" name="cli_bairro" maxlength="150">
                                    </div>

                                    <div class="col-sm-3 col-xs-6">
                                        <label for="cli_cep" class="control-label" id="cli_cep">CEP</label>
                                        <input type="text" class="form-control" id="cli_cep" name="cli_cep" maxlength="10">
                                    </div>

                                    <div class="col-sm-3 col-xs-6">
                                        <label for="cli_cidade" class="control-label" id="cli_cidade">Cidade</label>
                                        <input type="text" class="form-control" id="cli_cidade" name="cli_cidade" maxlength="150">
                                    </div>

                                    <div class="col-sm-3 col-xs-6">
                                        <label for="cli_estado" class="control-label">Estado</label>
                                        {{Form::select('cli_estado', ['Selecione um estado','Acre','Alagoas','Amazonas','Amapá','Bahia','Ceará','Distrito Federal','Espírito Santo','Goiás','Maranhão','Minas Gerais','Mato Grosso do Sul','Mato Grosso','Pará','Paraíba','Pernambuco','Piauí','Paraná','Rio de Janeiro','Rio Grande do Norte','Rondônia','Roraima','Rio Grande do Sul','Santa Catarina','São Paulo','Sergipe','Tocantins'],null, array('class' => 'form-control','name' => 'cli_estado') )}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <input type="hidden" id="usu_id" name="usu_id" value="{{Auth::user()->id}}">
            <input type="hidden" id="emp_id" name="emp_id" value="{{Auth::user()->emp_id}}">
            {{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}
            {{ Form::close() }}
        </div>
        </div>
    </div>
@endsection

<script type="text/javascript">
$(document).ready(function(){
  $('.data').mask('00/00/0000');
});
</script>