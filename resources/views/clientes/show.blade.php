@extends('layouts.app')

@section('content')
<div class="container">

<nav class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('clientes') }}">Lista</a>
        <a href="{{ URL::to('clientes/create') }}">Novo</a>
    </div>
</nav>      

      <h3>{{$Clientes->cli_id}} - {{$Clientes->cli_nome}}</h3>

    <div class="jumbotron text-center">
        <p>
            <strong>ID:</strong> {{$Clientes->cli_id}}<br>
            <strong>Nome:</strong> {{$Clientes->cli_nome}}<br>
            <strong>CPF/CNPJ:</strong> {{cpf(cnpj($Clientes->cli_cpfcnpj))}}<br>
            <strong>E-mail:</strong> {{$Clientes->cli_email}}<br>
            <strong>Telefone:</strong> {{fone($Clientes->cli_telefone)}}
        </p>
    </div>
</div>
@endsection