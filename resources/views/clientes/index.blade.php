@extends('layouts.app')

@section('content')

<div class="container">    
    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('clientes') }}">Lista</a>
            <a href="{{ URL::to('clientes/create') }}">Novo</a>
        </div>
    </nav> 

    <div class="card">
        <div class="card-body">
            <div class="card-title">CLIENTES</div>
            <!-- Mensagem de retorno do Cadastro -->
            @if (Session::has('message'))
                <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif
            {{ $Clientes->links() }}
            <div class="card-body clientes-lista">
                <table class="table">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>E-mail</th>
                        <th>Telefone</th>
                        <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                      {{--  MOSTRA UM ALERTA SE NÃO HOUVER CLIENTES NO BANCO DE DADOS  --}}                      
                      @if(isset($Clientes))                            
                            @foreach($Clientes as $row)  
                                <tr>
                                    <td class="tbindex"><span class="small">{{$row->cli_id}}</span></td>
                                    <td class="tbindex"><span class="small">{{$row->cli_nome}}</span></td>
                                    <td class="tbindex"><span class="small">{{$row->cli_email}}</span></td>
                                    <td class="tbindex"><span class="small">{{fone($row->cli_telefone)}}</span></td>
                                    <!-- Botões Exibir, Alterar e Excluir -->
                                    <td>
                                        <a class="btn btn-small btn-success" href="{{ URL::to('clientes/' . $row->cli_id) }}">V</a>
                                        <a class="btn btn-small btn-info" href="{{ URL::to('clientes/' . $row->cli_id . '/edit') }}">A</a>
                                        <div class="btn">
                                        {{ Form::open(array('url' => 'clientes/' . $row->cli_id, 'class' => 'pull-right')) }}
                                        {{ Form::hidden('_method', 'DELETE') }}
                                        {{ Form::submit('E', array('class' => 'btn btn-warning')) }}
                                        {{ Form::close() }}
                                        </div>
                                    </td>
                                </tr>
                            @endforeach                               
                      @else
                        <tr>
                            <td colspan="5"><div class="alert alert-default text-center">Não foram encontrado clientes no banco de dados</div></td>
                        </tr>

                      @endif
                    </tbody>
                </table>
                {{ $Clientes->links() }}
            </div>
        </div>
    </div>    
</div>
@endsection