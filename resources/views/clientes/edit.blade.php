@extends('layouts.app')

@section('content')
    <script>
      $(document).ready(function() {
        $(function() {jQuery("#tab a:first").tab("show"); });

        // POST JSON

        $(document).on('submit','#form_cli', function(e) {
            var data = $("#reg-form").serialize();
            e.preventDefault();
                $.ajax({
                    type: 'POST',
                    url: '{{ url("api/clientes/salvarjson") }}',
                    data: data,
                    success: function(data) {
                        alert("Post bem sucedido");
                        console.log(data);
                    },

                    error: function(data) {
                        alert("error");
                    }
                });
                return false;
      });

      });
                            
                                    
    </script>
<div class="container">
    <div class="card">
        <div class="card-body">

            <nav class="navbar navbar-inverse">
                <div class="navbar-header">
                    <a class="navbar-brand" href="{{ URL::to('clientes') }}">Lista</a>
                </div>
            </nav>

            <div class="card-title"><h1>Alterar {{$Clientes->cli_nome}}</h1></div>

            <!-- se houver erros, serão mostrados -->
            {{ Html::ul($errors->all()) }}
            {{ Form::model($Clientes, array('route' => array('clientes.update', $Clientes->cli_id), 'method' => 'PUT')) }} 
                <div class="form-group">
                    <div class="tab-custom">
                        <ul class="nav nav-tabs" id="tab">

                            <li class="nav-item">
                                <a  class="nav-link active" href="#tabGeral"  aria-expanded="true" data-toggle="tab">
                                    <span class="hidden-xs">Geral</span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="#tabContato"  aria-expanded="false" data-toggle="tab">
                                    <span class="hidden-xs">Contato</span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="#tabLocalizacao"  aria-expanded="false" data-toggle="tab">
                                    <span class="hidden-xs">Localização</span>
                                </a>
                            </li>
                        </ul>

                        <div class="tab-content" id="tabContent">
                            <div class="tab-pane fade show active" id="tabGeral">
                                <div class="row">
                                    <div class="col-sm-12">
                                        {{ Form::label('cli_nome', 'Nome') }} *
                                        {{ Form::text('cli_nome', null, array('class' => 'form-control')) }}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-3 col-xs-6">
                                            {{ Form::label('cli_cpfcnpj', 'CPF') }}
                                            {{ Form::text('cli_cpfcnpj', null, array('class' => 'form-control')) }}
                                        </div>
                                        <div class="col-sm-3 col-xs-6">
                                            {{ Form::label('cli_rg', 'RG') }}
                                            {{ Form::text('cli_rg', null, array('class' => 'form-control')) }}

                                        </div>
                                        <div class="col-sm-3 col-xs-6">
                                            <label for="cli_nascimento" class="control-label">Data de nascimento</label>
                                            <input type="text" class="form-control" id="cli_nascimento" maxlength="14" placeholder="dd/mm/aaaa" name="cli_nascimento" value="{{date('d/m/Y', strtotime($Clientes->cli_nascimento))}}">
                                        </div>
                                    </div>
                                </div>
                              </div>
                                <div class="tab-pane fade show" id="tabContato">
                                    <div class="row">
                                        <div class="col-sm-12" id="divContato">
                                            <table class="table" id="tableContatos">
                                                <tbody id="tbodyTelefones">
                                                    <tr class="colTelefone">
                                                        <td class="form-group">
                                                            {{ Form::label('cli_telefone', 'Celular') }}
                                                        </td>

                                                        <td class="tdContato">                                                    
                                                            {{ Form::text('cli_telefone', null, array('class' => 'form-control')) }}
                                                        </td>

                                                        <td class="tdOperadora">

                                                        </td>

                                                        <td class="tdObservacoes">

                                                        </td>

                                                    </tr>

                                                    <tr class="colTelefone">
                                                        <td class="form-group">
                                                            {{ Form::label('cli_email', 'E-mail') }}
                                                        </td>

                                                        <td class="tdContato">
                                                            {{ Form::text('cli_email', $Clientes->cli_email, array('class' => 'form-control')) }}
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                  </div>
                                    <div class="tab-pane" id="tabLocalizacao">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    {{ Form::label('cli_endereco', 'Endereço') }}
                                                    {{ Form::text('cli_endereco', null, array('class' => 'form-control')) }}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-3 col-xs-6">
                                                    {{ Form::label('cli_bairro', 'Bairro') }}
                                                    {{ Form::text('cli_bairro', null, array('class' => 'form-control')) }}
                                                </div>

                                                <div class="col-sm-3 col-xs-6">
                                                    {{ Form::label('cli_cep', 'CEP') }}
                                                    {{ Form::text('cli_cep', null, array('class' => 'form-control')) }}
                                                </div>

                                                <div class="col-sm-3 col-xs-6">
                                                    {{ Form::label('cli_cidade', 'Cidade') }}
                                                    {{ Form::text('cli_cidade', null, array('class' => 'form-control')) }}
                                                </div>

                                                <div class="col-sm-3 col-xs-6">
                                                    {{ Form::label('cli_estado', 'Estado') }}
                                                    {{ Form::text('cli_estado', null, array('class' => 'form-control')) }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}  
                    </div>
                </div>
                        
        </div>
    </div>
    {{ Form::close() }}
</div>
@endsection