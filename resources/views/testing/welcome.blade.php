<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Title</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="stylesheet" href="css/styles.css">
</head>
<body>

<div class="container">
    <div class="row">
        <div class="buttons">
            <a href="#" class="btn" hidefocus="true">
                <span>Botão</span>
            </a>
            <a href="#" class="btn btn-middle">
                <span>Médio</span>
            </a>
            <a href="#" class="btn btn-small">
                <span>Pequeno</span>
            </a>
            <a href="#" class="btn btn-large">
                <span>Grande</span>
            </a>
            <a href="#" class="btn btn-round">
                <span>Aredondado</span>
            </a>
            <a href="#" class="btn btn-green">
                <span>Botão verde</span>
            </a>
            <a href="#" class="btn btn-blue">
                <span>Botão azul</span>
            </a>
        </div>
        <div class="panel">
            <div class="inner">
                <span>Olá Mundo</span>
            </div>
        </div>

        <div class="tabs-framed">
            <div class="inner">
                <ul class="tabs">
                    <li class=""><a href="#tab1" data-toggle="tab">Aba1</a></li>
                    <li class=""><a href="#tab2" data-toggle="tab">Aba2</a></li>
                    <li class=""><a href="#tab3" data-toggle="tab">Aba3</a></li>
                </ul>

                <div class="tabs-content panel">
                    <div class="tab-pane" id="tab1">
                        <h1>Tab One</h1>
                    </div>
                    <div class="tab-pane" id="tab2">
                        <h1>Tab Two</h1>
                    </div>
                    <div class="tab-pane" id="tab3">
                        <h1>Tab Three</h1>
                    </div>
                </div>
            </div>
        </div>


        <div class="sidebar">
            <div class="panel sidebar-categories">
                <div class="inner">
                    <ul>
                        <li class="current-sidebar-link"><a href="{{ config('app.URL') }}/clientes"><i class="fa fa-calendar"></i>Agenda</a></li>
                        <li><a href="#"><i class="fa fa-user"></i>Clientes</a></li>
                        <li><a href="#"><i class="fa fa-graduation-cap"></i>Profissionais</a></li>
                        <li><a href="#"><i class="fa fa-troph"></i>Produtos/Serviços</a></li>
                        <li><a href="#"><i class="fa fa-shopping-cart"></i>Comandas</a></li>
                        <li><a href="#"><i class="fa fa-money"></i>Comissões</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</div>
  
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
</body>
</html>