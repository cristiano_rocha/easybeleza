@extends('layouts.app')

@section('content')

    <div class="container">    	
        <div class="card">
        	<div class="card-body">
	            <nav class="navbar navbar-inverse">
	                <div class="navbar-header">
	                    <a class="navbar-brand" href="{{ redirect()->getUrlGenerator()->previous() }}">Voltar</a>
	                </div>
	            </nav>

	            <div class="card-title">Associar comissão do Produto/Serviço {{ $prosrv_id }}</div>

                <!-- se houver erros, serão mostrados -->
	            {{ Html::ul($errors->all()) }}                
	            {{ Form::open(['route' => ['produtosservicos.produtosprofissionais.store', $prosrv_id], 'method' => 'post']) }}
                <div class="form-group">
                	<div class="row">
                        <div class="col-sm-12">
                            {{ Form::label('prof_id', 'Profissional') }} *
                            <select class="form-control" name="prof_id" id="prof_id">
                                @foreach ($jprof as $row)
                                <option value="{{ $row->prof_id }}">{{ $row->prof_nome }}</option>
                                @endforeach
                            </select>     
                        </div>
                        <div class="col-sm-2">
                            {{ Form::label('proprof_pcomissao', 'Comissão %') }} *
                            {{ Form::text('proprof_pcomissao', '0,00', array('id' => 'proprof_pcomissao', 'class' => 'form-control', 'placeholder'=>'0,00', 'style'=>'display: inline-block; text-align: right;')) }}
                        </div>
                    </div>
                </div>
                {{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}
                {{ Form::close() }}
        	</div>
        </div>
    </div>

@endsection