<?php
    setcookie('emp_id');
    setcookie('emp_id' , '0');
?>
<!doctype html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <title>{{ config('app.name') }} - {{ config('app.subtitle') }}</title>

    <!-- Ícone browser -->
    <link href="https://app.easybeleza.net.br/img/logo-x-easybeleza.png" rel="icon" />

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta informações CEO -->
    <meta name="description" content="Programa para salão de beleza, sistema de gestão e marketing, seu salão de beleza na Web. Agendamento online e muito mais.">
    <meta name="keywords" content= "programa para salao, programa para salão, controle de estoque, salão de beleza online, como abrir um salão, programa para salao, programa para salão, salao de beleza online, sistema de salao de beleza, sistema para salão, software para salao, software para salão, sistema salao, sistema de salao, sistema salão, salao de beleza na web, software livre, sistema de gestão" />
    <meta name="robots" content="index,follow">
    <meta name="author" content="RochaNet Informática - SiD Soluções">
    <meta http-equiv="content-language" content="pt-BR">
    <meta name="googlebot" content="index,follow">
    <meta http-equiv="cache-control" content="max-age">

    <meta property="og:title" content="Programa para Salão de Beleza Completo - EasyBeleza">
    <meta property="og:description" content="Programa para salão de beleza com agendamento on-line, gestão de agenda, cadastro de clientes, controle financeiro, controle de estoque">
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="EasyBeleza">
    <meta property="og:url" content="https://app.easybeleza.net.br">
    <meta property="og:image" content="https://app.easybeleza.net.br/img/logo-easybeleza.png">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link rel="stylesheet" href="{{ URL::to('css/app.css') }}">
  </head>
  <body>
      <nav class="navbar navbar-expand-lg navbar-inverse fixed-top">
        <a href="#" class="navbar-brand">Easy Beleza</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarAlt">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse  float-lg-right" id="navbarAlt">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a href="#loginModal" class="nav-link" role="button" data-target="#loginModal" data-toggle="modal"><i class="fa fa-cog"></i> Painel de Controle</a>
                </li>

                <li class="nav-item">
                    <a href="https://sidsolucoes.com.br" class="nav-link">Sobre nós</a>
                </li>
                
                <li class="nav-item">
                    <a href="#" class="nav-link">Serviços</a>
                </li>

                <li class="nav-item">
                    <a href="#" class="nav-link">Contato</a>
                </li>
            </ul>
        </div>
      </nav>
    
    <div id="loginModal" class="modal" data-easein="fadeIn" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalTitle">Iniciar sessão</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                </div>
                <div class="modal-body">
                    {{ Html::ul($errors->all() )}}
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                    <div class="row">
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-12 control-label">E-Mail:</label>

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-12 control-label">Senha</label>

                            <div class="col-md-10">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link-primary" href="{{ route('password.request') }}">
                                    Esqueceu sua senha?
                                </a>
                            </div>
                        </div>
                    </div>                    
                    {{ Form::close() }}
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>


      <header id="home-section">
        <div class="dark-overlay">
            <div class="home-inner">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col text-center">
                            <h1 class="display-2 h1 hero-brand">Easy <span>Beleza</span></h1>
                            <div class="hero-subtitle mt-3">Sistema de gerenciamento</div>
                            <a href="#" class="btn btn-outline-secondary mt-2">Saiba mais</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </header>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    {{--  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>  --}}
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    <script type="text/javascript">

    $(document).ready(function($) {
        $('#login').click(function(event) {
            event.preventDefault();
                postLogin();
        })
    });

        function postLogin() {
            formData = $('#formData').serializeArray();

            $.ajax({
                url: '{{ url('login') }}',
                type: 'POST',
                dataType: 'JSON',
                data: formData,
            })
            .done(function() {
                console.log("success");
                setTimeout(function(){$('#loginModal').modal('hide')},3000);
            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });

            
        }
    </script>
  </body>
</html>