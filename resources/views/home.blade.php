<?php
    setcookie('emp_id');
    $empresaid = Auth::user()->emp_id;
    setcookie('emp_id' , $empresaid);

?>

@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    Bem vindo!
                </div>
                <div class="panel-body">
                    <h2>Links provisórios</h2>
                    <a href="{{ URL::to('./usuarios') }}">Usuários</a>    
                    <a href="{{ URL::to('./profissionais') }}">| Profissionais</a>  
                    <a href="{{ URL::to('./clientes') }}">| Clientes</a>  
                    <a href="{{ URL::to('./produtos') }}">| Produtos</a>  
                    <a href="{{ URL::to('./agenda') }}">| Agendas</a>    
                    <a href="{{ URL::to('./comandas') }}">| Comandas</a>         
                    <a href="{{ URL::to('./comissoes') }}">| Comissões</a>           
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
