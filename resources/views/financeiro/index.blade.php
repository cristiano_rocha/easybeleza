@extends('layouts.app')

@section('content')
    <div class="container">
    <div id="section2" class="corpo metro-section tile-span-4">
        <a class="tile app bg-color-red" href="/financeiro/movcaixa/abrir" id="pedidos">
            <div class="image-wrapper">
                <img src="./img/menu/cadastros.svg">
                <div class="app-label">Abrir Caixa</div>
            </div>
        </a>
        <a class="tile app bg-color-red" href="/retirada" id="retirada">
            <div class="image-wrapper">
                <img src="./img/menu/retirada.svg">
                <div class="app-label">Retirada</div>
            </div>
        </a>
        <a class="tile app bg-color-red" href="/suplemento" id="suplemento">
            <div class="image-wrapper">
                <img src="./img/menu/suplemento.svg">
                <div class="app-label">Suplemento</div>
            </div>
        </a>
        <a class="tile app bg-color-red" href="/financeiro/movcaixa/fechar" id="financeiro">
            <div class="image-wrapper">
                <img src="./img/menu/financeiro.svg">
                <div class="app-label">Fechar Caixa</div>
            </div>
        </a>
        <a class="tile app bg-color-red" href="/financeiro/caixa" id="caixa">
            <div class="image-wrapper">
                <img src="./img/menu/caixa.svg">
                <div class="app-label">Tipo Caixa</div>
            </div>
        </a>
        <a class="tile app bg-color-red" href="/relatorios" id="relatorios">
            <div class="image-wrapper">
                <img src="./img/menu/relatorios.svg">
                <div class="app-label">Relatórios</div>
            </div>
        </a>
        <a class="tile app bg-color-red" href="/parametros" id="parametros">
            <div class="image-wrapper">
                <img src="./img/menu/parametros.svg">
                <div class="app-label">Parâmetros</div>
            </div>
        </a>
        <a class="tile app bg-color-red" href="/ajuda" id="ajuda">
            <div class="image-wrapper">
                <img src="./img/menu/ajuda.svg">
                <div class="app-label">Ajuda</div>
            </div>
        </a>
    </div>
</div>


@endsection
