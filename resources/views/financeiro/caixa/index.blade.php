@extends('layouts.app')

@section('content')
    <div class="container">
        <nav class="navbar navbar-inverse">
            <div class="navbar-header">
                <a href="{{ URL::to('financeiro/caixa') }}" class="navbar-brand">Lista</a>
                <a href="{{ URL::to('financeiro/caixa/create') }}">Novo</a>
            </div>
        </nav>
        <div class="card">
            <div class="card-body">
            <div class="card-title">Caixas</div>
            @if(Session::has('message'))
                <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif
            {{ $Caixa->links () }}
            {{Session::get('statuses')}}
                <table class="table" id="caixa-table">
                    <thead>
                        <tr>
                            <th>CAIXA</th>
                            <th>DESCRIÇÃO</th>
                            <th>STATUS</th>
                            <th>CRIADO POR</th>
                            <th>Ação</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($Caixa))
                            @foreach($Caixa as $row)
                                <tr>
                                    {{--  <td class="tbindex"><span class="small">{{$row->cx_id}}</span></td>  --}}
                                    <td class="tbindex"><span class="small">{{$row->cx_id}}</span></td>
                                    <td class="tbindex"><span class="small">{{$row->cx_descricao}}</span></td>
                                    <td class="tbindex"><span class="small">
                                    @if ($row->status == 1)
                                        Ativo
                                    @elseif ($row->status == 0)
                                        Inativo
                                    @endif
                                    </span></td>
                                    <td class="tbindex"><span class="small">{{$row->users->name}}</span></td>
                                    <td>
                                        <a href="{{ URL::to('financeiro/caixa/' . $row->cx_id) }}" class="btn btn-small btn-success">Exibir</a>
                                        <a href="{{ URL::to('financeiro/caixa/' . $row->cx_id . '/edit') }}" class="btn btn-small btn-info">Editar</a>
                                        <div class="btn">
                                            {{ Form::open(array('url' => 'financeiro/caixa/' . $row->cx_id, 'class' => 'pull-right')) }}
                                            {{ Form::hidden('_method', 'DELETE') }}
                                            {{ Form::submit('Excluir', array('class' => 'btn btn-warning')) }}
                                            {{ Form::close() }}
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="7"><div class="alert alert-default text-center">Não foram encontrados resultados</div></td>
                            </tr>
                        @endif
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection

