@extends('layouts.app')

@section('content')
<div class="container">

<nav class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('financeiro/caixa') }}">Lista</a>
        <a href="{{ URL::to('financeiro/caixa/create') }}">Novo</a>
    </div>
</nav>      

      <h3>{{$Caixa->cx_id}}</h3>


    <div class="jumbotron text-center">
        <p>
            <strong>ID:</strong> {{$Caixa->cx_id}}<br>
            <strong>Descrição:</strong> {{$Caixa->cx_descricao}}<br>
            <strong>Status:</strong> {{$Caixa->status}}<br>
            <strong>Criado por:</strong> {{$Caixa->usu_id}}<br>
        </p>
    </div>
</div>
@endsection