@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <nav class="navbar navbar-inverse">
                <div class="navbar-header">
                    <a href="{{ redirect()->getUrlGenerator()->previous() }}" class="navbar-brand">Voltar</a>
                </div>
            </nav>

            <div class="card-title h1">Cadastrar caixa</div>

            {{ Html::ul($errors->all() )}}
            {{ Form::open(['route' => ['caixa.store'], 'method' => 'post']) }}
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-7">
                        {{ Form::label('cx_descricao', 'Descrição') }}*
                        {{Form::text('cx_descricao', null, array('id' => 'cx_descricao', 'class' => 'form-control', 'placeholder' => 'Descrição'))}}
                    </div>

                    <div class="col-sm-12 clear"><BR /></div>
                    <div class="col-sm-3 checkbox">
                        <label>
                            Ativo 
                        </label>
                        <input data-toggle="toggle" id="status" name="status" type="checkbox" checked>
                    </div>
                </div>
            </div>
            <input type="hidden" id="usu_id" name="usu_id" value="{{Auth::user()->id}}">
            <input type="hidden" id="emp_id" name="emp_id" value="{{Auth::user()->emp_id }}">
            {{Form::submit('Salvar', array('class' => 'btn btn-primary'))}}
            {{Form::close()}}
        </div>
    </div>
</div>
@endsection