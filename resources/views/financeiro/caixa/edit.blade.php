@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <nav class="navbar navbar-inverse">
                <div class="navbar-header">
                    <a href="{{ redirect()->getUrlGenerator()->previous() }}" class="navbar-brand">Voltar</a>
                </div>
            </nav>

            <div class="card-title h1">Atualizar Caixa: {{$Caixa->cx_id}}</div>

            {{ Html::ul($errors->all() )}}
            {{ Form::model($Caixa, array('route' => array('caixa.update', $Caixa->cx_id), 'method' => 'PUT')) }}
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-7">
                        {{ Form::label('cx_descricao', 'Descrição') }}*
                        {{Form::text('cx_descricao', null, array('id' => 'cx_descricao', 'class' => 'form-control', 'placeholder' => 'Descrição')) }}
                    </div>
                    <div class="col-sm-12 clear"><BR /></div>
                    <div class="col-sm-3 checkbox">
                        <label>
                            Ativo 
                        </label>
                        <input data-toggle="toggle" id="status" name="status" type="checkbox" {{ ($Caixa->status == 1) ? "checked" : ""}}>
                    </div>
                </div>
            </div>
            <input type="hidden" id="usu_id" name="usu_id" value="{{Auth::user()->id}}">
            {{Form::submit('Salvar', array('class' => 'btn btn-primary'))}}
        </div>
    </div>
</div>
<script>
  $(function() {
    $('#status').bootstrapToggle();
  })
</script>
@endsection