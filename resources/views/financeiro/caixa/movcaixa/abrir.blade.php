@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-body">
            <nav class="navbar navbar-inverse">
                <div class="navbar-header">
                    <a href="{{ URL::to('financeiro/movcaixa') }}" class="navbar-brand">Voltar</a>
                </div>
            </nav>

            <div class="card-title">Abrir caixa</div>

            {{ Html::ul($errors->all()) }}
            {{ Form::open(['route' => ['movcaixa.abertura'], 'method' => 'post']) }}
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-3">
                        <select name="cx_id" id="cx_id" class="form-control">
                            <option value="0" selected="selected">Selecione o Caixa</option>
                            @foreach($Caixas as $row)
                                <option value="{{ $row->cx_id }}">{{$row->cx_descricao}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-2">
                        {{ Form::label('cx_saldo_ini', 'Saldo Inicial') }}
                        {{ Form::text('cx_saldo_ini', null, array('id' => 'cx_saldo_ini', 'class' => 'form-control', 'placeholder'=>'0,00')) }}
                    </div>
                </div>
            </div>

            <input type="hidden" id="usu_id" name="usu_id" value="{{ Auth::user()->id }}">
            <input type="hidden" id="emp_id" name="emp_id" value="{{Auth::user()->emp_id }}">
            {{ Form::submit('Abrir', array('class' => 'btn btn-primary')) }}
            {{ Form::close() }}

        </div>
    </div>
@endsection