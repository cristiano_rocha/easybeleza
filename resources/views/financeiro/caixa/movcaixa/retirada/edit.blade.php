@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <nav class="vanvbar navbar-header">
                <a href="{{ redirect()->getUrlGenerator()->previous() }}" class="navbar-brand">Voltar</a>
            </nav>

            <div class="card-title h1">Alterar Retirada</div>

            {{ Html::ul($errors->all() )}}
            {{ Form::model($Retirada, array('route' => array('retirada.update', $Retirada->re_id), 'method' => 'PUT')) }}
            <div class="form-group">
                <div class="row">

                    <div class="col-sm-2">
                        {{ Form::label('cx_id') }}
                        <select name="cx_id" id="cx_id" class="form-control">
                        @foreach($Caixa as $row)
                            <option value="{{ $row->cx_id }}"> {{ $row->cx_descricao}} </option>
                        @endforeach
                        </select>
                    </div>

                    <div class="col-sm-2">
                        {{ Form::label('re_valor', 'Valor') }}
                        {{ Form::text('re_valor', null, array('id' => 're_valor', 'class' => 'form-control')) }}
                    </div>

                    <div class="col-sm-8">
                        {{ Form::label('re_descricao', 'Descrição') }}
                        {{ Form::text('re_descricao', null, array('id' => 're_valor', 'class' => 'form-control' )) }}
                    </div>
                </div>
            </div>
            <input type="hidden" id="usu_id" name="usu_id" value="{{ Auth::user()->id }}">
            <input type="hidden" id="emp_id" name="emp_id" value="{{ Auth::user()->emp_id }}">
            {{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection