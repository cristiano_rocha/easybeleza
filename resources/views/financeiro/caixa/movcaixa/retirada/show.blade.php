@extends('layouts.app')

@section('content')
<div class="container">

<nav class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('retirada') }}">Lista</a>
        <a href="{{ URL::to('retirada/create') }}">Novo</a>
    </div>
</nav>      

      <h3>{{$Retirada->re_id}}</h3>


    <div class="jumbotron text-center">
        <p>
            <strong>ID:</strong> {{$Retirada->re_id}}<br>
            <strong>Descrição:</strong> {{$Retirada->re_descricao}}<br>
            <strong>Valor:</strong> {{$Retirada->re_valor}}<br>
            <strong>Criado por:</strong> {{$Retirada->usu_id}}<br>
        </p>
    </div>
</div>
@endsection