@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <nav class="vanvbar navbar-header">
                <a href="{{ redirect()->getUrlGenerator()->previous() }}" class="navbar-brand">Voltar</a>
            </nav>

            <div class="card-title h1">Alterar Suplemento</div>

            {{ Html::ul($errors->all() )}}
            {{ Form::model($Suplemento, array('route' => array('suplemento.update', $Suplemento->supl_id), 'method' => 'PUT')) }}
            <div class="form-group">
                <div class="row">

                    <div class="col-sm-2">
                        {{ Form::label('cx_id') }}
                        <select name="cx_id" id="cx_id" class="form-control">
                        @foreach($Caixa as $row)
                            <option value="{{ $row->cx_id }}"> {{ $row->cx_descricao}} </option>
                        @endforeach
                        </select>
                    </div>

                    <div class="col-sm-2">
                        {{ Form::label('supl_valor', 'Valor') }}
                        {{ Form::text('supl_valor', null, array('id' => 'supl_valor', 'class' => 'form-control')) }}
                    </div>

                    <div class="col-sm-8">
                        {{ Form::label('supl_descricao', 'Descrição') }}
                        {{ Form::text('supl_descricao', null, array('id' => 'supl_valor', 'class' => 'form-control' )) }}
                    </div>
                </div>
            </div>
            <input type="hidden" id="usu_id" name="usu_id" value="{{ Auth::user()->id }}">
            <input type="hidden" id="emp_id" name="emp_id" value="{{ Auth::user()->emp_id }}">
            {{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection