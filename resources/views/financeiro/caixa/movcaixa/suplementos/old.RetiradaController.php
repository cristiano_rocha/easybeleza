<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\Retirada;
use App\Caixa;
use Illuminate\Support\Facades\Redirect;

class RetiradaController extends Controller
{

    public function __constructor(Retirada $Retirada)
    {
        $this->Retirada = $Retirada;
        $this->middleware('CheckUserCaixaStatus');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Retirada = Retirada::all();
        
        return view('financeiro.caixa.movcaixa.retirada.index')->with('Retirada', $Retirada);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Caixa = Caixa::all();
        return view('financeiro.caixa.movcaixa.retirada.create')->with('Caixa', $Caixa);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Retirada = New Retirada;
        $MovCaixa  = parent::TemCXAberto();

        $Retirada->re_valor          = $request->re_valor;
        $Retirada->re_descricao      = $request->re_descricao;
        $Retirada->mov_id            = $MovCaixa->mov_id;
        $Retirada->usu_id            = Auth::user()->id;
        $Retirada->emp_id            = Auth::user()->emp_id;
        $Retirada->save();

        Session::flash('message','Retirada criado com sucesso');
        return redirect('/retirada');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($re_id)
    {
        $Retirada = Retirada::where('re_id', $re_id)->first();
        return view('financeiro.caixa.movcaixa.retirada.show')->with('Retirada', $Retirada);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($re_id)
    {
        $Retirada = Retirada::findOrfail($re_id);
        $Caixa = Caixa::all();
        return view('financeiro.caixa.movcaixa.retirada.edit')->with('Caixa', $Caixa)->with('Retirada',$Retirada);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $re_id)
    {
        $Retirada = Retirada::findOrfail($re_id);

        $Retirada->re_valor          = $request->re_valor;
        $Retirada->re_descricao      = $request->re_descricao;
        $Retirada->usu_id            = Auth::user()->id;
        $Retirada->emp_id            = Auth::user()->emp_id;
        $Retirada->save();

        Session::flash('message', 'Retirada alterada com sucesso');
        return redirect('retirada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($re_id)
    {
        $Retirada = Retirada::find($re_id);
        $Retirada->delete('re_id');

        Session::flash('message',' Retirada excluída!');
        return Redirect::to('retirada');
    }
}
