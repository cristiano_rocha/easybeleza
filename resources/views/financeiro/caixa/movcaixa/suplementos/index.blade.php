@extends('layouts.app')

@section('content')

    <nav class="navbar navvbar-inverse">
        <div class="navbar-header">
            <a href="{{ URL::to('/suplemento') }}" class="navbar-brand">Lista</a>
            <a href="{{ URL::to('/suplemento/create') }}">Novo</a>
        </div>
    </nav>

    <div class="card">
        <div class="card-body">
            <div class="card-title">Suplemento</div>
            @if(Session::has('message'))
                <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif

            {{--  {{ $suplemento->links() }}  --}}
            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>MOVIMENTO</th>
                        <th>DESCRIÇÃO</th>
                        <th>VALOR</th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($Suplemento))
                        @foreach($Suplemento as $row)
                            <tr>
                                <td class="tbindex"><span class="small">{{ $row->supl_id }}</span></td>
                                <td class="tbindex"><span class="small">{{ $row->mov_id }}</span></td>
                                <td class="tbindex"><span class="small">{{ $row->supl_descricao }}</span></td>
                                <td class="tbindex"><span class="small">{{ $row->supl_valor}}</span></td>
                                <td>
                                    <a href="{{URL::to('suplemento/' . $row->supl_id )}}" class="btn btn-small btn-success">Exibir</a>
                                    <a href="{{URL::to('suplemento/' . $row->supl_id . '/edit' )}}" class="btn btn-small btn-info">Alterar</a>
                                    <div class="btn">
                                        {{ Form::open(array('url' => 'suplemento/' . $row->supl_id, 'class' => 'pull-right')) }}
                                        {{ Form::hidden('_method', 'DELETE') }}
                                        {{ Form::submit('Excluir', array('class' => 'btn btn-small btn-warning')) }}
                                        {{ Form::close() }}
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="6"><div class="alert alert-default text-center">Não foram encontrado resultados!</div></td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>


@endsection