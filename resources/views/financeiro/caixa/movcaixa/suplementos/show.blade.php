@extends('layouts.app')

@section('content')
<div class="container">

<nav class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('suplementos') }}">Lista</a>
        <a href="{{ URL::to('suplemento/create') }}">Novo</a>
    </div>
</nav>      

      <h3>{{$Suplemento->supl_id}}</h3>


    <div class="jumbotron text-center">
        <p>
            <strong>ID:</strong> {{$Suplemento->supl_id}}<br>
            <strong>Descrição:</strong> {{$Suplemento->supl_descricao}}<br>
            <strong>Valor:</strong> {{$Suplemento->supl_valor}}<br>
            <strong>Criado por:</strong> {{$Suplemento->usu_id}}<br>
        </p>
    </div>
</div>
@endsection