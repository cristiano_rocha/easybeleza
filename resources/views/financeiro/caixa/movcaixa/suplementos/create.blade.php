@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <nav class="vanvbar navbar-header">
                <a href="{{ redirect()->getUrlGenerator()->previous() }}" class="navbar-brand">Voltar</a>
            </nav>

            <div class="card-title h1">Suplemento</div>

            {{ Html::ul($errors->all() )}}
            {{ Form::open(['route' => ['suplemento.store'], 'method' => 'post']) }}
            <div class="form-group">
                <div class="row">

                    <div class="col-sm-2">
                        {{ Form::label('cx_id','Caixa') }}
                        <select name="cx_id" id="cx_id" class="form-control">
                        <option selected="selected">Selecione o Caixa</option>
                        @foreach($Caixa as $row)
                            <option value="{{ $row->cx_id }}"> {{ $row->cx_descricao}} </option>
                        @endforeach
                        </select>
                    </div>

                    <div class="col-sm-2">
                        {{ Form::label('supl_valor', 'Valor R$') }}
                        {{ Form::text('supl_valor', '1,00', array('id' => 'supl_valor', 'class' => 'form-control numero')) }}
                    </div>

                    <div class="col-sm-8">
                        {{ Form::label('supl_descricao', 'Descrição') }}
                        {{ Form::text('supl_descricao', null, array('id' => 'supl_valor', 'class' => 'form-control' )) }}
                    </div>
                </div>
            </div>
            <input type="hidden" id="usu_id" name="usu_id" value="{{ Auth::user()->id }}">
            <input type="hidden" id="emp_id" name="emp_id" value="{{ Auth::user()->emp_id }}">
            {{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection