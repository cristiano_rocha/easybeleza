@extends('layouts.app')

@section('content')

    <nav class="navbar navvbar-inverse">
        <div class="navbar-header">
            <a href="{{ URL::to('financeiro/movcaixa') }}" class="navbar-brand">Lista</a>
            <a href="{{ URL::to('financeiro/movcaixa/abrir/') }}">Abrir</a>
        </div>
    </nav>

    <div class="card">
        <div class="card-body">
            <div class="card-title">Movimento</div>
            @if(Session::has('message'))
                <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif

            {{ $MovCaixa->links() }}
            <table class="table">
                <thead>
                    <tr>
                        <th>MOV</th>
                        <th>SALDO</th>
                        <th>DATA ABERTURA</th>
                        <th>DATA FECHAMENTO</th>
                        <th>ABERTO/FECHADO POR</th>
                        <th>AÇÃO</th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($MovCaixa))
                        @foreach($MovCaixa as $row)
                            <tr>
                                <td class="tbindex"><span class="small">{{ $row->movcx_id }}</span></td>
                                <td class="tbindex"><span class="small">{{ number_format($row->mov_saldo_ini, 2, ',', '.') }}</span></td>
                                <td class="tbindex"><span class="small">{{ date( 'd/m/Y' , strtotime($row->dt_aberto)) }}</span></td>
                                <td class="tbindex"><span class="small">{{ (isset($row->dt_fecha)) ? date( 'd/m/Y' , strtotime($row->dt_fecha)) : '' }}</span></td>
                                <td class="tbindex"><span class="small">{{ $row->users->name }}</span></td>
                                <td>
                                    <a href="{{URL::to('financeiro/movcaixa/' . $row->movcx_id )}}" class="btn btn-small btn-success">Exibir</a>
                                    <div class="btn">
                                        {{ Form::open(array('url' => 'financeiro/movcaixa/' . $row->movcx_id, 'class' => 'pull-right')) }}
                                        {{ Form::hidden('_method', 'DELETE') }}
                                        {{ Form::submit('Excluir', array('class' => 'btn btn-small btn-warning')) }}
                                        {{ Form::close() }}
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="6"><div class="alert alert-default text-center">Não foram encontrado resultados!</div></td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>


@endsection