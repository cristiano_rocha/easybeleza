@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-body">
            <nav class="navbar navbar-inverse">
                <div class="navbar-header">
                    <a href="{{ URL::to('financeiro/movcaixa') }}" class="navbar-brand">Voltar</a>
                </div>
            </nav>
          <div class="container">
            <div class="card-title col-md-12">
                <span></span>
                <h1><i class="fa fa-key"></i> Fechamento de Caixa</h1>
            </div>
            <div class="col-lg-2 col-md-5 col-sm-6 fechacx titulo">Caixa</div>
            <div class="col-lg-4 col-md-5 col-sm-6 fechacx cod">{{ str_pad($MovCaixa->cx_id, 5, '0', STR_PAD_LEFT) }}</div>
            <div class="col-lg-5 col-md-5 col-sm-6 fechacx desc">{{ $MovCaixa->Caixa->cx_descricao }}</div>
            
            <div class="col-lg-2 col-md-5 col-sm-6 fechacx titulo">Empresa</div>
            <div class="col-lg-4 col-md-5 col-sm-6 fechacx cod">{{ str_pad(Auth::user()->emp_id, 5, '0', STR_PAD_LEFT) }}</div>
            <div class="col-lg-5 col-md-5 col-sm-6 fechacx desc">{{ $Empresa->emp_nome }}</div>
            
            <div class="col-lg-2 col-md-5 col-sm-6 fechacx titulo">Operador</div>
            <div class="col-lg-4 col-md-5 col-sm-6 fechacx cod">{{ str_pad(Auth::user()->id, 5, '0', STR_PAD_LEFT) }}</div>
            <div class="col-lg-5 col-md-5 col-sm-6 fechacx desc">{{ Auth::user()->name }}</div>

            <div class="clear"></div>
          
            {{ Html::ul($errors->all()) }}
            {{ Form::open(['route' => ['movcaixa.fechamento'], 'method' => 'post','id' => 'frm-fecha-caixa']) }}
                <div class="form-group campos-fecha-caixa">
                    <div class="row">
                        <div class="col-sm-3">
                            {{ Form::label('mov_dinheiroinfo', 'Dinheiro' ) }}
                            {{ Form::text('mov_dinheiroinfo', null, array('class' => 'form-control', 'placeholder' => '0,00')) }}
                        </div>

                        <div class="col-sm-3">
                            {{ Form::label('mov_duplicatainfo', 'Duplicata' ) }}
                            {{ Form::text('mov_duplicatainfo', null, array('class' => 'form-control', 'placeholder' => '0,00')) }}
                        </div>

                        <div class="col-sm-3">
                            {{ Form::label('mov_chequeinfo', 'Cheque' ) }}
                            {{ Form::text('mov_chequeinfo', null, array('class' => 'form-control', 'placeholder' => '0,00')) }}
                        </div>

                        <div class="col-sm-3">
                            {{ Form::label('mov_cartaoinfo', 'Cartão' ) }}
                            {{ Form::text('mov_cartaoinfo', null, array('class' => 'form-control', 'placeholder' => '0,00')) }}
                        </div>
                    </div>
                </div>
            <input type="hidden" id="usu_id" name="usu_id" value="{{ Auth::user()->id }}">
            <input type="hidden" id="emp_id" name="emp_id" value="{{ Auth::user()->emp_id }}">
            <input type="hidden" id="usu_id_fechou" name="usu_id_fechou" value="{{ Auth::user()->usu_id }}">
           {{ Form::submit('Fechar', array('class' => 'btn btn-primary')) }} 
           {{ Form::close() }}
          </div>
        </div>
    </div>
@endsection