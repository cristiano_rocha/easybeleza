@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-body">
            <nav class="navbar navbar-inverse">
                <div class="navbar-header">
                    <a href="{{ URL::to('financeiro/movcaixa') }}" class="navbar-brand">Voltar</a>
                    <a href="{{ URL::to('financeiro/movcaixa/create') }}">Novo</a>
                </div>
            </nav>

            <div class="card-title">Nova Movimentação</div>

            {{ Html::ul($errors->all()) }}
            {{ Form::open(['route' => ['movcaixa.store'], 'method' => 'post']) }}
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-3">
                        {{ Form::label('cx_saldo_ini', 'Saldo Inicial') }}
                        {{ Form::text('cx_saldo_ini', null, array('id' => 'cx_saldo_ini', 'class' => 'form-control', 'placeholder' => 'Saldo Inicial')) }}
                    </div>
                    <div class="col-sm-3">
                        {{ Form::label('dt_aberto', 'Data Abertura') }}
                        {{ Form::text('dt_aberto', null, array('id' => 'dt_aberto', 'class' => 'form-control', 'placeholder' => 'Data abertura' )) }}
                    </div>
                    <div class="col-sm-3">
                        {{ Form::label('dt_fecha','Data Fechamento') }}
                        {{ Form::text('dt_fecha', null, array('id' => 'dt_fecha', 'class' => 'form-control', 'placeholder' => 'Data fechamento')) }}
                    </div>
                    <div class="col-sm-3">
                        {{ Form::label('cx_sangria', 'Sangria') }}
                        {{Form::text('cx_sangria', null, array('id' => 'cx_sangria', 'class' => 'form-control', 'placeholder' => 'Sangria')) }}
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-3">
                        {{ Form::label('cx_suplemento','Suplemento') }}
                        {{ Form::text('cx_suplemento', null, array('id' => 'cx_suplemento', 'class' => 'form-control', 'placeholder' => 'Suplemento')) }}
                    </div>
                    <div class="col-sm-3">
                        {{ Form::label('cx_totalvenda','Total Venda') }}
                        {{ Form::text('cx_totalvenda', null, array('id' => 'cx_totalvenda', 'class' => 'form-control', 'placeholder' => 'Total Venda')) }}
                    </div>
                    <div class="col-sm-3">
                        {{ Form::label('cx_pago', 'Pago') }}
                        {{ Form::text('cx_pago', null, array('id' => 'cx_pago', 'class' => 'form-control', 'placeholder' => 'Pago')) }}
                    </div>
                    <div class="col-sm-3">
                        {{ Form::label('cx_dinheiro', 'Dinheiro') }}
                        {{ Form::text('cx_dinheiro', null, array('id' => 'cx_dinheiro', 'class' => 'form-control', 'placeholder' => 'Dinheiro')) }}
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-3">
                        {{ Form::label('cx_duplicata', 'Duplicata') }}
                        {{ Form::text('cx_duplicata', null, array('id' => 'cx_duplicata', 'class' => 'form-control', 'placeholder' => 'Duplicata'))}}
                    </div>
                    <div class="col-sm-3">
                        {{ Form::label('cx_cheque','Cheque') }}
                        {{ Form::text('cx_cheque', null, array('id' => 'cx_cheque', 'class' => 'form-control', 'placeholder' => 'Cheque')) }}
                    </div>
                    <div class="col-sm-3">
                        {{ Form::label('cx_cartao', 'Cartão') }}
                        {{ Form::text('cx_cartao', null,  array('id' => 'cx_cartao', 'class' => 'form-control', 'placeholder' => 'Cartão')) }}
                    </div>
                    <div class="col-sm-3">
                        {{ Form::label('cx_desconto','Desconto') }}
                        {{ Form::text('cx_desconto', null, array('id' => 'cx_desconto', 'class' => 'form-control', 'placeholder' => 'Desconto')) }}
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        {{ Form::label('cx_totalliquido','Total Liquido') }}
                        {{ Form::text('cx_totalliquido', null, array('id' => 'cx_totalliquido', 'class' => 'form-control', 'placeholder' => 'Total Liquido')) }}
                    </div>
                    <div class="col-sm-6">
                        {{ Form::label('status', 'Status') }}
                        <select name="status" id="status" class="form-control">
                            <option value="0">Inativo</option>
                            <option value="1">Ativo</option>
                        </select>
                    </div>
                </div>
            </div>
            <input type="hidden" id="usu_id" name="usu_id" value="{{ Auth::user()->id }}">
            <input type="hidden" id="usu_id_fechou" name="usu_id_fechou" value="{{ Auth::user()->id }}">
            <input type="hidden" id="emp_id" name="emp_id" value="{{Auth::user()->emp_id }}">
            {{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}
            {{ Form::close() }}

        </div>
    </div>
@endsection