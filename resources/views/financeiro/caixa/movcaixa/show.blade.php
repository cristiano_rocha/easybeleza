@extends('layouts.app')

@section('content')
    <div class="container">
        <nav class="navbar navbar-inverse">
            <div class="navbar-header">
                <a href="{{ URL::to('financeiro/movcaixa') }}" class="navbar-brand">Lista</a>
            <a href="{{ URL::to('financeiro/movcaixa/abrir') }}">Abrir</a>
            </div>
        </nav>

        <h3>Movimento Nº {{$MovCaixa->movcx_id}}</h3>

        <div class="jumbotron text-center">
            <p>
                <strong>ID:</strong> {{ $MovCaixa->movcx_id }}<br>
                <strong>Caixa:</strong> {{ $MovCaixa->Caixa->cx_descricao }}<br>
                <strong>SALDO INICIAL:</strong> {{ number_format($MovCaixa->mov_saldo_ini, 2, ',', '.') }}<br>
                <strong>DATA ABERTURA:</strong> {{ date( 'd/m/Y' , strtotime($MovCaixa->dt_aberto)) }} <br>
                <strong>DATA FECHAMENTO:</strong> {{ (isset($row->dt_fecha)) ? date( 'd/m/Y' , strtotime($MovCaixa->dt_fecha)) : '' }}<br>
                <strong>SANGRIA:</strong>   {{ number_format($MovCaixa->mov_sangria, 2, ',', '.') }}<br>
                <strong>SUPLEMENTO:</strong> {{ number_format($MovCaixa->mov_suplemento, 2, ',', '.')}}<br>
                <strong>TOTAL VENDA:</strong> {{ number_format($MovCaixa->mov_totalvenda, 2, ',', '.') }}<br>                
                <strong>DINHEIRO:</strong>  {{ number_format($MovCaixa->mov_dinheiro, 2, ',', '.') }}<br>
                <strong>DUPLICATA:</strong> {{ number_format($MovCaixa->mov_duplicata, 2, ',', '.')}}<br>
                <strong>CHEQUE:</strong>    {{ number_format($MovCaixa->mov_cheque, 2, ',', '.')}}<br>
                <strong>CARTÃO:</strong>    {{ number_format($MovCaixa->mov_cartao, 2, ',', '.')}}<br>
                <strong>DESCONTO:</strong>  {{ number_format($MovCaixa->mov_desconto, 2, ',', '.')}}<br>
                <strong>TOTAL LIQUÍDO:</strong> {{ number_format($MovCaixa->mov_totalliquido, 2, ',', '.') }}<br>
                <strong>STATUS</strong> {{ ($MovCaixa->status == 1) ? 'Aberto' : 'Fechado' }}<br>
                <strong>ABERTO POR:</strong> {{ $MovCaixa->users->name}}
            </p>
        </div>

    </div>
@endsection