<?php
    setcookie('emp_id');
    setcookie('emp_id' , '0');
?>
<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <meta name="description" content="Programa para salão de beleza, sistema de gestão e marketing, seu salão de beleza na Web. Agendamento online e muito mais.">
        <meta name="keywords" content= "programa para salao, programa para salão, controle de estoque, salão de beleza online, como abrir um salão, programa para salao, programa para salão, salao de beleza online, sistema de salao de beleza, sistema para salão, software para salao, software para salão, sistema salao, sistema de salao, sistema salão, salao de beleza na web, software livre, sistema de gestão" />
        <meta name="robots" content="index,follow">
        <meta name="author" content="RochaNet Informática - SiD Soluções">
        <meta http-equiv="content-language" content="pt-BR">
        <meta name="googlebot" content="index,follow">
        <meta http-equiv="cache-control" content="max-age">

        <meta property="og:title" content="Programa para Salão de Beleza Completo - EasyBeleza">
        <meta property="og:description" content="Programa para salão de beleza com agendamento on-line, gestão de agenda, cadastro de clientes, controle financeiro, controle de estoque">
        <meta property="og:type" content="website">
        <meta property="og:site_name" content="EasyBeleza">
        <meta property="og:url" content="https://app.easybeleza.net.br">
        <meta property="og:image" content="https://app.easybeleza.net.br/img/logo-easybeleza.png">


        <link href="https://app.easybeleza.net.br/img/logo-x-easybeleza.png" rel="icon" />

        <title>{{ config('app.name') }} - {{ config('app.subtitle') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 54px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>

        <!-- Configurações Push Notification -->
        <link rel="manifest" href="/manifest.json">
        <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
        <script>
          var OneSignal = window.OneSignal || [];
          OneSignal.push(["init", {
            appId: "d26cc21e-6a5c-4937-bfc3-c19c900821eb",
            autoRegister: true,
            notifyButton: {
              enable: true /* Set to false to hide */
            },            
            welcomeNotification: {
                "title": "EasyBeleza",
                "message": "Agradecemos por ativar as notificações!"
            }
          }]);
        </script>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <!--<a href="{{ route('register') }}">Registrar</a> -->
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md col-sm-12 col-xs-12">
                    {{ config('app.name') }}
                </div>

                <div class="links">
                    {{ config('app.name') }} - {{ config('app.subtitle') }}
                </div>
            </div>
        </div>
    <!--Add the following script at the bottom of the web page (immediately before the </body> tag)-->
    <script type="text/javascript" async="async" defer="defer" data-cfasync="false" src="https://mylivechat.com/chatinline.aspx?hccid=14659850"></script>
    </body>
</html>