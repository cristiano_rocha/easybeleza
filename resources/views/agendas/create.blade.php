<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Cadastro</title>
  <link rel="stylesheet" href="{{ URL::to('css/app.css')}}" type="text/css">
  <link rel="stylesheet" href="{{ URL::to('css/fullcalendar.min.css') }}" type="text/css">
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
  <!-- Include Required Prerequisites -->
  <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
  {{--  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/3/css/bootstrap.css" /> --}}
  <!-- Include Date Range Picker -->
  <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
  <link rel="stylesheet" href="{{ URL::to('css/select2.min.css') }}"/>
  <script type="text/javascript" src="{{ URL::to('js/fullcalendar.js') }}"></script>
  <script type="text/javascript" src="{{ URL::to('js/pt-br.js') }}"></script>
  <script type="text/javascript" src="{{ URL::to('js/jquery.mask.min.js')}}"></script>
  <script type="text/javascript" src="{{ URL::to('js/select2.min.js')}}"></script>
  <script>
    $(document).ready(function() {
        $('#calendario')
          .fullCalendar({
            lang: 'pt-br',
            header: {
              center: 'title',
              left: 'prev,next,today',
              right: 'month,agendaWeek, agendaDay'
            },

            events: [{
              title: 'Manicure - Andressa',
              start: '2017-10-12'
            }],
            color: 'blue',
            textColor: 'white'

          });

        $('#tel')
          .mask('00 0000-00000');
        $('#hora')
          .mask('00:00');
        $('#date')
          .mask('00/00/0000')

        

        $('.profissionais')
          .select2();

        
      });
  </script>
</head>

<body>
  <script>
    $(document).ready(function() {
      jQuery("#tab a:first")
        .tab("show");
    });
  </script>
  <script>
    $(document)
      .ready(function() {
        $(function() {
            $('input[name="date"]').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true
              })
          },
          function(start, end, label) {
            var years = moment()
              .diff(start, 'years');
            alert("You are " + years + " years old.");
          });

      });
  </script>

    <!--//########## NAVBAR ################// -->
  <nav class="navbar bg-primary text-white">
    <a class="navbar-brand text-white" href="#">EasyBeleza</a>
  </nav>
    <!--//########## END OF NAVBAR ################// -->

    <!--//########## CONTAINER ################// -->
    <div class="container">
        <div class="card">
            <div class="card-body">
                <ul class="nav nav-tabs" id="tab">
                    <li class="nav-item">
                        <a href="#tabAgenda" class="nav-link" data-toggle="tab" aria-expanded="true">Agenda</a>
                    </li>
                    <li class="nav-item">
                        <a href="#tabCliente" class="nav-link"  data-toggle="tab" aria-expanded="true">Cliente</a>
                    </li>
                    <li class="nav-item">
                        <a href="#tabProfissional" class="nav-link"  data-toggle="tab" aria-expanded="true">Profissional</a>
                    </li>
                </ul>

                <!--//########## TAB CONTENT ################// -->
                <div class="tab-content">
                
                <!--//########## TAB AGENDA ################// -->

                <div class="tab-pane" id="tabAgenda">
                    <div class="row">
                        <div class="col-sm-5 card-body">
                            <h4 style="margin-bottom:25px;">
                                <i class="fa fa-calendar"></i>
                                Agenda
                                <button class="btn btn-primary float-right">
                                    <i class="fa fa-plus"></i>
                                </button>
                                <button class="btn btn-success float-right mr-1">
                                    <i class="fa fa-refresh"></i>
                                </button>
                            </h4>

                            <form action="">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Cliente">
                                        <span class="input-group-btn">
                                            <button class="btn btn-primary" type="button">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>  

                                <div class="form-row">
                                    <div class="col-sm-6">
                                        <div class="form-group ">
                                            <div class="input-group">
                                                <input type="tel" id="tel" class="form-control" placeholder="(__) ____-____">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-phone"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="date" id="date" class="form-control" placeholder="__/__/____">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>  

                                <div class="form-row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <select name="servicos" id="servicos" class="custom-select">
                                                <option selected>Selecione o Serviço</option>
                                                <option value="">2</option>
                                                <option value="">3</option>
                                                <option value="">4</option>                                        
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <select name="profisionais[]" id="profissionais[]" class="custom-select">
                                                <option selected>Selecione o Profissional</option>
                                                <option value="">1</option>
                                                <option value="">2</option>
                                                <option value="">3</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>     

                                <div class="form-row">
                                    <div class="col-sm-6">
                                        <div class="input-group">
                                            <input type="text" class="form-control">
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="input-group">
                                            <input type="time" id="hora" class="form-control">
                                            <span class="input-group-addon">
                                                <i class="fa fa-clock-o"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>               
                            </form>
                        </div>
                        
                        <!--//########## CALENDÁRIO ################// -->
                        <div class="col-sm-7 card-body">
                            <h4><i class="fa fa-calendar mr-1"></i>Calendário</h4>
                            
                            <div id="calendario"></div>
                        </div>
                    </div>
                      <!--//########## END OF CALENDÁRIO ################// -->
                </div>

                <!--//########## END OF TAB AGENDA ################// --> 

                <!--//########## TAB CLIENTE ################// -->

                <div class="tab-pane" id="tabCliente">
                    <div class="form-row" style="margin-top: 25px;">
                        <div class="col-sm-12">
                            <div class="form-row">
                                <div class="col-sm-3">
                                    <label for="cliente" class="control-label">Cliente</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control">
                                        <span class="input-group-addon">
                                            <i class="fa fa-search"></i>
                                        </span>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="periodo">Periodo</label>
                                        <select id="selectPeriodo" class="form-control">
                                            <option selected> Selecione um periodo...</option>
                                            <option value="0">Hoje</option>
                                            <option value="1">Ontem</option>
                                            <option value="7">7 dias</option>
                                            <option value="15">15 dias</option>
                                            <option value="30">30 dias</option>
                                            <option value="45">45 dias</option>
                                            <option value="60">60 dias</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="form-row">
                                        <label for="intervalo" class="control-label">Intervalo</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control">
                                            <span class="input-group-addon">
                                                até
                                            </span>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-3 ml-2">
                                    <div class="form-row">
                                        <label class="control-label">Profissional</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control">
                                            <span class="input-group-addon">
                                                <i class="fa fa-search"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div id="tabelaHistorico">
                                <table class="table table-stiped table-condensed">
                                    <thead>
                                        <tr>
                                            <th width="150" class="text-left">Data</th>
                                            <th class="text-left">Cliente</th>
                                            <th class="text-left">Serviço</th>
                                            <th class="text-left">Profissional</th>
                                            <th class="text-left">Status</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr>
                                            <td colspan="5"><div class="alert alert-default text-center">Nenhum registro encontrado.</div></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <!--//########## END OF TAB CLIENTE ################// -->

                <!--//########## TAB PROFISSIONAL ################// -->

                <div class="tab-pane" id="tabProfissional">
                    <div class="form-row" style="margin-top: 25px;">
                        <div class="col-sm-12">
                            <div class="form-row">
                                <div class="col-sm-3">
                                    <label for="profissional" class="control-label">Profissional</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control">
                                        <span class="input-group-addon">
                                            <i class="fa fa-search"></i>
                                        </span>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="periodo">Periodo</label>
                                        <select id="selectPeriodo" class="form-control">
                                            <option selected> Selecione um periodo...</option>
                                            <option value="0">Hoje</option>
                                            <option value="1">Ontem</option>
                                            <option value="7">7 dias</option>
                                            <option value="15">15 dias</option>
                                            <option value="30">30 dias</option>
                                            <option value="45">45 dias</option>
                                            <option value="60">60 dias</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="form-row">
                                        <label for="intervalo" class="control-label">Intervalo</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control">
                                            <span class="input-group-addon">
                                                até
                                            </span>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-3 ml-2">
                                    <div class="form-row">
                                        <label class="control-label">Cliente</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control">
                                            <span class="input-group-addon">
                                                <i class="fa fa-search"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div id="tabelaHistorico">
                                <table class="table table-stiped table-condensed">
                                    <thead>
                                        <tr>
                                            <th width="150" class="text-left">Data</th>
                                            <th class="text-left">Profissional</th>
                                            <th class="text-left">Serviço</th>
                                            <th class="text-left">Cliente</th>
                                            <th class="text-left">Status</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr>
                                            <td colspan="5"><div class="alert alert-default text-center">Nenhum registro encontrado.</div></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <!--//########## END OF TAB PROFISSIONAL ################// -->                    
                </div>
                <!--//########## END OF TAB CONTENT ################// -->

            </div>
        </div>
    </div>
    <!--//########## END OF NAVBAR ################// -->

  

    {{--  Jquery no bottom comentado pois precisa ser carregado no head para que alguns plugins funcione, ex: FullCalender e Datarangepicker  --}}
    {{--  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>  --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
</body>

</html>