@extends('layouts.app')

@section('content')
	<div class="agenda col-md-12">
		<nav class="navbar navbar-inverse">
	        <div class="navbar-header">
        	    <a class="navbar-brand" href="{{ URL::to('lista') }}"><- Lista</a>
    	        <a href="{{ URL::to('gcalendar/create') }}">Novo</a>
	        </div>
		</nav> 
	
		<div class="clear"></div>
		<span class="col-md-8"><h3><i class="fa fa-user-circle-o"></i> Cliente: {{$agenda->Cliente->cli_nome}}</h3></span>
		<span class="col-md-8"><h5><i class="fa fa-user"></i> Profissional: {{$agenda->Profissional->prof_nome}}</h5></span>
		<span class="col-md-8"><h5><i class="fa fa-address-card-o"></i> Serviço: {{$agenda->ProdutoServico->prosrv_nome}}</h5></span>
		<span class="col-md-8"><h5><i class="fa fa-calendar"></i> {{ date('d/m/Y', strtotime($agenda->agd_data)) }}</h5></span>
		<span class="col-md-8"><h5><i class="fa fa-clock-o"></i> {{ date('H:i', strtotime($agenda->agd_hora)) }} - {{ date('H:i', strtotime($agenda->agd_horafim)) }}</h5></span>
	</div>
@endsection
