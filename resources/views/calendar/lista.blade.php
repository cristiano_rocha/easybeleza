@extends('layouts.app')

@section('content')
<!-- CheckBox marque -->
<div class="container">
    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <i class="fa fa-calendar"></i> <a class="navbar-brand" href="{{ URL::to('./agenda') }}">Agenda</a>
            <i class="fa fa-list-alt"></i> <a class="navbar-brand" href="{{ URL::to('lista') }}">Lista</a>
            <i class="fa fa-newspaper-o"></i> <a class="navbar-brand" href="{{ URL::to('agenda/create') }}">Novo</a>
        </div>
    </nav> 

    <div class="card">
        <div class="card-body">
            <div class="card-title col-md-3">Planejamento/Agenda</div>  
            <!-- Mensagem de retorno do Cadastro -->
            @if (Session::has('message'))
                <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif  
            <div class="filtro-data">
                <form name="form-lista" id="form-lista" method="POST" class="navbar-form navbar-left" role="search" action="{{url('lista/busca')}}">
                <div class="btn-group" data-toggle="buttons">
                    <label class="btn btn-light {{($periodo == 'mes') ? 'active' : '' }}">
                      <input class="periodo" type="radio" name="periodo[]" id="mes" autocomplete="off" value="mes"  onchange="document.getElementById('form-lista').submit();" {{($periodo == 'mes') ? 'checked' : '' }}>
                       Mês
                    </label>
                    <label class="btn btn-light {{($periodo == 'semana') ? 'active' : '' }}">
                      <input class="periodo" type="radio" name="periodo[]" id="semana" autocomplete="off" value="semana"  onchange="document.getElementById('form-lista').submit();" {{($periodo == 'semana') ? 'checked' : '' }}> Semana
                    </label>
                    <label class="btn btn-light {{($periodo == 'hoje') ? 'active' : '' }}">
                      <input class="periodo" type="radio" name="periodo[]" id="hoje" autocomplete="off" value="hoje" onchange="document.getElementById('form-lista').submit();" {{($periodo == 'hoje') ? 'checked' : '' }}> Hoje
                    </label>
                </div>
            </div>        
            <div id="agenda-itens" class="table-resposivo">            
                <table class="table">
                    <thead>
                      <tr>
                        <th class="chkfiltro">
                            <label id="marque" class="checkbox col-md-5">
                                <input class="todos" type="checkbox" name="profissionais" onclick="marcardesmarcar(this.checked);enviar(this.checked);" checked>
                                <span id="acao">Todos</span>
                            </label>
                        </th>
                        <th class="titulo">Profissional</th>
                        <th class="titulo">Cliente</th>
                        <th class="titulo">Data</th>
                        <th class="titulo">Hora</th>
                        <th class="titulo">Ações </th>
                        </tr>
                    </thead>
                    <tbody>    
                        <tr>
                            <td rowspan="{{(count($profissionais) > count($lista)) ? count($profissionais) : count($lista) }}">
                                
                                <div class="col-md-12">
                                    <input type="hidden" name="profissionais[]" value="0" checked/>
                                    @foreach($profissionais as $profissional)
                                        <?php $nome_prof = explode(" ", $profissional->prof_nome); ?>
                                        <label class="checkbox small col-md-12">
                                            <input onclick="document.getElementById('form-lista').submit();" class="marcar" type="checkbox" id="prof_{{ $profissional->prof_id }}" name="profissionais[]" value="{{ $profissional->prof_id }}"
                                            @foreach ($request->profissionais as $postprof)
                                                {{ $profissional->prof_id == $postprof ? 'checked' : '' }}
                                            @endforeach >
                                            {{ $profissional->prof_id.' - '.$nome_prof[0] }}
                                        </label>
                                        <?php $prof_fim = $profissional->prof_id ?>
                                    @endforeach
                                </div>
                                {{ csrf_field() }}
                                </form>
                            </td>
                      @if (count($lista) == 0) 
                            <td colspan="6"><div class="alert alert-info">{{ Session::get('message') }}</div></td>
                        </tr>
                      @else
                        @foreach($lista as $row)  
                        </tr>                      
                        <tr>
                          <td class="agenda-lista">{{$row->profissional->prof_nome}}</td>
                          <td class="agenda-lista">{{$row->cliente->cli_nome}}</td>
                          <td class="agenda-lista">{{ date('d/m/Y', strtotime($row->agd_data) ) }}</td>
                          <td class="agenda-lista">{{ date('H:i:s', strtotime($row->agd_hora)) }}</td>
                          <!-- Botões Exibir, Alterar e Excluir -->
                          <td>
                              <a class="btn btn-small btn-success" href="{{ URL::to('agenda/' . $row->agd_id) }}">V</a>
                              <a class="btn btn-small btn-info" href="{{ URL::to('gcalendar/'.$row->agd_id.'/edit') }}">A</a>
                              <div class="btn">
                              {{ Form::open(array('url' => 'gcalendar/' . $row->agd_id, 'class' => 'pull-right')) }}
                              {{ Form::hidden('_method', 'DELETE') }}
                              {{ Form::submit('E', array('class' => 'btn btn-warning')) }}
                              {{ Form::close() }}
                              </div>
                          </td>
                        </tr>
                        @endforeach 
                      @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- CheckBox marque -->
<script>
    function marcardesmarcar() {
        $('.marcar').each(function () {
            if (this.checked){
                this.checked = false;
                document.getElementById('acao').innerHTML = 'Todos';
            }else{
                this.checked = true;
                document.getElementById('acao').innerHTML = 'Todos';                
            }     
        });  
    }
    function enviar(){
        $('[name="profissionais[]"]').change(function()
        {
            if ($(this).is(':checked')) {
               // Tem profissional selecionado...
               document.forms["form-lista"].submit();
            }else{
                alert('Nenhum profissional selecionado!');
            }
        });        
    }
</script>
@endsection