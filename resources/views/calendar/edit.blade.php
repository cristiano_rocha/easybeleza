@extends('layouts.app')

@section('content')
<div class="container">
    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('lista') }}"><- Lista</a>
        </div>
    </nav> 
    <!-- se houver erros, serão mostrados -->
    {{ Html::ul($errors->all()) }}
    <div class="form-control">
        {{ Form::model($agenda, array('route' => array('gcalendar.update', $agenda->agd_id), 'method' => 'PUT')) }} 
        <legend>
            Alterar Evento Nº {{ $agenda->agd_id }}
        </legend>
        <div class="form-group">
            <label for="title">
                Cliente
            </label>
            <select class="form-control" name="cli_id" id="cli_id">
                @foreach ($jCli as $row)
                    <option value="{{ $row->cli_id }}|{{ $row->cli_nome }}" {{($agenda->cli_id == $row->cli_id) ? 'selected' : ''}}>{{ $row->cli_id }} - {{ $row->cli_nome }}</option>
                @endforeach
            </select>  
        </div>
        <div class="form-group">
            <label for="title">
                Profissional
            </label>
            <select class="form-control" name="title" id="sprosrv_id">
                <option value="0" selected="select">Selecione Profissional</option>
                @foreach ($jprof as $row)
                    <option value="{{ $row->prof_id }}|{{ $row->prof_nome }}" {{($agenda->prof_id == $row->prof_id) ? 'selected' : ''}}>{{ $row->prof_id }} - {{ $row->prof_nome }}</option>
                @endforeach
            </select>  
        </div>
        <div class="form-group">
            <label for="description">
                Serviço
            </label>
            <select class="form-control" name="prosrv_id" id="sprosrv_id">
                <option value="0" selected="select">Selecione um produto</option>
                @foreach ($jprod as $row)
                    <option value="{{ $row->prosrv_id }}|{{ $row->prosrv_nome }}" {{($agenda->prosrv_id == $row->prosrv_id) ? 'selected' : ''}}>{{ $row->prosrv_id }} - {{ $row->prosrv_nome }}</option>
                @endforeach
            </select>  
        </div>
        <div class="form-group">
            <label for="start_date" class="col-md-4">
                Início
            </label>     
            <label for="end_date">   Fim</label>
            <div class="row">      
                <div class="col-md-2">
                    <input class="form-control datepicker" name="start_date" placeholder="dd/mm/aaaa" type="text" value="{{ date('d/m/Y', strtotime($agenda->agd_data)) }}">
                </div>
                <div class="col-md-2">
                    <select class="form-control" name="start_time">
                        <option value="08:00" {{(date('H:i:s', strtotime($agenda->agd_hora)) == '08:00:00') ? 'selected' : ''}}>08:00</option>
                        <option value="08:30" {{(date('H:i:s', strtotime($agenda->agd_hora)) == '08:30:00') ? 'selected' : ''}}>08:30</option>
                        <option value="09:00" {{(date('H:i:s', strtotime($agenda->agd_hora)) == '09:00:00') ? 'selected' : ''}}>09:00</option>
                        <option value="09:30" {{(date('H:i:s', strtotime($agenda->agd_hora)) == '09:30:00') ? 'selected' : ''}}>09:30</option>
                        <option value="10:00">10:00</option>
                        <option value="10:00">10:30</option>
                        <option value="11:00">11:00</option>
                        <option value="11:00">11:30</option>
                        <option value="11:00">12:00</option>
                        <option value="11:00">12:30</option>
                        <option value="13:00">13:00</option>
                        <option value="13:00">13:30</option>
                        <option value="14:00">14:00</option>
                        <option value="14:00">14:30</option>
                        <option value="15:00">15:00</option>
                        <option value="15:00">15:30</option>
                        <option value="16:00">16:00</option>
                        <option value="16:00">16:30</option>
                        <option value="17:00">17:00</option>
                        <option value="17:00">17:30</option>
                        <option value="18:00">18:00</option>
                        <option value="18:00">18:30</option>
                        <option value="19:00">19:00</option>
                    </select>
                </div>            
                <div class="col-md-2">   
                    <input class="form-control datepicker" name="end_date" placeholder="dd/mm/aaaa" type="text" value="{{ date('d/m/Y', strtotime($agenda->agd_data)) }}">
                </div>       
                <div class="col-md-2">
                    <select class="form-control" name="end_time">
                        <option value="08:00" {{(date('H:i:s', strtotime($agenda->agd_hora)) == '08:00:00') ? 'selected' : ''}}>08:00</option>
                        <option value="08:30" {{(date('H:i:s', strtotime($agenda->agd_hora)) == '08:30:00') ? 'selected' : ''}}>08:30</option>
                        <option value="09:00" {{(date('H:i:s', strtotime($agenda->agd_hora)) == '09:00:00') ? 'selected' : ''}}>09:00</option>
                        <option value="09:30" {{(date('H:i:s', strtotime($agenda->agd_hora)) == '09:30:00') ? 'selected' : ''}}>09:30</option>
                        <option value="10:00">10:00</option>
                        <option value="10:00">10:30</option>
                        <option value="11:00">11:00</option>
                        <option value="11:00">11:30</option>
                        <option value="11:00">12:00</option>
                        <option value="11:00">12:30</option>
                        <option value="13:00">13:00</option>
                        <option value="13:00">13:30</option>
                        <option value="14:00">14:00</option>
                        <option value="14:00">14:30</option>
                        <option value="15:00">15:00</option>
                        <option value="15:00">15:30</option>
                        <option value="16:00">16:00</option>
                        <option value="16:00">16:30</option>
                        <option value="17:00">17:00</option>
                        <option value="17:00">17:30</option>
                        <option value="18:00">18:00</option>
                        <option value="18:00">18:30</option>
                        <option value="19:00">19:00</option>
                    </select>
                </div>
            </div>            
        </div>
        <input type="hidden" id="agd_id" name="agd_id" value="{{$agenda->agd_id}}">
        <input type="hidden" id="emp_id" name="emp_id" value="{{Auth::user()->emp_id}}">
        <button class="btn btn-primary" type="submit">
            Salvar
        </button>
        </form>
    </div>
</div>
@endsection