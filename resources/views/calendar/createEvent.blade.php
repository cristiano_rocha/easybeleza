@extends('layouts.app')

@section('content')
<div class="container">
    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('lista') }}"><- Lista</a>
        </div>
    </nav> 
    <!-- se houver erros, serão mostrados -->
    {{ Html::ul($errors->all()) }}
    <div class="form-control">
        <form action="{{route('gcalendar.store')}}" method="POST" role="form">
        {{csrf_field()}}
        <legend>
            Criar Evento
        </legend>
        <div class="form-group">
            <label for="title">
                Cliente
            </label>
            <select class="form-control" name="cli_id" id="cli_id">
                <option value="0" selected="select">Selecione Cliente</option>
                @foreach ($jCli as $row)
                    <option value="{{ $row->cli_id }}|{{ $row->cli_nome }}">{{ $row->cli_id }} - {{ $row->cli_nome }}</option>
                @endforeach
            </select>  
        </div>
        <div class="form-group">
            <label for="title">
                Profissional
            </label>
            <select class="form-control" name="title" id="sprosrv_id">
                <option value="0" selected="select">Selecione Profissional</option>
                @foreach ($jprof as $row)
                    <option value="{{ $row->prof_id }}|{{ $row->prof_nome }}">{{ $row->prof_id }} - {{ $row->prof_nome }}</option>
                @endforeach
            </select>  
        </div>
        <div class="form-group">
            <label for="description">
                Serviço
            </label>
            <select class="form-control" name="prosrv_id" id="sprosrv_id">
                <option value="0" selected="select">Selecione um produto</option>
                @foreach ($jprod as $row)
                    <option value="{{ $row->prosrv_id }}|{{ $row->prosrv_nome }}">{{ $row->prosrv_id }} - {{ $row->prosrv_nome }}</option>
                @endforeach
            </select>  
        </div>
        <div class="form-group">
            <label for="start_date" class="col-md-4">
                Início
            </label>     
            <label for="end_date">   Fim</label>
            <div class="row">      
                <div class="col-md-2">
                    <input class="form-control datepicker" name="start_date" placeholder="dd/mm/aaaa" type="text">
                </div>
                <div class="col-md-2">
                    <select class="form-control" name="start_time">
                        <option value="08:00">08:00</option>
                        <option value="08:00">08:30</option>
                        <option value="09:00">09:00</option>
                        <option value="09:00">09:30</option>
                        <option value="10:00">10:00</option>
                        <option value="10:00">10:30</option>
                        <option value="11:00">11:00</option>
                        <option value="11:00">11:30</option>
                        <option value="11:00">12:00</option>
                        <option value="11:00">12:30</option>
                        <option value="13:00">13:00</option>
                        <option value="13:00">13:30</option>
                        <option value="14:00">14:00</option>
                        <option value="14:00">14:30</option>
                        <option value="15:00">15:00</option>
                        <option value="15:00">15:30</option>
                        <option value="16:00">16:00</option>
                        <option value="16:00">16:30</option>
                        <option value="17:00">17:00</option>
                        <option value="17:00">17:30</option>
                        <option value="18:00">18:00</option>
                        <option value="18:00">18:30</option>
                        <option value="19:00">19:00</option>
                    </select>
                </div>            
                <div class="col-md-2">   
                    <input class="form-control datepicker" name="end_date" placeholder="dd/mm/aaaa" type="text">
                </div>       
                <div class="col-md-2">
                    <select class="form-control" name="end_time">
                        <option value="08:00">08:00</option>
                        <option value="08:00">08:30</option>
                        <option value="09:00">09:00</option>
                        <option value="09:00">09:30</option>
                        <option value="10:00">10:00</option>
                        <option value="10:00">10:30</option>
                        <option value="11:00">11:00</option>
                        <option value="11:00">11:30</option>
                        <option value="11:00">12:00</option>
                        <option value="11:00">12:30</option>
                        <option value="13:00">13:00</option>
                        <option value="13:00">13:30</option>
                        <option value="14:00">14:00</option>
                        <option value="14:00">14:30</option>
                        <option value="15:00">15:00</option>
                        <option value="15:00">15:30</option>
                        <option value="16:00">16:00</option>
                        <option value="16:00">16:30</option>
                        <option value="17:00">17:00</option>
                        <option value="17:00">17:30</option>
                        <option value="18:00">18:00</option>
                        <option value="18:00">18:30</option>
                        <option value="19:00">19:00</option>
                    </select>
                </div>
            </div>            
        </div>
        <input type="hidden" id="usu_id" name="usu_id" value="{{Auth::user()->id}}">
        <input type="hidden" id="emp_id" name="emp_id" value="{{Auth::user()->emp_id}}">
        <button class="btn btn-primary" type="submit">
            Salvar
        </button>
        </form>
    </div>
</div>
@endsection