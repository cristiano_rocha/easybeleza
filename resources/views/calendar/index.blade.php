@extends('layouts.app')

@section('content')
<div class="container">
	<div class="card">
		<!-- Se houver mensagens -->
        @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif
	</div>

    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('lista') }}">Lista</a>
            <a href="{{ URL::to('agenda/create') }}">Novo</a>
        </div>
    </nav>
</div>
@endsection