@extends('layouts.app')

@section('content')
	<div class="agenda col-md-12">
		<nav class="navbar navbar-inverse">
	        <div class="navbar-header">
        	    <a class="navbar-brand" href="{{ URL::to('/agendas') }}">Agenda</a>
        	    <a class="navbar-brand" href="{{ URL::to('lista') }}">Lista</a>
    	        <a href="{{ URL::to('gcalendar/create') }}">Novo</a>
	        </div>
		</nav> 
	
		<div class="clear"></div>
		<h3>Profissional: {{$event->getSummary()}}</h3>
		<?php $partes = explode(" <BR />", $event->getDescription()); ?>
		<span class="col-md-12">{{$partes[0]}}</span><BR />
		<span class="col-md-12">{{$partes[1]}}</span><BR />
		<span class="col-md-8">Data: {{date('d/m/Y', strtotime($event->start->getDateTime()))}}</span><BR />
		<span class="col-md-8">Hora início: {{substr($event->start->getDateTime(),11,5)}}</span>
		<span class="col-md-8">Hora fim: {{substr($event->end->getDateTime(),11,5)}}</span>
	</div>
@endsection
