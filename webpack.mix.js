let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.autoload({
    jquery: ['$', 'window.jQuery', 'jQuery']
    // 'popper.js/dist/umd/popper.js': ['Popper']
})

mix.js('resources/assets/js/bootstrap.js', 'public/js')
    .copy('node_modules/jquery-mask-plugin/dist/jquery.mask.min.js', 'public/js')
    .copy('node_modules/typeahead/typeahead.js', 'public/js')
    .copy('node_modules/bloodhound/index.js','public/js')
    .copy('node_modules/typeahead/style.css','public/css');
//    .sass('resources/assets/sass/app.scss', 'public/css');
