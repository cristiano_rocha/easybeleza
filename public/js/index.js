var astjourney = require('astjourney')

module.exports = bloodhound

function bloodhound(code, rawNodes) {
  var ast = astjourney.makeAst(code)
  var args = []
  
  astjourney.visitAll(ast, function(node, parents) {
    if (node.type === 'call' && node.func.type === 'name' && node.func.value === 'require' && node.args.length === 1) {
      args.push(node.args[0])
    }
  })
  
  if (!rawNodes) {
    args = args.filter(function(node) {
      return node.type === 'string'
    }).map(function(node) {
      return node.value
    })
  }
  
  return args
}
